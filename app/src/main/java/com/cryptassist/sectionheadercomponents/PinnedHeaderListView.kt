package com.cryptassist.sectionheadercomponents

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.widget.ListAdapter
import android.widget.ListView


/**
 * A ListView that maintains a header pinned at the top of the list. The pinned header can be pushed up and dissolved as
 * needed.
 */
class PinnedHeaderListView : ListView {
    private var mEnableHeaderTransparencyChanges = true
    private var mAdapter: PinnedHeaderAdapter? = null
    private var mHeaderView: View? = null
    private var mHeaderViewVisible: Boolean = false
    private var mHeaderViewWidth: Int = 0
    private var mHeaderViewHeight: Int = 0

    /**
     * Adapter interface. The list adapter must implement this interface.
     */
    interface PinnedHeaderAdapter {

        /**
         * Computes the desired state of the pinned header for the given position of the first visible list item.
         * Allowed return values are [.PINNED_HEADER_GONE], [.PINNED_HEADER_VISIBLE] or
         * [.PINNED_HEADER_PUSHED_UP].
         */
        fun getPinnedHeaderState(position: Int): Int

        /**
         * Configures the pinned header view to match the first visible list item.
         *
         * @param header   pinned header view.
         * @param position position of the first visible list item.
         * @param alpha    fading of the header view, between 0 and 255.
         */
        fun configurePinnedHeader(header: View, position: Int, alpha: Int)

        companion object {
            /**
             * Pinned header state: don't show the header.
             */
            val PINNED_HEADER_GONE = 0
            /**
             * Pinned header state: show the header at the top of the list.
             */
            val PINNED_HEADER_VISIBLE = 1
            /**
             * Pinned header state: show the header. If the header extends beyond the bottom of the first shown element,
             * push it up and clip.
             */
            val PINNED_HEADER_PUSHED_UP = 2
        }
    }

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    fun setPinnedHeaderView(view: View) {
        mHeaderView = view
        // Disable vertical fading when the pinned header is present
        // TODO change ListView to allow separate measures for top and bottom fading edge;
        // in this particular case we would like to disable the top, but not the bottom edge.
        if (mHeaderView != null)
            setFadingEdgeLength(0)
        requestLayout()
    }

    override fun setAdapter(adapter: ListAdapter) {
        super.setAdapter(adapter)
        if (!isInEditMode)
            mAdapter = adapter as PinnedHeaderAdapter
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (mHeaderView != null) {
            measureChild(mHeaderView, widthMeasureSpec, heightMeasureSpec)
            mHeaderViewWidth = mHeaderView!!.measuredWidth
            mHeaderViewHeight = mHeaderView!!.measuredHeight
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (mHeaderView != null) {
            mHeaderView!!.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight)
            configureHeaderView(firstVisiblePosition)
        }
    }

    fun configureHeaderView(position: Int) {
        try {
            if (mHeaderView == null || mAdapter == null)
                return
            val state = mAdapter!!.getPinnedHeaderState(position)
            when (state) {
                PinnedHeaderAdapter.PINNED_HEADER_GONE -> {
                    mHeaderViewVisible = false
                }
                PinnedHeaderAdapter.PINNED_HEADER_VISIBLE -> {
                    mAdapter!!.configurePinnedHeader(mHeaderView!!, position, MAX_ALPHA)
                    if (mHeaderView!!.top != 0)
                        mHeaderView!!.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight)
                    mHeaderViewVisible = true
                }
                PinnedHeaderAdapter.PINNED_HEADER_PUSHED_UP -> {
                    val firstView = getChildAt(0) ?: /*break*/return
                    val bottom = firstView.bottom
                    // final int itemHeight=firstView.getHeight();
                    val headerHeight = mHeaderView!!.height
                    val y: Int
                    val alpha: Int
                    if (bottom < headerHeight) {
                        y = bottom - headerHeight
                        alpha = MAX_ALPHA * (headerHeight + y) / headerHeight
                    } else {
                        y = 0
                        alpha = MAX_ALPHA
                    }
                    mAdapter!!.configurePinnedHeader(mHeaderView!!, position, if (mEnableHeaderTransparencyChanges) alpha else MAX_ALPHA)
                    if (mHeaderView!!.top != y)
                        mHeaderView!!.layout(0, y, mHeaderViewWidth, mHeaderViewHeight + y)
                    mHeaderViewVisible = true
                }
            }
        } catch (ex: Exception) {
        }

    }

    fun setEnableHeaderTransparencyChanges(mEnableHeaderTransparencyChanges: Boolean) {
        this.mEnableHeaderTransparencyChanges = mEnableHeaderTransparencyChanges
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        if (mHeaderViewVisible)
            drawChild(canvas, mHeaderView, drawingTime)
    }

    companion object {

        private val MAX_ALPHA = 255
    }
}
