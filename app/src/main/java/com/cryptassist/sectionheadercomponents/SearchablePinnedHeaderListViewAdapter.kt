package com.cryptassist.sectionheadercomponents

import android.text.TextUtils
import android.widget.Filter
import android.widget.Filterable

import java.util.ArrayList


/**
 * This gives the ability of searching in a pinnedHead list view
 */
abstract class SearchablePinnedHeaderListViewAdapter<T> : IndexedPinnedHeaderListViewAdapter(), Filterable {
    private var filterListCopy: ArrayList<T>? = null
    private val mFilter: Filter

    abstract val originalList: ArrayList<T>

    init {
        mFilter = object : Filter() {
            internal var lastConstraint: CharSequence? = null

            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults? {
                if (constraint == null || constraint.length == 0)
                    return null
                val newFilterArray = ArrayList<T>()
                val results = Filter.FilterResults()
                for (item in originalList)
                    if (doFilter(item, constraint))
                        newFilterArray.add(item)
                results.values = newFilterArray
                results.count = newFilterArray.size
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults?) {
                filterListCopy = if (results == null) null else results.values as ArrayList<T>
                val needRefresh = !TextUtils.equals(constraint, lastConstraint)
                lastConstraint = constraint
                if (needRefresh)
                    notifyDataSetChanged()
            }
        }
    }

    override fun getFilter(): Filter {
        return mFilter
    }

    /** returns true iff the item can "pass" the filtering process and should be shown  */
    abstract fun doFilter(item: T, constraint: CharSequence?): Boolean

    override fun getItem(position: Int): T? {
        if (position < 0)
            return null
        val listCopy = filterListCopy
        if (listCopy != null) {
            return if (position < listCopy.size)
                listCopy[position]
            else
                null
        } else {
            val originalList = originalList
            return if (position < originalList.size)
                originalList[position]
            else
                null
        }
    }

    override fun getCount(): Int {
        val listCopy = filterListCopy
        return listCopy?.size ?: originalList.size
    }

}
