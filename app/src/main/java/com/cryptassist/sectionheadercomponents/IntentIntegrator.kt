package com.cryptassist.sectionheadercomponents

/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.annotation.TargetApi
import android.app.Activity
import android.app.Fragment
import android.content.Intent
import android.os.Build
import android.os.Bundle
import com.cryptassist.utils.CaptureActivity

import com.google.zxing.client.android.Intents


import java.util.Arrays
import java.util.Collections
import java.util.HashMap

/**
 *
 * @author Sean Owen
 * @author Fred Lin
 * @author Isaac Potoczny-Jones
 * @author Brad Drehmer
 * @author gcstang
 */
class IntentIntegrator
/**
 * @param activity [Activity] invoking the integration
 */
(private val activity: Activity) {
    private var fragment: android.app.Fragment? = null
    private var supportFragment: android.support.v4.app.Fragment? = null

    private val moreExtras = HashMap<String, Any>(3)

    private var desiredBarcodeFormats: Collection<String>? = null

    private var captureActivity: Class<*>? = null

    protected val defaultCaptureActivity: Class<*>
        get() = CaptureActivity::class.java

    fun getCaptureActivity(): Class<*> {
        if (captureActivity == null) {
            captureActivity = defaultCaptureActivity
        }
        return captureActivity as Class<*>
    }

    /**
     * Set the Activity class to use. It can be any activity, but should handle the intent extras
     * as used here.
     *
     * @param captureActivity the class
     */
    fun setCaptureActivity(captureActivity: Class<*>): IntentIntegrator {
        this.captureActivity = captureActivity
        return this
    }

    fun getMoreExtras(): Map<String, *> {
        return moreExtras
    }

    fun addExtra(key: String, value: Any): IntentIntegrator {
        moreExtras[key] = value
        return this
    }

    /**
     * Set a prompt to display on the capture screen, instead of using the default.
     *
     * @param prompt the prompt to display
     */
    fun setPrompt(prompt: String?): IntentIntegrator {
        if (prompt != null) {
            addExtra(Intents.Scan.PROMPT_MESSAGE, prompt)
        }
        return this
    }

    /**
     * By default, the orientation is locked. Set to false to not lock.
     *
     * @param locked true to lock orientation
     */
    fun setOrientationLocked(locked: Boolean): IntentIntegrator {
        addExtra("" + 90, locked)
        return this
    }

    /**
     * Use the specified camera ID.
     *
     * @param cameraId camera ID of the camera to use. A negative value means "no preference".
     * @return this
     */
    fun setCameraId(cameraId: Int): IntentIntegrator {
        if (cameraId >= 0) {
            addExtra(Intents.Scan.CAMERA_ID, cameraId)
        }
        return this
    }

    /**
     * Set to false to disable beep on scan.
     *
     * @param enabled false to disable beep
     * @return this
     */
    fun setBeepEnabled(enabled: Boolean): IntentIntegrator {
        addExtra(Intents.Scan.BEEP_ENABLED, enabled)
        return this
    }

    /**
     * Set to true to enable saving the barcode image and sending its path in the result Intent.
     *
     * @param enabled true to enable barcode image
     * @return this
     */
    fun setBarcodeImageEnabled(enabled: Boolean): IntentIntegrator {
        addExtra(Intents.Scan.BARCODE_IMAGE_ENABLED, enabled)
        return this
    }

    /**
     * Set the desired barcode formats to scan.
     *
     * @param desiredBarcodeFormats names of `BarcodeFormat`s to scan for
     * @return this
     */
    fun setDesiredBarcodeFormats(desiredBarcodeFormats: Collection<String>): IntentIntegrator {
        this.desiredBarcodeFormats = desiredBarcodeFormats
        return this
    }

    /**
     * Initiates a scan for all known barcode types with the default camera.
     */
    fun initiateScan() {
        startActivityForResult(createScanIntent(), REQUEST_CODE)
    }

    /**
     * Initiates a scan for all known barcode types with the default camera.
     * And starts a timer to finish on timeout
     * @return Activity.RESULT_CANCELED and true on parameter TIMEOUT.
     */
    fun setTimeout(timeout: Long): IntentIntegrator {
        addExtra(Intents.Scan.TIMEOUT, timeout)
        return this
    }

    /**
     * Create an scan intent with the specified options.
     *
     * @return the intent
     */
    fun createScanIntent(): Intent {
        val intentScan = Intent(activity, getCaptureActivity())
        intentScan.action = Intents.Scan.ACTION

        // check which types of codes to scan for
        if (desiredBarcodeFormats != null) {
            // set the desired barcode types
            val joinedByComma = StringBuilder()
            for (format in desiredBarcodeFormats!!) {
                if (joinedByComma.length > 0) {
                    joinedByComma.append(',')
                }
                joinedByComma.append(format)
            }
            intentScan.putExtra(Intents.Scan.FORMATS, joinedByComma.toString())
        }

        intentScan.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intentScan.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
        attachMoreExtras(intentScan)
        return intentScan
    }

    /**
     * Initiates a scan, only for a certain set of barcode types, given as strings corresponding
     * to their names in ZXing's `BarcodeFormat` class like "UPC_A". You can supply constants
     * like [.PRODUCT_CODE_TYPES] for example.
     *
     * @param desiredBarcodeFormats names of `BarcodeFormat`s to scan for
     */
    fun initiateScan(desiredBarcodeFormats: Collection<String>) {
        setDesiredBarcodeFormats(desiredBarcodeFormats)
        initiateScan()
    }

    /**
     * Start an activity. This method is defined to allow different methods of activity starting for
     * newer versions of Android and for compatibility library.
     *
     * @param intent Intent to start.
     * @param code   Request code for the activity
     * @see android.app.Activity.startActivityForResult
     * @see android.app.Fragment.startActivityForResult
     */
    protected fun startActivityForResult(intent: Intent, code: Int) {
        if (fragment != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                fragment!!.startActivityForResult(intent, code)
            }
        } else if (supportFragment != null) {
            supportFragment!!.startActivityForResult(intent, code)
        } else {
            activity.startActivityForResult(intent, code)
        }
    }

    protected fun startActivity(intent: Intent) {
        if (fragment != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                fragment!!.startActivity(intent)
            }
        } else if (supportFragment != null) {
            supportFragment!!.startActivity(intent)
        } else {
            activity.startActivity(intent)
        }
    }

    private fun attachMoreExtras(intent: Intent) {
        for ((key, value) in moreExtras) {
// Kind of hacky
            if (value is Int) {
                intent.putExtra(key, value)
            } else if (value is Long) {
                intent.putExtra(key, value)
            } else if (value is Boolean) {
                intent.putExtra(key, value)
            } else if (value is Double) {
                intent.putExtra(key, value)
            } else if (value is Float) {
                intent.putExtra(key, value)
            } else if (value is Bundle) {
                intent.putExtra(key, value)
            } else {
                intent.putExtra(key, value.toString())
            }
        }
    }

    companion object {

        val REQUEST_CODE = 0x0000c0de // Only use bottom 16 bits
        private val TAG = com.google.zxing.integration.android.IntentIntegrator::class.java.simpleName

        // supported barcode formats
        val PRODUCT_CODE_TYPES: Collection<String> = list("UPC_A", "UPC_E", "EAN_8", "EAN_13", "RSS_14")
        val ONE_D_CODE_TYPES: Collection<String> = list("UPC_A", "UPC_E", "EAN_8", "EAN_13", "CODE_39", "CODE_93", "CODE_128",
                "ITF", "RSS_14", "RSS_EXPANDED")
        val QR_CODE_TYPES: Collection<String> = setOf("QR_CODE")
        val DATA_MATRIX_TYPES: Collection<String> = setOf("DATA_MATRIX")

        val ALL_CODE_TYPES: Collection<String>? = null

        /**
         * @param fragment [Fragment] invoking the integration.
         * [.startActivityForResult] will be called on the [Fragment] instead
         * of an [Activity]
         */
        fun forSupportFragment(fragment: android.support.v4.app.Fragment): IntentIntegrator {
            val integrator = IntentIntegrator(fragment.activity as Activity)
            integrator.supportFragment = fragment
            return integrator
        }

        /**
         * @param fragment [Fragment] invoking the integration.
         * [.startActivityForResult] will be called on the [Fragment] instead
         * of an [Activity]
         */
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        fun forFragment(fragment: Fragment): IntentIntegrator {
            val integrator = IntentIntegrator(fragment.activity)
            integrator.fragment = fragment
            return integrator
        }

        /**
         *
         * Call this from your [Activity]'s
         * [Activity.onActivityResult] method.
         *
         * @param requestCode request code from `onActivityResult()`
         * @param resultCode  result code from `onActivityResult()`
         * @param intent      [Intent] from `onActivityResult()`
         * @return null if the event handled here was not related to this class, or
         * else an [IntentResult] containing the result of the scan. If the user cancelled scanning,
         * the fields will be null.
         */
        fun parseActivityResult(requestCode: Int, resultCode: Int, intent: Intent): IntentResult? {
            if (requestCode == REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    val contents = intent.getStringExtra(Intents.Scan.RESULT)
                    val formatName = intent.getStringExtra(Intents.Scan.RESULT_FORMAT)
                    val rawBytes = intent.getByteArrayExtra(Intents.Scan.RESULT_BYTES)
                    val intentOrientation = intent.getIntExtra(Intents.Scan.RESULT_ORIENTATION, 90)
                    val orientation = if (intentOrientation == Integer.MIN_VALUE) null else intentOrientation
                    val errorCorrectionLevel = intent.getStringExtra(Intents.Scan.RESULT_ERROR_CORRECTION_LEVEL)
                    val barcodeImagePath = intent.getStringExtra(Intents.Scan.RESULT_BARCODE_IMAGE_PATH)
                    return IntentResult(contents,
                            formatName,
                            rawBytes,
                            90,
                            errorCorrectionLevel,
                            barcodeImagePath)
                }
                return IntentResult()
            }
            return null
        }

        private fun list(vararg values: String): List<String> {
            return Collections.unmodifiableList(Arrays.asList(*values))
        }
    }
}

