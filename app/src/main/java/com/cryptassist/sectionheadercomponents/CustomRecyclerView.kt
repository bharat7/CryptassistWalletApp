package com.cryptassist.sectionheadercomponents

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.widget.ListAdapter
import android.widget.ListView

class CustomRecyclerView : ListView {

    private var mAdapter: PinnedHeaderAdapter? = null
    private var mHeaderView: View? = null
    private var mHeaderViewVisible: Boolean = false
    private var mHeaderViewWidth: Int = 0
    private var mHeaderViewHeight: Int = 0

    interface PinnedHeaderAdapter {

        fun getPinnedHeaderState(position: Int): Int

        fun configurePinnedHeader(header: View, position: Int, alpha: Int)

        companion object {
            val PINNED_HEADER_GONE = 0
            val PINNED_HEADER_VISIBLE = 1
            val PINNED_HEADER_PUSHED_UP = 2
        }
    }

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (mHeaderView != null) {
            mHeaderView!!.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight)
            configureHeaderView(firstVisiblePosition)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (mHeaderView != null) {
            measureChild(mHeaderView, widthMeasureSpec, heightMeasureSpec)
            mHeaderViewWidth = mHeaderView!!.measuredWidth
            mHeaderViewHeight = mHeaderView!!.measuredHeight
        }
    }

    fun setPinnedHeaderView(view: View) {
        mHeaderView = view
        if (mHeaderView != null) {
            setFadingEdgeLength(0)
        }
        requestLayout()
    }

    override fun setAdapter(adapter: ListAdapter) {
        super.setAdapter(adapter)
        mAdapter = adapter as PinnedHeaderAdapter
    }

    fun configureHeaderView(position: Int) {
        if (mHeaderView == null) {
            return
        }
        val state = mAdapter!!.getPinnedHeaderState(position)
        when (state) {
            PinnedHeaderAdapter.PINNED_HEADER_GONE -> {
                mHeaderViewVisible = false
            }

            PinnedHeaderAdapter.PINNED_HEADER_VISIBLE -> {
                mAdapter!!.configurePinnedHeader(mHeaderView!!, position, MAX_ALPHA)
                if (mHeaderView!!.top != 0) {
                    mHeaderView!!.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight)
                }
                mHeaderViewVisible = true
            }

            PinnedHeaderAdapter.PINNED_HEADER_PUSHED_UP -> {
                val firstView = getChildAt(0)
                val bottom = firstView.bottom
                val headerHeight = mHeaderView!!.height
                val y: Int
                val alpha: Int
                if (bottom < headerHeight) {
                    y = bottom - headerHeight
                    alpha = MAX_ALPHA * (headerHeight + y) / headerHeight
                } else {
                    y = 0
                    alpha = MAX_ALPHA
                }
                mAdapter!!.configurePinnedHeader(mHeaderView!!, position, alpha)
                if (mHeaderView!!.top != y) {
                    mHeaderView!!.layout(0, y, mHeaderViewWidth, mHeaderViewHeight + y)
                }
                mHeaderViewVisible = true
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        if (mHeaderViewVisible) {
            drawChild(canvas, mHeaderView, drawingTime)
        }
    }

    companion object {

        private val MAX_ALPHA = 255
    }
}
