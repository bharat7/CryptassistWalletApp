package com.cryptassist.sectionheadercomponents

import android.graphics.Color
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.view.View
import android.widget.TextView


abstract class IndexedPinnedHeaderListViewAdapter : BasePinnedHeaderListViewAdapter() {
    private var _pinnedHeaderBackgroundColor: Int = 0
    private var _pinnedHeaderTextColor: Int = 0

    fun setPinnedHeaderBackgroundColor(pinnedHeaderBackgroundColor: Int) {
        _pinnedHeaderBackgroundColor = pinnedHeaderBackgroundColor
    }

    fun setPinnedHeaderTextColor(pinnedHeaderTextColor: Int) {
        _pinnedHeaderTextColor = pinnedHeaderTextColor
    }

    override fun getSectionTitle(sectionIndex: Int): CharSequence {
        return sections[sectionIndex].toString()
    }

    override fun configurePinnedHeader(v: View, position: Int, alpha: Int) {
        val header = v as TextView
        val sectionIndex = getSectionForPosition(position)
        val sections = sections
        if (sections != null && sections.size != 0) {
            val title = getSectionTitle(sectionIndex)
            header.text = title
            header.setTextColor(Color.WHITE)
        }
        if (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB)
            if (alpha == 255) {
                header.setBackgroundColor(_pinnedHeaderBackgroundColor)
                header.setTextColor(Color.WHITE)
            } else {
                header.setBackgroundColor(Color.argb(alpha, Color.red(_pinnedHeaderBackgroundColor),
                        Color.green(_pinnedHeaderBackgroundColor), Color.blue(_pinnedHeaderBackgroundColor)))
                header.setTextColor(Color.WHITE)
            }
        else {
            header.setBackgroundColor(_pinnedHeaderBackgroundColor)
            header.setTextColor(Color.WHITE)
            header.alpha = alpha / 255.0f
        }
    }

}
