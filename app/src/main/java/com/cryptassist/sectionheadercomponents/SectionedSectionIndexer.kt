package com.cryptassist.sectionheadercomponents

import android.widget.SectionIndexer

open class SectionedSectionIndexer(private val mSectionArray: Array<SimpleSection>) : SectionIndexer {

    val itemsCount: Int
        get() = if (mSectionArray.size == 0) 0 else mSectionArray[mSectionArray.size - 1].endIndex + 1

    init {
        //
        var previousIndex = 0
        for (i in mSectionArray.indices) {
            mSectionArray[i].startIndex = previousIndex
            previousIndex += mSectionArray[i].itemsCount
            mSectionArray[i].endIndex = previousIndex - 1
        }
    }

    override fun getPositionForSection(section: Int): Int {
        return if (section < 0 || section >= mSectionArray.size) -1 else mSectionArray[section].startIndex
    }

    /** given a flat position, returns the position within the section  */
    fun getPositionInSection(flatPos: Int): Int {
        val sectionForPosition = getSectionForPosition(flatPos)
        val simpleSection = mSectionArray[sectionForPosition]
        return flatPos - simpleSection.startIndex
    }

    override fun getSectionForPosition(flatPos: Int): Int {
        if (flatPos < 0)
            return -1
        var start = 0
        var end = mSectionArray.size - 1
        var piv = (start + end) / 2
        while (true) {
            val section = mSectionArray[piv]
            if (flatPos >= section.startIndex && flatPos <= section.endIndex)
                return piv
            if (piv == start && start == end)
                return -1
            if (flatPos < section.startIndex)
                end = piv - 1
            else
                start = piv + 1
            piv = (start + end) / 2
        }
    }

    override fun getSections(): Array<SimpleSection> {
        return mSectionArray
    }

    fun getItem(flatPos: Int): Any {
        val sectionIndex = getSectionForPosition(flatPos)
        val section = mSectionArray[sectionIndex]
        return section.getItem(flatPos - section.startIndex)
    }

    fun getItem(sectionIndex: Int, positionInSection: Int): Any {
        val section = mSectionArray[sectionIndex]
        return section.getItem(positionInSection)
    }

    fun getRawPosition(sectionIndex: Int, positionInSection: Int): Int {
        val section = mSectionArray[sectionIndex]
        return section.startIndex + positionInSection
    }

    // /////////////////////////////////////////////
    // Section //
    // //////////
    abstract class SimpleSection {
        var name: String? = null
        var startIndex: Int = 0
        var endIndex: Int = 0

        abstract val itemsCount: Int

        constructor() {}

        constructor(sectionName: String) {
            this.name = sectionName
        }

        abstract fun getItem(posInSection: Int): Any

        override fun toString(): String {
            return name!!
        }
    }

}
