package com.cryptassist.sectionheadercomponents

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.media.ThumbnailUtils
import android.os.Build
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.TextView
import android.widget.ViewSwitcher

import com.cryptassist.R


class CircularContactView @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
constructor(context: Context, attrs: AttributeSet?) : ViewSwitcher(context, attrs) {
    //  private static final int DEFAULT_CONTENT_SIZE_IN_DP=20;
    var imageView: ImageView? = null
        private set
    var textView: TextView? = null
        private set
    private var mBitmap: Bitmap? = null
    private var mText: CharSequence? = null
    private var mBackgroundColor = 0
    private var mImageResId = 0
    private var mContentSize: Int = 0

    constructor(context: Context) : this(context, null) {}

    init {
        addView(ImageView(context), FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER))
        addView(TextView(context), FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER))
        textView!!.gravity = Gravity.CENTER
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            textView!!.setAllCaps(true)
        mContentSize = resources.getDimensionPixelSize(R.dimen.list_item__contact_imageview_size)
        if (isInEditMode)
            setTextAndBackgroundColor("", -0x10000)
    }

    fun setContentSize(contentSize: Int) {
        this.mContentSize = contentSize
    }

    private fun drawContent(viewWidth: Int, viewHeight: Int) {
        var roundedBackgroundDrawable: ShapeDrawable? = null
        if (mBackgroundColor != 0) {
            roundedBackgroundDrawable = ShapeDrawable(OvalShape())
            roundedBackgroundDrawable.paint.color = mBackgroundColor
            roundedBackgroundDrawable.intrinsicHeight = viewHeight
            roundedBackgroundDrawable.intrinsicWidth = viewWidth
            roundedBackgroundDrawable.bounds = Rect(0, 0, viewWidth, viewHeight)
        }
        if (mImageResId != 0) {
            imageView!!.setBackgroundDrawable(roundedBackgroundDrawable)
            imageView!!.setImageResource(mImageResId)
            imageView!!.scaleType = ScaleType.CENTER_INSIDE
        } else if (mText != null) {
            textView!!.text = mText
            textView!!.setBackgroundDrawable(roundedBackgroundDrawable)
            textView!!.setTextSize(TypedValue.COMPLEX_UNIT_PX, (viewHeight / 2).toFloat())
        } else if (mBitmap != null) {
            imageView!!.scaleType = ScaleType.FIT_CENTER
            imageView!!.setBackgroundDrawable(roundedBackgroundDrawable)
            if (mBitmap!!.width != mBitmap!!.height)
                mBitmap = ThumbnailUtils.extractThumbnail(mBitmap, viewWidth, viewHeight)
            val roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(resources,
                    mBitmap)
            roundedBitmapDrawable.cornerRadius = ((mBitmap!!.width + mBitmap!!.height) / 4).toFloat()
            imageView!!.setImageDrawable(roundedBitmapDrawable)
        }
        resetValuesState(false)
    }

    fun setTextAndBackgroundColor(text: CharSequence, backgroundColor: Int) {
        resetValuesState(true)
        while (currentView !== textView)
            showNext()
        this.mBackgroundColor = backgroundColor
        mText = text
        drawContent(mContentSize, mContentSize)
    }

    fun setImageResource(imageResId: Int, backgroundColor: Int) {
        resetValuesState(true)
        while (currentView !== imageView)
            showNext()
        mImageResId = imageResId
        this.mBackgroundColor = backgroundColor
        drawContent(mContentSize, mContentSize)
    }

    fun setImageBitmap(bitmap: Bitmap) {
        setImageBitmapAndBackgroundColor(bitmap, 0)
    }

    fun setImageBitmapAndBackgroundColor(bitmap: Bitmap, backgroundColor: Int) {
        resetValuesState(true)
        while (currentView !== imageView)
            showNext()
        this.mBackgroundColor = backgroundColor
        mBitmap = bitmap
        drawContent(mContentSize, mContentSize)
    }

    private fun resetValuesState(alsoResetViews: Boolean) {
        mImageResId = 0
        mBackgroundColor = mImageResId
        mBitmap = null
        mText = null
        if (alsoResetViews) {
            textView!!.text = null
            textView!!.setBackgroundDrawable(null)
            imageView!!.setImageBitmap(null)
            imageView!!.setBackgroundDrawable(null)
        }
    }

}
