package com.cryptassist


import android.content.Context
import android.os.StrictMode
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDialog
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.text.TextUtils
import android.widget.TextView
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.app.Activity
import android.view.View
import android.widget.ImageView


//import dagger.android.AndroidInjector
//import dagger.android.DaggerApplication



class CryptassistApplication : MultiDexApplication()/* DaggerApplication() */{

    /*override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }*/

    override fun onCreate() {
        super.onCreate()
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }



    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(applicationContext)
    }


}
