package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.view.LayoutInflater
import com.cryptassist.R
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.databinding.RowSettingListBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SuggestCoinViewModel(private var context: Context,var dialog: Dialog)  {
    var coinName : ObservableField<String> = ObservableField()
    var coinAmnt : ObservableField<String> = ObservableField()
    var coinNameError : ObservableField<String> = ObservableField()
    var coinAmntError : ObservableField<String> = ObservableField()
    lateinit var prefManager:PrefManager
    private var disposable: Disposable? = null

    public fun onClick() {
        if (coinName.get().isNullOrBlank()){
            coinNameError.set(context.getString(R.string.please_enter_coin_name))
        }
        else if (coinAmnt.get().isNullOrBlank()){
            coinAmntError.set(context.getString(R.string.please_enter_coin_amnt))
        }
        else{
            dialog.dismiss()
            suggestCoinApi()

        }
    }

    private val apiClient by lazy {
        APIExecutor.clientApi()
    }


    private fun suggestCoinApi(){
        prefManager=PrefManager.getInstance(context)
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", prefManager.getPreference(AppConstants.USER_ID,""))
        jsonObject.addProperty("coinName", coinName.get())
        jsonObject.addProperty("amount", coinAmnt.get())
        disposable = apiClient.coinListing(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            if (result.responseCode == 200) {
                                val layoutInflater: LayoutInflater = LayoutInflater.from(context as Context)
                                val dialogPopUpBinding = DataBindingUtil.inflate<DialogPopUpBinding>(layoutInflater, R.layout.dialog_pop_up, null, false)
                                val dialogUtils = DialogUtils.createCustomDialog(context as Context, dialogPopUpBinding.root)
                                val popUpDialogViewModel = PopUpDialogViewModel(context as Activity, dialogUtils, context.getString(R.string.coin_suggestion_succefully), "")
                                dialogPopUpBinding.onClickOk = popUpDialogViewModel
                                dialogUtils.show()

                            } else ToastUtils.showToastShort(context, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(context, error.message!!) },
                        { disposable?.dispose() }
                )

    }


}


