package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.BaseObservable
import com.cryptassist.R
import com.cryptassist.activity.ValidatePassPhraseActivity
import com.cryptassist.model.PreferredCurrencyModel
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import java.io.Serializable

class PassphraseViewModel : BaseObservable, Serializable {
    private lateinit var activity: Activity
    var isSelected: Boolean = false
    var viaLocales: ArrayList<String> = arrayListOf()/*arrayOf("Mental", "you", "recovery", "In", "Perpendicular", "tricky", "limb", "smart", "dad", "voice", "ejaculate", "jealous")*/

    lateinit var mCountryName: String
    val preferredLanguageModels: ArrayList<PassphraseViewModel>
        get() {
            val preferredLanguageModels = ArrayList<PassphraseViewModel>()

            for (i in viaLocales.indices) {
                val preferredLanguageModel = PassphraseViewModel(PreferredCurrencyModel(viaLocales[i]))
                preferredLanguageModels.add(preferredLanguageModel)
            }
            return preferredLanguageModels
        }

    constructor(activity: Activity) {
        this.activity = activity
    }


    fun getCountryName(): String {
        return mCountryName

    }

    fun setCountryName(countryName: String) {
        this.mCountryName = countryName
        notifyPropertyChanged(R.id.tvCountry)
    }


    constructor(preferedLanguageModel: PreferredCurrencyModel) {
        this.mCountryName = preferedLanguageModel.countryName
    }

    fun onClick() {
        CommonUtils.hideKeyboard(activity)
        val intent = Intent(activity, ValidatePassPhraseActivity::class.java)
        intent.putStringArrayListExtra(AppConstants.VALIDATE_PHRASE_LIST, viaLocales)
        activity.startActivity(intent)
        activity.finish()

    }

    private fun onSelected(): Boolean {
        isSelected = true
        return isSelected
    }

}
