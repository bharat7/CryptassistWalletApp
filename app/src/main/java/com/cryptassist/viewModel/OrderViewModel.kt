package com.cryptassist.viewModel

import android.databinding.ObservableField
import com.google.gson.annotations.SerializedName

data class OrderViewModel (
        val id: ObservableField<String>? = ObservableField(),
        @SerializedName("user_id") val userId: ObservableField<String>? = ObservableField(),
        @SerializedName("parent_order_id") val parentOrderId: ObservableField<String>? = ObservableField(),
        val status: ObservableField<String>? = ObservableField(),
        val type: ObservableField<String>? = ObservableField(),
        @SerializedName("payment_type") val paymentType:ObservableField<String>? = ObservableField(),
        @SerializedName("sender_sent") val senderSent: ObservableField<String>? = ObservableField(),
        @SerializedName("visible_rate") val visibleRate: ObservableField<String>? = ObservableField(),
        @SerializedName("real_rate") val realRate: ObservableField<String>? = ObservableField(),
        @SerializedName("sender_receive") val senderReceive: ObservableField<String>? = ObservableField(),
        @SerializedName("sender_receive_total") val senderReceiveTotal: ObservableField<String>? = ObservableField(),
        val payload: ObservableField<String>? = ObservableField(),
        @SerializedName("order_history") val orderHistory: ObservableField<String>? = ObservableField(),
        val note: ObservableField<String>? = ObservableField(),
        @SerializedName("trf_address") val trfAddress: ObservableField<String>? = ObservableField(),
        val txid: ObservableField<String>? = ObservableField(),
        @SerializedName("created_at") val createdAt: ObservableField<String>? = ObservableField(),
        @SerializedName("updated_at") val updatedAt: ObservableField<String>? = ObservableField()

    )