package com.cryptassist.viewModel

import android.databinding.BaseObservable

import com.cryptassist.model.TransactionModel

import java.util.ArrayList

class TransactionViewModel : BaseObservable {

    lateinit var amount: String
    lateinit var userName: String
    lateinit var date: String
    val preferredLanguageModels: ArrayList<TransactionViewModel>
        get() {
            val preferedLanguageModels = ArrayList<TransactionViewModel>()
            val preferedLanguageModel = TransactionViewModel(TransactionModel("Vivek giri", "0.0000578 CTA", "july20, 2018"))
            val preferedLanguageModel1 = TransactionViewModel(TransactionModel("Vivek giri", "0.0000578 CTA", "july20, 2018"))
            val preferedLanguageMode2 = TransactionViewModel(TransactionModel("Vivek giri", "0.0000578 CTA", "july20, 2018"))
            val preferedLanguageMode3 = TransactionViewModel(TransactionModel("Vivek giri", "0.0000578 CTA", "july20, 2018"))
            preferedLanguageModels.add(preferedLanguageModel)
            preferedLanguageModels.add(preferedLanguageMode2)
            preferedLanguageModels.add(preferedLanguageMode3)
            preferedLanguageModels.add(preferedLanguageModel1)
            return preferedLanguageModels
        }

    constructor()


    constructor(transactionModel: TransactionModel) {
        this.amount = transactionModel.amount
        this.userName = transactionModel.userName
        this.date = transactionModel.date
    }

}
