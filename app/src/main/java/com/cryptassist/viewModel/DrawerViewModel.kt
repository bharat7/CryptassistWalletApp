package com.cryptassist.viewModel

import android.app.Activity
import android.content.Context
import android.databinding.BaseObservable
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat

import com.cryptassist.commandintefaces.DrawerOptionSelected
import com.cryptassist.R


import java.util.ArrayList
import java.util.Arrays

class DrawerViewModel : BaseObservable {
    private val activity: Activity? = null
    private lateinit var  listener: DrawerOptionSelected


   lateinit var optionName: String

   lateinit var optionImage: Drawable

    constructor()

    constructor(listener: DrawerOptionSelected) {

        this.listener = listener
    }


    constructor(optionName: String, optionImage: Drawable) {
        this.optionName = optionName
        this.optionImage = optionImage

    }

    fun getPreferredLanguageModels(context: Context): ArrayList<DrawerViewModel> {
        val drawerViewModel = ArrayList<DrawerViewModel>()
        val itemNameList = ArrayList<String>()
        itemNameList.addAll(Arrays.asList(*context.resources.getStringArray(R.array.ld_activityScreenTitles)))
        val itemImageList = ArrayList<String>()
        itemImageList.addAll(Arrays.asList(*context.resources.getStringArray(R.array.ld_activityScreenIcons)))
        val icons = loadScreenIcons(context)
        for (i in itemNameList.indices) {
            val drawerViewModel1 = DrawerViewModel(itemNameList[i], icons[i]!!)
            drawerViewModel.add(drawerViewModel1)
        }

        return drawerViewModel
    }

    private fun loadScreenIcons(context: Context): Array<Drawable?> {
        val ta = context.resources.obtainTypedArray(R.array.ld_activityScreenIcons)
        val icons = arrayOfNulls<Drawable>(ta.length())
        for (i in 0 until ta.length()) {
            val id = ta.getResourceId(i, 0)
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(context, id)
            }
        }
        ta.recycle()
        return icons
    }

    fun onOptionClick() {

    }
}
