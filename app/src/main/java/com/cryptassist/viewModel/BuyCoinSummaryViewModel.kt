package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.cryptassist.R
import com.cryptassist.activity.BuyCoinExchangeActivity
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.databinding.RowSettingListBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class BuyCoinSummaryViewModel(private var activity: Activity)  {
    var coinAmntSend : ObservableField<String> = ObservableField()
    var coinAmntReceived : ObservableField<String> = ObservableField()
    var ctaAddress : ObservableField<String> = ObservableField()
    lateinit var prefManager:PrefManager
    private var disposable: Disposable? = null

    private val apiClient by lazy {
        APIExecutor.clientApi()
    }

     fun onClick() {



    }

    fun onClickCopy() {
        val clipboard = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("CTA Address", ctaAddress.get())
        clipboard.primaryClip = clip
        Toast.makeText(activity, "Address Copied", Toast.LENGTH_SHORT).show()

    }

    private fun submitOrder(){
        CommonUtils.showProgressDialog(activity)
        prefManager= PrefManager.getInstance(activity)
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_id", prefManager.getPreference(AppConstants.USER_ID,""))
        jsonObject.addProperty("payment_type", "")
        jsonObject.addProperty("sender_sent", "")
        jsonObject.addProperty("visible_rate", "")
        jsonObject.addProperty("txid", "")
        disposable = apiClient.buyCoinApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(activity)
                            if (result.responseCode == 200) {
                                val layoutInflater: LayoutInflater = LayoutInflater.from(activity as Context)
                                val dialogPopUpBinding = DataBindingUtil.inflate<DialogPopUpBinding>(layoutInflater, R.layout.dialog_pop_up, null, false)
                                dialogPopUpBinding.tvTitle.visibility=View.VISIBLE
                                val dialogUtils = DialogUtils.createCustomDialog(activity as Context, dialogPopUpBinding.root)
                                val popUpDialogViewModel = PopUpDialogViewModel(activity as Activity, dialogUtils, activity.getString(R.string.order_place_successfully), activity.getString(R.string.thank_you))
                                dialogPopUpBinding.onClickOk = popUpDialogViewModel
                                dialogUtils.show()

                            } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(activity, error.message!!)
                            CommonUtils.hideProgressDialog(activity)},
                        { disposable?.dispose() }
                )

    }


}


