package com.cryptassist.viewModel

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField

object DataRepository {
    private val data = MutableLiveData<String>()

    fun getData(ctaAddress: ObservableField<String>): MutableLiveData<String> {
        data.value = ctaAddress.get()
        return data
    }
}
