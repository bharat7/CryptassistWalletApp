package com.cryptassist.viewModel

import android.app.Activity
import android.databinding.*
import android.opengl.Visibility
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.cryptassist.R
import com.cryptassist.adapter.ProfileAddressAdapter
import com.cryptassist.databinding.ActivityProfileBinding
import com.cryptassist.imageutils.TakePictureUtilsOld
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CheckPermission
import io.fabric.sdk.android.services.common.CommonUtils


class ProfileAddressViewModel {
    var mBinding: ActivityProfileBinding?=null
    lateinit var prefManager: PrefManager
    private var activity: Activity
    public var imageName: String? = null
    var imagePath: ObservableField<String> = ObservableField()
    var myAddress: ObservableField<String> = ObservableField()
    var myName: ObservableField<String> = ObservableField()
    var visibility: ObservableField<Boolean> = ObservableField()
    var isEdit: ObservableField<Boolean> = ObservableField()
    public var profileAddressAdapter: ProfileAddressAdapter? = null
    public lateinit var profileAddressListViewModel: ProfileAddressListViewModel
    var ctaAddressesList: ObservableArrayList<ProfileAddressListViewModel> = ObservableArrayList()
    private var viaLocales = arrayOf("16gyuuy67jhg67dfr67yt65tr5ki876655u", "17gyuuy67jhg67dfr67yt65tr5ki876655u", "18gyuuy67jhg67dfr67yt65tr5ki876655u", "19gyuuy67jhg67dfr67yt65tr5ki876655u", "15gyuuy67jhg67dfr67yt65tr5ki876655u")


    constructor(activity: Activity,mBinding: ActivityProfileBinding) {
        this.activity = activity
        this.mBinding = mBinding
        prefManager=PrefManager.getInstance(activity)
        myAddress.set(activity.resources.getString(R.string.cta_address))
        if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.USER_NAME, ""))) {
            myName.set(prefManager.getPreference(AppConstants.USER_NAME))
        }
        else {
            myName.set(activity.resources.getString(R.string.app_name))

        }
        if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.USER_IMAGE, ""))) {
            imagePath.set(prefManager.getPreference(AppConstants.USER_IMAGE))
        }
        else {
            imagePath.set("")

        }
        isEdit.set(false)
        addListAdapter(activity)

    }


    fun onEditClick() {
        com.cryptassist.utils.CommonUtils.hideKeyboard(activity)
        if (isEdit.get()==false) {
            isEdit.set(true)
            visibility.set(true)
        }
        else{
            isEdit.set(false)
            visibility.set(false)
        }
        prefManager.savePreference(AppConstants.USER_NAME,myName.get())
        prefManager.savePreference(AppConstants.USER_IMAGE,imagePath.get())
    }

    fun openCamera() {
       if (CheckPermission.checkCameraPermission(activity)){
           addPhotoDialog()
       }
        else {
           CheckPermission.requestCameraPermission(activity)
       }
    }

    fun onBackPressed() {
       activity.finish()
    }


    fun addListAdapter(activity: Activity) {
        for (i in viaLocales.indices) {
            profileAddressListViewModel = ProfileAddressListViewModel(activity,this)
            profileAddressListViewModel.mAddress.set(viaLocales[i])
            ctaAddressesList.add(profileAddressListViewModel)

        }
    }
    protected fun addPhotoDialog() {
        val items = arrayOf<CharSequence>(activity!!.getString(R.string.take_photo), activity!!.getString(R.string.choose_from_lib), activity!!.getString(R.string.cancel))

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity!!.getString(R.string.add_photo))
        builder.setItems(items) { dialog, item ->
            if (items[item] == activity!!.getString(R.string.take_photo)) {
                try {
                    //                                        dialog.dismiss();
                    imageName = System.currentTimeMillis().toString()
                    TakePictureUtilsOld.takePicture(activity,imageName!!)
                } catch (e: Exception) {
                    e.printStackTrace()

                }

            } else if (items[item] == activity!!.getString(R.string.choose_from_lib)) {
                imageName = "delivery_picture"
                dialog.dismiss()
                imageName = "picture_"  + System.currentTimeMillis()
                TakePictureUtilsOld.openGallery((activity as Activity?)!!)

            } else {
                dialog.dismiss()
            }
        }
        builder.create().show()
        builder.setCancelable(true)
    }



    }



