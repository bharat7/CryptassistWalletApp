package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import com.cryptassist.R
import com.cryptassist.activity.*
import com.cryptassist.utils.AppConstants


class PopUpDialogViewModel {
    private var dialog: Dialog? = null
    private var activity: Activity? = null
    private lateinit var name: String
    private lateinit var address: String
    private lateinit var email: String
    private lateinit var imagePath: String

    var message: String? = null
    var title: String? = null

    constructor(activity: Activity, dialog: Dialog, message: String, title: String) {
        this.dialog = dialog
        this.activity = activity
        this.message = message
        this.title = title

    }

    constructor(activity: Activity, dialog: Dialog, message: String, title: String, name: String, address: String, email: String, imagePath: String) {
        this.name = name
        this.address = address
        this.email = email
        this.dialog = dialog
        this.activity = activity
        this.message = message
        this.title = title
        this.imagePath = imagePath
    }


    fun dismissDialog() {
        try {
            if (activity != null && activity is AddContactActivity) {
                val intent = Intent(activity, ContactDetailActivity::class.java)
                intent.putExtra(AppConstants.USER_NAME, name)
                intent.putExtra(AppConstants.USER_ADDRESS, address)
                intent.putExtra(AppConstants.USER_EMAIL, email)
                intent.putExtra(AppConstants.USER_IMAGE, imagePath)
                activity!!.startActivity(intent)
                activity!!.finish()
            }
            else   if (activity != null && activity is PreferredLanguageActivity) {
                val intent=Intent(activity,DashboardActivity::class.java)
                intent.putExtra("openSettingFragment","openSettingFragment")
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                activity!!.startActivity(intent)
                activity!!.finish()
            }
            else if (activity != null && message!!.equals(activity!!.getString(R.string.your_wallet_successfully_backup), ignoreCase = true) && activity is ValidatePassPhraseActivity) {
                activity!!.finish()
            }
            else if (activity != null && activity is BuyCoinSummaryActivity) {
                activity!!.finish()
            }
            dialog!!.dismiss()
        } catch (e: Exception) {
        }

    }
}
