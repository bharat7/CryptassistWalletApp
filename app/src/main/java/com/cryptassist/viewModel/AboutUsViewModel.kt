package com.cryptassist.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField

class AboutUsViewModel(application: Application) : AndroidViewModel(application) {

    val aboutUs: ObservableField<String> = ObservableField()

}