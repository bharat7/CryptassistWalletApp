package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.graphics.Bitmap
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater

import com.cryptassist.BR
import com.cryptassist.R
import com.cryptassist.databinding.ActivityAddContactBinding
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.model.AddContactModel
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils


class AddContactViewModel : BaseObservable {
    private lateinit var context: Activity
    private lateinit var dialogPopUpBinding: DialogPopUpBinding
    private lateinit var dialogUtils: Dialog
    private var popUpDialogViewModel: PopUpDialogViewModel? = null

    //    private AddContactModel addContactModel=new AddContactModel();
    private lateinit var activityAddContactBinding: ActivityAddContactBinding

//    lateinit var mEmail: String

    var mName: ObservableField<String> = ObservableField()
    var mAddress: ObservableField<String> = ObservableField()
    var photoId: ObservableField<String> = ObservableField()
    var mEmail: ObservableField<String> = ObservableField()
    var userImage: ObservableField<String> = ObservableField()
    var isFromSend: ObservableField<Boolean> = ObservableField()
    var profileImage:String?=null

//    lateinit var mAddress: String

//    lateinit var photoId: String


    val nameWatcher: TextWatcher
        get() = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                activityAddContactBinding.tiName.isErrorEnabled = false
                activityAddContactBinding.tiEmail.isErrorEnabled = false
                activityAddContactBinding.tiAddress.isErrorEnabled = false

            }

        }


    constructor(context: Activity, activityAddContactBinding: ActivityAddContactBinding) {
        isFromSend.set(false)
        this.context = context
        this.activityAddContactBinding = activityAddContactBinding
        val layoutInflater = LayoutInflater.from(context)
        dialogPopUpBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_pop_up, null, false)
        dialogUtils = DialogUtils.createCustomDialog(context, dialogPopUpBinding.root)
      if (context.intent!=null&& context.intent.getStringExtra(AppConstants.CTA_ADDRESS)!=null){
          mAddress.set(context.intent.getStringExtra(AppConstants.CTA_ADDRESS))
      }
    }

    constructor()


    fun onSaveClick() {
        try {
            if (TextUtils.isEmpty(mName.get())) {
                activityAddContactBinding.tiName.error = context.getString(R.string.please_enter_name)

            } else if (TextUtils.isEmpty(mEmail.get())) {
                activityAddContactBinding.tiEmail.error =  context.getString(R.string.please_enter_email)
                activityAddContactBinding.etEmail.requestFocus()

            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail.get()).matches()) {
                activityAddContactBinding.tiEmail.error = context.getString(R.string.please_enter_valid_email)

            } else if (TextUtils.isEmpty(mAddress.get())) {
                activityAddContactBinding.tiAddress.error = context.getString(R.string.please_enter_address)
                activityAddContactBinding.etAddress.requestFocus()

            } else {
                popUpDialogViewModel = PopUpDialogViewModel(context, dialogUtils, context.getString(R.string.contact_added_successfully), "", mName.get()!!, mAddress.get()!!, mEmail.get()!!, userImage.get().toString())
                dialogPopUpBinding.onClickOk = popUpDialogViewModel
                dialogUtils.show()

                val viewModel = AddressViewModel(context.application)
                val contact = AddContactModel()
                contact.address = mAddress.get().toString()
                contact.name = mName.get().toString()
                contact.email = mEmail.get().toString()
                contact.firstLetterName = mName.get().toString()
                contact.imagePath=userImage.get().toString()
                viewModel.insert(contact)

            }
        } catch (e: Exception) {
        }
    }


    /*fun onEmailTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        activityAddContactBinding.tiName.isEnabled = false
    }*/

    //    public void onClickSave(){
    //        Intent intent=new Intent(context, ScannerActivity.class);
    //        context.startActivity(intent);
    //    }
}
