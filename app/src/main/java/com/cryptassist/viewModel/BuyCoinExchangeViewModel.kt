package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.view.LayoutInflater
import com.cryptassist.R
import com.cryptassist.activity.BuyCoinExchangeActivity
import com.cryptassist.activity.BuyCoinSummaryActivity
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.databinding.RowSettingListBinding
import com.cryptassist.utils.DialogUtils

class BuyCoinExchangeViewModel(private var activity: Activity)  {
    var coinAmntSend : ObservableField<String> = ObservableField()
    var coinAmntReceived : ObservableField<String> = ObservableField()


    public fun onClick() {
        if (coinAmntSend.get().isNullOrBlank()){
            val layoutInflater: LayoutInflater = LayoutInflater.from(activity as Context)
            val dialogPopUpBinding = DataBindingUtil.inflate<DialogPopUpBinding>(layoutInflater, R.layout.dialog_pop_up, null, false)
            val dialogUtils = DialogUtils.createCustomDialog(activity as Context, dialogPopUpBinding.root)
            val popUpDialogViewModel = PopUpDialogViewModel(activity as Activity, dialogUtils, activity.getString(R.string.enter_coin_amont), "")
            dialogPopUpBinding.onClickOk = popUpDialogViewModel
            dialogUtils.show()
        }

        else{
            var intent = Intent(activity, BuyCoinSummaryActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
        }
    }


}


