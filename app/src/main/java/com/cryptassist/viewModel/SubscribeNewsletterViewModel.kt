package com.cryptassist.viewModel

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.ObservableField
import android.text.TextUtils
import android.util.Log
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.activity.OtpActivity
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.databinding.RowSettingListBinding
import com.cryptassist.fragment.SendFragment
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SubscribeNewsletterViewModel {
    var dialog: Dialog
    var mContext: Context
    var fragmentNewsletterDialogBinding: FragmentNewsletterDialogBinding
   lateinit var settingListBinding: RowSettingListBinding
    var position: Int

    val mEmail: ObservableField<String> = ObservableField()
    val mTitle: ObservableField<String> = ObservableField()
    private var disposable: Disposable? = null
    lateinit var  prefManager : PrefManager

    constructor(mContext: Context, dialog: Dialog, fragmentNewsletterDialogBinding: FragmentNewsletterDialogBinding, settingListBinding: RowSettingListBinding,  position: Int){
        this.mContext=mContext
        this.dialog=dialog
        this.fragmentNewsletterDialogBinding=fragmentNewsletterDialogBinding
        this.settingListBinding=settingListBinding
        prefManager=PrefManager.getInstance(mContext)
        this.position=position
        mTitle.set(mContext.resources.getString(R.string.subscribe_newsletter_email))

    }

    constructor(mContext: Context, dialog: Dialog, fragmentNewsletterDialogBinding: FragmentNewsletterDialogBinding, position: Int){
        this.mContext=mContext
        this.dialog=dialog
        this.fragmentNewsletterDialogBinding=fragmentNewsletterDialogBinding
        prefManager=PrefManager.getInstance(mContext)
        this.position=position
        mTitle.set(mContext.resources.getString(R.string.enter_email))

    }

    private val apiClient by lazy {
        APIExecutor.clientApi()
    }


    fun onClick() {
        if (TextUtils.isEmpty(fragmentNewsletterDialogBinding.etEmail.text.toString())) {
            fragmentNewsletterDialogBinding.etEmail.error = mContext.getString(R.string.please_enter_email)
            fragmentNewsletterDialogBinding.etEmail.requestFocus()

        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(fragmentNewsletterDialogBinding.etEmail.text.toString()).matches()) {
            fragmentNewsletterDialogBinding.etEmail.error = mContext.getString(R.string.please_enter_valid_email)

        } else {
            if (position == 0) {
                CommonUtils.showProgressDialog(mContext )
                val jsonObject = JsonObject()
                jsonObject.addProperty("email", mEmail.get()?.let { CommonUtils.encrypt(it) })
                disposable = apiClient.emailVerificationService(jsonObject)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    CommonUtils.hideProgressDialog(mContext )
                                    if (result.responseCode == 200) {
                                        ToastUtils.showToastShort(mContext, CommonUtils.decrypt(result.responseMessage))
                                        Log.e("OTP", CommonUtils.decrypt(result.otp))
                                        prefManager.savePreference(AppConstants.USER_ID, result.userId)
                                        DashboardActivity.fragment = SendFragment()
                                        val intent = Intent(mContext, OtpActivity::class.java)
                                        intent.putExtra(AppConstants.EMAIL_ID, mEmail.get()?.let { CommonUtils.encrypt(it) })
                                        mContext.startActivity(intent)
                                    } else ToastUtils.showToastShort(mContext, CommonUtils.decrypt(result.responseMessage))
                                },
                                { error ->
                                    CommonUtils.hideProgressDialog(mContext )
                                    ToastUtils.showToastShort(mContext, error.message!!) },
                                { disposable?.dispose() }
                        )

            }
            dialog.dismiss()
        }
    }

    fun dismissDialog() {
        if (position!=0) {
            settingListBinding.btSwitch.isChecked = !settingListBinding.btSwitch.isChecked
        }
        dialog.dismiss()


    }
}
