package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.ObservableField
import android.os.CountDownTimer
import android.util.Log

import com.cryptassist.R
import com.cryptassist.activity.BuyCoinActivity
import com.cryptassist.otputils.OtpView
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class OtpViewModel(private val activity: Activity) {
    private val thread: Thread? = null
    internal var time = 0
    internal lateinit var email :String
    private var timer: CountDownTimer? = null
    private val millisInFuture: Long = 30000 //30 seconds
    private val countDownInterval: Long = 1000
    var otpTime = ObservableField<String>()
    private var disposable: Disposable? = null
    private val apiClient by lazy {
        APIExecutor.clientApi()
    }
    private val otpView: OtpView = activity.findViewById(R.id.otp_view)


    init {
        setTime()
        if (activity.intent!=null && activity.intent.getStringExtra(AppConstants.EMAIL_ID)!=null){
            email = activity.intent.getStringExtra(AppConstants.EMAIL_ID)
        }

    }

    fun onSubmitClick() {
        CommonUtils.showProgressDialog(activity)
        val mJsonObject = JsonObject()
        mJsonObject.addProperty("email", email)
        mJsonObject.addProperty("otp", CommonUtils.encrypt(otpView.otp))
        disposable = apiClient.verifyOtpService(mJsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(activity)
                            if (result.responseCode == 200) {
                                ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))

                                val intent = Intent(activity,BuyCoinActivity::class.java)
                                activity.startActivity(intent)
                                activity.finish()
                            } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(activity, error.message!!)
                            CommonUtils.hideProgressDialog(activity)},

                        { disposable?.dispose() }
                )

    }

    fun onClickResend() {
        if (otpTime.get().equals(activity.resources.getString(R.string.resend_otp), false)) {
            setTime()
            resendOTP()
        }


    }

    private fun setTime() {
        timer = object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                //do something in every tick
                if (millisUntilFinished / 1000 >= 10) {
                    otpTime.set("00:" + millisUntilFinished / 1000)
                } else {
                    otpTime.set("00:0" + millisUntilFinished / 1000)
                }
            }

            override fun onFinish() {
                otpTime.set(activity.getString(R.string.resend_otp))
            }

        }.start()
    }

    fun resendOTP(){
        CommonUtils.showProgressDialog(activity)
        val mJsonObject = JsonObject()
        mJsonObject.addProperty("email", email)
        disposable = apiClient.resendOtpService(mJsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(activity)
                            if (result.responseCode == 200) {
                                ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                                Log.e("OTP", CommonUtils.decrypt(result.otp))
                            } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                        },
                        { error -> ToastUtils.showToastShort(activity, error.message!!)
                            CommonUtils.hideProgressDialog(activity)},
                        { disposable?.dispose() }
                )
    }
}
