package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.BaseObservable
import android.text.TextUtils
import com.amirarcane.lockscreen.activity.EnterPinActivity
import com.cryptassist.R

import com.cryptassist.databinding.WelcomeDialogBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants

class WelcomeDialogViewModel(private val activity: Activity, private val dialog: Dialog, internal var mBinding: WelcomeDialogBinding) : BaseObservable() {
    private val prefManager = PrefManager.getInstance(activity)

    fun onClick() {
        if (TextUtils.isEmpty(mBinding.etEmail.text.toString())) {
            mBinding.etEmail.error = activity.getString(R.string.please_enter_name)
            mBinding.etEmail.requestFocus()

        } else {
            prefManager.savePreference(AppConstants.IS_WALLET_CREATED, true)
            dialog.dismiss()
            val i = Intent(activity, EnterPinActivity::class.java)
            activity.startActivityForResult(i, AppConstants.REQUEST_CODE_PASS_CODE)
        }
    }

    fun dismissDialog() {
        dialog.dismiss()
    }


}
