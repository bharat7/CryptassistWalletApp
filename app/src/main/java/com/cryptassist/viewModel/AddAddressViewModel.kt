package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.BaseObservable

import com.cryptassist.activity.AddContactActivity
import com.cryptassist.utils.CommonUtils

class AddAddressViewModel(private val activity: Activity) : BaseObservable() {
    private val dialog: Dialog? = null
    private val string: String? = null

    fun onClick() {
        CommonUtils.hideKeyboard(activity)
        val intent = Intent(activity, AddContactActivity::class.java)
        activity.startActivity(intent)

    }
}
