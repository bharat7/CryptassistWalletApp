package com.cryptassist.viewModel

import android.app.Activity
import android.databinding.BaseObservable
import com.cryptassist.utils.AppConstants

import com.cryptassist.utils.CommonUtils

class ContactDetailViewModel(private val activity: Activity) : BaseObservable() {
    var name: String? = null
    var address: String? = null
    var email: String? = null
    var imagePath: String? = null


    init {
        CommonUtils.hideKeyboard(activity)
        if (activity.intent != null) {
            name = activity.intent.getStringExtra(AppConstants.USER_NAME)
            address = activity.intent.getStringExtra(AppConstants.USER_ADDRESS)
            email = activity.intent.getStringExtra(AppConstants.USER_EMAIL)
            imagePath = activity.intent.getStringExtra(AppConstants.USER_IMAGE)
        }
    }
}
