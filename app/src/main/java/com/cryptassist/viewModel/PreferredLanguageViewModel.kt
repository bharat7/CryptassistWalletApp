package com.cryptassist.viewModel

import android.app.Activity
import android.databinding.BaseObservable
import android.databinding.ObservableField
import android.widget.Toast

import com.cryptassist.R
import com.cryptassist.activity.PreferredLanguageActivity
import com.cryptassist.model.PreferredLanguageModel
import com.cryptassist.utils.CommonUtils
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

import java.util.ArrayList

class PreferredLanguageViewModel : BaseObservable {
    private lateinit var activity: Activity
    var mName: ObservableField<String> = ObservableField()
    lateinit var mCountryName: String

    val preferredLanguageModels: ArrayList<PreferredLanguageViewModel>
        get() {
            val preferedLanguageModels = ArrayList<PreferredLanguageViewModel>()
            var viaLocales = arrayOfNulls<String>(0)
            if (activity is PreferredLanguageActivity) {
                viaLocales = activity.resources.getStringArray(R.array.prefer_languages)
            }
            for (i in viaLocales.indices) {
                val preferredLanguageModel = PreferredLanguageViewModel(PreferredLanguageModel(viaLocales[i]!!))
                preferedLanguageModels.add(preferredLanguageModel)
            }

            return preferedLanguageModels
        }


    constructor(activity: Activity) {
        this.activity = activity
    }

    fun getCountryName(): String {
        return mCountryName

    }

    fun setCountryName(countryName: String) {
        this.mCountryName = countryName
        notifyPropertyChanged(R.id.tvCountry)
    }


    constructor(preferredLanguageModel: PreferredLanguageModel) {
        this.mCountryName = preferredLanguageModel.countryName
    }

    fun onClick() {
//        activity.finish()
    }


}
