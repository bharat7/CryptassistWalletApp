package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import com.amirarcane.lockscreen.activity.EnterPinActivity

import com.cryptassist.R
import com.cryptassist.adapter.ImportPassphraseAdapter
import com.cryptassist.databinding.ImportWalletDialogBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import java.util.ArrayList

class ImportDialogViewModel(private val activity: Activity, private val dialog: Dialog, internal var mBinding: ImportWalletDialogBinding) : BaseObservable() {
    var importPhrasesViewModels: ObservableArrayList<ImportPhrasesViewModel> = ObservableArrayList()
    private lateinit var importPassphraseAdapter: ImportPassphraseAdapter
    private val prefManager = PrefManager.getInstance(activity)

    init {
//        val layoutInflater = LayoutInflater.from(activity)
        mBinding.etEmail.setOnEditorActionListener { _, actionId, _ ->
            if (actionId!=EditorInfo.IME_ACTION_DONE) {
                if (importPhrasesViewModels.size <= 10) {
                    onEditPhrases()
                }
                if (importPhrasesViewModels.size == 11) {
                    onEditPhrases()
                    mBinding.etEmail.imeOptions = EditorInfo.IME_ACTION_DONE
                }
            }


            false
        }

    }

    fun onClick() {
        onEditPhrases()
    }


    fun dismissDialog() {
        dialog.dismiss()
    }

    fun onEditPhrases() {
        if (TextUtils.isEmpty(mBinding.etEmail.text.toString())) {
            mBinding.etEmail.error = activity.getString(R.string.please_enter_passphrase)
            mBinding.etEmail.requestFocus()

        }
        else {

            val importPhrasesViewModel = ImportPhrasesViewModel(activity)
            importPhrasesViewModel.mCountryName = mBinding.etEmail.text.toString()
            importPhrasesViewModels.add(importPhrasesViewModel)
            importPassphraseAdapter = ImportPassphraseAdapter(activity, importPhrasesViewModels)
            mBinding.etEmail.setText("")

            mBinding.etEmail.setFocusable(true)
            mBinding.etEmail.setFocusableInTouchMode(true)
            if (importPhrasesViewModels.size==12){
                prefManager.savePreference(AppConstants.IS_WALLET_CREATED, true)
                val i = Intent(activity, EnterPinActivity::class.java)
        activity.startActivityForResult(i, AppConstants.REQUEST_CODE_PASS_CODE)
            }

        }
    }

}
