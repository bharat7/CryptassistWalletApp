package com.cryptassist.viewModel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context

import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants

class LanguageViewModel(application: Application) : AndroidViewModel(application) {
    var users: MutableLiveData<String>? = MutableLiveData()

    fun setLanguage(language: String) {
        if (!language.equals("")) {
            users?.setValue(language)
        } else {
            users?.setValue("en")
        }
    }

}
