package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.view.LayoutInflater
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.FragmentFeedbackDialogBinding
import com.cryptassist.fragment.SettingsFragment
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FeedbackViewModel(private val dialog: Dialog, internal var fragmentFeedbackDialogBinding: FragmentFeedbackDialogBinding, private val activity: Activity) : BaseObservable() {
    private val dialogPopUpBinding: DialogPopUpBinding
    private val dialogUtils: Dialog
    private var popUpDialogViewModel: PopUpDialogViewModel? = null
    val mFeedback = ObservableField<String>()
    val mIsAll = ObservableField<Boolean>()
    private var disposable: Disposable? = null
    private val apiClient by lazy {
        APIExecutor.clientApi()
    }

    init {
        val layoutInflater: LayoutInflater = LayoutInflater.from(activity)
        dialogPopUpBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_pop_up, null, false)
        dialogUtils = DialogUtils.createCustomDialog(activity, dialogPopUpBinding.root)
    }

    fun onClick() {
        //        CommonUtils.hideKeyboard(fragmentFeedbackDialogBinding);
        if (fragmentFeedbackDialogBinding.etEmail.text.isNullOrEmpty()) {
            fragmentFeedbackDialogBinding.etEmail.error = "Please enter message"
            fragmentFeedbackDialogBinding.etEmail.requestFocus()
        } else if (mIsAll.get() == null) {
            ToastUtils.showToastShort(activity, "Please select any one checkbox")
        } else {
            CommonUtils.showProgressDialog(activity)
            val mJsonObject = JsonObject()
            mJsonObject.addProperty("screen", CommonUtils.encrypt(if (mIsAll.get()!!) "All" else activity.localClassName))
            mJsonObject.addProperty("feedback", mFeedback.get()?.let { CommonUtils.encrypt(it) })
            disposable = apiClient.sendFeedbackService(mJsonObject)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result ->
                                CommonUtils.hideProgressDialog(activity)
                                if (result.responseCode == 200) {
                                    dialog.dismiss()
                                    popUpDialogViewModel = PopUpDialogViewModel(activity, dialogUtils, activity.getString(R.string.feedback_successfully), "")
                                    dialogPopUpBinding.onClickOk = popUpDialogViewModel
                                    dialogUtils.show()
                                } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                            },
                            {
                                error -> ToastUtils.showToastShort(activity, error.message!!)
                                CommonUtils.hideProgressDialog(activity)},
                            { disposable?.dispose() }
                    )


        }

        if (DashboardActivity.fragment != null && DashboardActivity.fragment is SettingsFragment) {
            MyViewHolder.hideEasyTouchView()
        } else {
            MyViewHolder.showEasyTouchView()
        }
    }


    fun dismissDialog() {
        dialog.dismiss()
        if (DashboardActivity.fragment != null && DashboardActivity.fragment is SettingsFragment) {
            MyViewHolder.hideEasyTouchView()
        } else {
            MyViewHolder.showEasyTouchView()
        }
    }
}
