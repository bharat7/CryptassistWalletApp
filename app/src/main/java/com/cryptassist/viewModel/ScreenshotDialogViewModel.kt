package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable

import com.cryptassist.activity.PassPhraseActivity
import com.cryptassist.utils.CommonUtils

class ScreenshotDialogViewModel(private val dialog: Dialog, private val context: Context) : BaseObservable() {

    fun dismissDialog() {
        CommonUtils.hideKeyboard(context as Activity)
        val intent = Intent(context, PassPhraseActivity::class.java)
        //        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent)
        dialog.dismiss()
    }
}
