package com.cryptassist.viewModel

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.ObservableField
import com.cryptassist.R

class SendViewModel constructor(var context: Context) : BaseObservable() {

   val errorAddress: ObservableField<String> = ObservableField()
   val errorCta: ObservableField<String> = ObservableField()
   val errorToAmount: ObservableField<String> = ObservableField()

    val address: ObservableField<String> = ObservableField()
    val cta: ObservableField<String> = ObservableField()
    val toAmount: ObservableField<String> = ObservableField()

    fun validation() {
//        notifyChange()
//        notifyPropertyChanged(BR.errorAddress)

        errorAddress.set(if (address.get().isNullOrEmpty()) context.getString(R.string.please_enter_valid_address) else null)
        errorCta.set(if (cta.get().isNullOrEmpty()) context.getString(R.string.please_ebter_valid_amount) else null)
        errorToAmount.set(if (toAmount.get().isNullOrEmpty()) context.getString(R.string.please_ebter_valid_amount) else null)
    }

    //    @Bindable
//    fun getErrorAddress() = if (mAddress.get() != null && (mAddress.get()?.isEmpty()!! || mAddress.get()?.length!! in 2..9)) "Please enter valid mAddress" else null

}