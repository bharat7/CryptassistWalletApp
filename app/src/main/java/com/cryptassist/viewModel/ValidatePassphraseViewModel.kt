package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.BaseObservable
import com.cryptassist.R
import com.cryptassist.activity.ValidatePassPhraseActivity
import com.cryptassist.databinding.RowPhraseSelectedBinding
import com.cryptassist.model.PreferredCurrencyModel
import com.cryptassist.utils.CommonUtils
import java.util.*

class ValidatePassphraseViewModel : BaseObservable {
    private var activity: Activity? = null
    private lateinit var phraseListBinding: RowPhraseSelectedBinding
    var isSelected: Boolean = false
    var count: Int = 0

    var viaLocales: ArrayList<String> = arrayListOf()/*arrayOf("limb", "smart", "dad", "In", "Perpendicular", "tricky", "voice", "ejaculate", "jealous", "Mental", "you", "recovery")*/

    /*   public void setClickListener() {
       count++;

       }*/
    lateinit var mCountryName: String
    val validatePassphraseViewModels: ArrayList<ValidatePassphraseViewModel>
        get() {
            val preferredLanguageModels = ArrayList<ValidatePassphraseViewModel>()

            for (i in viaLocales.indices) {
                val preferredLanguageModel = ValidatePassphraseViewModel(PreferredCurrencyModel(viaLocales[i]))
                preferredLanguageModels.add(preferredLanguageModel)
            }
            return preferredLanguageModels
        }

    constructor(activity: Activity) {
        this.activity = activity
    }

    constructor(activity: Activity, phraseListBinding: RowPhraseSelectedBinding) {
        this.activity = activity
        this.phraseListBinding = phraseListBinding
    }

    fun getCountryName(): String {
        return mCountryName

    }

    fun setCountryName(countryName: String) {
        this.mCountryName = countryName
        notifyPropertyChanged(R.id.tvCountry)
    }


    constructor(preferredLanguageModel: PreferredCurrencyModel) {
        this.mCountryName = preferredLanguageModel.countryName
    }

    fun onClick() {
        CommonUtils.hideKeyboard(activity!!)
        val intent = Intent(activity, ValidatePassPhraseActivity::class.java)
        activity?.startActivity(intent)
        activity?.finish()

    }


}
