package com.cryptassist.viewModel

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.databinding.adapters.NumberPickerBindingAdapter.setValue



/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

class LiveDataViewModel(/*application: Application,activity: Activity*/) /*: AndroidViewModel(application) */{

//    private val mRepository: SendFragmentViewModel = SendFragmentViewModel(activity)
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.

//    init {
//        ctaAddress.value=(mRepository.mWalletAddress.get())
//    }
  fun getData(): LiveData<String> {
    ctaAddress.value="gdgdgdg"
    return ctaAddress
}


    companion object {
        var ctaAddress:  MutableLiveData<String> = MutableLiveData()
//        fun  LiveData<String> insert(/*address: ObservableField<String>,"":String*/)
//        {
//        ctaAddress.value= "sdsddds"
//       return ctaAddress
//        }
    }

}