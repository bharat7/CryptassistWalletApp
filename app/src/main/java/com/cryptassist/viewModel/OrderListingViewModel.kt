package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.ObservableArrayList
import com.cryptassist.activity.BuyCoinExchangeActivity
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class OrderListingViewModel{
    private var activity: Activity
    private var disposable: Disposable? = null
    var orderViewModel: ObservableArrayList<OrderViewModel> = ObservableArrayList()
    lateinit var prefManager: PrefManager



    private val apiClient by lazy {
        APIExecutor.clientApi()
    }



    constructor(activity: Activity) {
        this.activity = activity
        prefManager=PrefManager.getInstance(activity)
        getCoinList()

    }


    fun onClick() {
        var intent = Intent(activity, BuyCoinExchangeActivity::class.java)
        activity.startActivity(intent)

    }

    fun addListAdapter(activity: Activity){
        for (i in orderViewModel.indices) {
//            buyCoinListModel=OrderViewModel(activity)
//            buyCoinListModel.coinName.set(CommonUtils.decrypt(coinListModel.get(i).name!!))
//            buyCoinListModel.coinImage.set(CommonUtils.decrypt(coinListModel.get(i).icon!!))
//            buyCoinListModels.add(buyCoinListModel)

        }
//        buyCoinListAdapter = BuyCoinListAdapter(activity, buyCoinListModels)
    }
    
    private fun getCoinList(){
        CommonUtils.showProgressDialog(activity)
        val jsonObject = JsonObject()
        jsonObject.addProperty("User_id", prefManager.getPreference(AppConstants.USER_ID,""))
        jsonObject.addProperty("page_no", "1")
        jsonObject.addProperty("page_size", "10")
        disposable = apiClient.myOrdersListing(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(activity)
                            if (result.responseCode == 200) {
                                ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                                orderViewModel.addAll(result.leads);

                            } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(activity, error.message!!)
                            CommonUtils.hideProgressDialog(activity)},
                        { disposable?.dispose() }
                )

    }


}
