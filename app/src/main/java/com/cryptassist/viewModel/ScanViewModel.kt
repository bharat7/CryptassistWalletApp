package com.cryptassist.viewModel

import android.app.Dialog
import android.databinding.BaseObservable

class ScanViewModel : BaseObservable() {
    private val dialog: Dialog? = null


    fun dismissDialog() {

        dialog!!.dismiss()
    }
}
