package com.cryptassist.viewModel

import android.app.Activity
import android.databinding.BaseObservable

import com.cryptassist.R
import com.cryptassist.model.PreferredCurrencyModel
import com.cryptassist.utils.CommonUtils

import java.util.ArrayList

class PreferedCurrencyViewModel : BaseObservable {
    private lateinit var activity: Activity
    var isSelected: Boolean = false
    internal var viaLocales = arrayOf("JPY", "CNY", "SDG", "RON", "MKD", "MXN", "CAD", "ZAR", "AUD", "NOK", "ILS", "ISK", "SYP", "LYD", "UYU", "YER", "CSD", "EEK", "THB", "IDR", "LBP", "AED", "BOB", "QAR", "BHD", "HNL", "HRK", "COP", "ALL", "DKK", "MYR", "SEK", "RSD", "BGN", "DOP", "KRW", "LVL", "VEF", "CZK", "TND", "KWD", "VND", "JOD", "NZD", "PAB", "CLP", "PEN", "GBP", "DZD", "CHF", "RUB", "UAH", "ARS", "SAR", "EGP", "INR", "PYG", "TWD", "TRY", "BAM", "OMR", "SGD", "MAD", "BYR", "NIO", "HKD", "LTL", "SKK", "GTQ", "BRL", "EUR", "HUF", "IQD", "CRC", "PHP", "SVC", "PLN", "USD")

    lateinit var mCountryName: String
    val preferedLanguageModels: ArrayList<PreferedCurrencyViewModel>
        get() {
            val preferedLanguageModels = ArrayList<PreferedCurrencyViewModel>()

            for (i in viaLocales.indices) {
                val preferedLanguageModel = PreferedCurrencyViewModel(PreferredCurrencyModel(viaLocales[i]))
                preferedLanguageModels.add(preferedLanguageModel)
            }
            return preferedLanguageModels
        }

    constructor(activity: Activity) {
        this.activity = activity
    }

    fun getCountryName(): String {
        return mCountryName

    }

    fun setCountryName(countryName: String) {
        this.mCountryName = countryName
        notifyPropertyChanged(R.id.tvCountry)
    }


    constructor(preferedLanguageModel: PreferredCurrencyModel) {
        this.mCountryName = preferedLanguageModel.countryName
    }

    fun onClick() {
        CommonUtils.hideKeyboard(activity)
        onSelected()
        activity.onBackPressed()


    }

    private fun onSelected(): Boolean {
        isSelected = true
        return isSelected
    }

}
