package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.databinding.BaseObservable

import com.cryptassist.utils.CommonUtils

class ReceiveViewModel(private val context: Activity) : BaseObservable() {
    private val dialog: Dialog? = null
    private val string: String? = null

    fun shareIntent() {
        CommonUtils.hideKeyboard(context)
        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        context.startActivity(Intent.createChooser(i, "Share via"))

    }
}
