package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.databinding.BaseObservable
import android.databinding.DataBindingUtil
import android.view.LayoutInflater

import com.cryptassist.R
import com.cryptassist.databinding.ImportWalletDialogBinding
import com.cryptassist.databinding.WelcomeDialogBinding
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils

class MainActivityViewModel(private val activity: Activity) : BaseObservable() {
    private var importWalletDialogBinding: ImportWalletDialogBinding? = null
    private var welcomeDialogBinding: WelcomeDialogBinding? = null
    private var dialogUtils: Dialog? = null


    init {
        CommonUtils.hideKeyboard(activity)
    }

    fun onImportClick() {
        val layoutInflater = LayoutInflater.from(activity)
        importWalletDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.import_wallet_dialog, null, false)
        dialogUtils = DialogUtils.createCustomDialog(activity, importWalletDialogBinding!!.root)
        val importDialogViewModel = ImportDialogViewModel(activity, dialogUtils!!, importWalletDialogBinding!!)
        importWalletDialogBinding!!.clickIcon = importDialogViewModel
        dialogUtils!!.show()

    }

    fun onNewWalletclick() {
        val layoutInflater = LayoutInflater.from(activity)
        welcomeDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.welcome_dialog, null, false)
        dialogUtils = DialogUtils.createCustomDialog(activity, welcomeDialogBinding!!.root)
        val welcomeDialogViewModel = WelcomeDialogViewModel(activity,dialogUtils!!, welcomeDialogBinding!!)
        welcomeDialogBinding!!.clickIcon = welcomeDialogViewModel
        dialogUtils!!.show()
    }
}
