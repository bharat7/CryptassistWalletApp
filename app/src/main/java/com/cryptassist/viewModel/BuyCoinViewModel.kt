package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.ObservableArrayList
import com.cryptassist.activity.BuyCoinExchangeActivity
import com.cryptassist.adapter.BuyCoinListAdapter
import com.cryptassist.model.BuyCoinListModel
import com.cryptassist.model.CoinsListModel
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class BuyCoinViewModel{
    private var activity: Activity
    private var disposable: Disposable? = null

    var buyCoinListAdapter: BuyCoinListAdapter? = null
    private lateinit var buyCoinListModel: BuyCoinListModel
    var buyCoinListModels: ObservableArrayList<BuyCoinListModel> = ObservableArrayList()
    private var coinListModel:ArrayList<CoinsListModel> = ArrayList()



    private val apiClient by lazy {
        APIExecutor.clientApi()
    }



    constructor(activity: Activity) {
        this.activity = activity
        getCoinList()

    }


    fun onClick() {
        var intent = Intent(activity, BuyCoinExchangeActivity::class.java)
        activity.startActivity(intent)

    }

    fun addListAdapter(activity: Activity){
        for (i in coinListModel.indices) {
            buyCoinListModel=BuyCoinListModel(activity)
            buyCoinListModel.coinName.set(CommonUtils.decrypt(coinListModel.get(i).name!!))
            buyCoinListModel.coinImage.set(CommonUtils.decrypt(coinListModel.get(i).icon!!))
            buyCoinListModel.coinAmount.set(CommonUtils.decrypt(coinListModel.get(i).currentRate!!))
            buyCoinListModel.coinBonusRate.set(CommonUtils.decrypt(coinListModel.get(i).currentBonusRate!!))
            buyCoinListModels.add(buyCoinListModel)

        }
//        buyCoinListAdapter = BuyCoinListAdapter(activity, buyCoinListModels)
    }
    
    private fun getCoinList(){
        val jsonObject = JsonObject()
        CommonUtils.showProgressDialog(activity)
        disposable = apiClient.coinListing(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(activity)
                            if (result.responseCode == 200) {
                                ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                                 coinListModel.addAll(result.coins);
                                addListAdapter(activity)

                            } else ToastUtils.showToastShort(activity, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(activity, error.message!!)
                            CommonUtils.hideProgressDialog(activity)},
                        { disposable?.dispose() }
                )

    }


}
