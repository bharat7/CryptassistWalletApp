package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import android.widget.Toast
import com.cryptassist.R

import com.cryptassist.activity.AddContactActivity
import com.cryptassist.activity.ScannerActivity
import com.cryptassist.utils.AppConstants
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScannerDialogViewModel(
        private val activity: Activity,
        public val dialog: Dialog,
        private val scannerView: ZXingScannerView) {
    var ctaAddress = ObservableField<String>()

    fun onAddContactClick() {
        val intent = Intent(activity, AddContactActivity::class.java)
        intent.putExtra(AppConstants.CTA_ADDRESS, ctaAddress.get())
        activity.startActivity(intent)
        dialog.dismiss()
    }

    fun onCopyAddress() {
        scannerView.startCamera()
        val clipboard = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(AppConstants.CTA_ADDRESS, ctaAddress.get())
        clipboard.primaryClip = clip
        Toast.makeText(activity, activity.getString(R.string.address_copied), Toast.LENGTH_SHORT).show()
        dialog.dismiss()
    }

    fun onCancelClick() {
        scannerView.startCamera()
        dialog.dismiss()
    }

    fun onSendClick() {
        scannerView.startCamera()
        DataRepository.getData(ctaAddress)
        dialog.dismiss()
       activity.onBackPressed()
    }



}
