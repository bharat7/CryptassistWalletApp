package com.cryptassist.viewModel

import android.app.Activity
import android.app.Dialog
import android.app.Fragment
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.activity.ScannerActivity
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.fragment.AddAddressFragment
import com.cryptassist.fragment.InviteContactFragment
import com.cryptassist.fragment.ScanFragment
import com.cryptassist.fragment.SendFragment
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils

class SendFragmentViewModel constructor(private val activity: Activity) : ViewModel() {
    var btTitle : ObservableField<String> = ObservableField()
    private val dialogPopUpBinding: DialogPopUpBinding
    private val dialogUtils: Dialog
    private lateinit var  dialogUtilstwo: Dialog
    private  lateinit var layoutInflater: LayoutInflater
    private var popUpDialogViewModel: PopUpDialogViewModel? = null
    private lateinit var fragmentNewsletterDialogBinding: FragmentNewsletterDialogBinding

    internal var price: Double? = 0.0
//    val context: Context = activity


     var mWalletAddress: ObservableField<String> = ObservableField()
    var mCtaAmount: ObservableField<String> = ObservableField()
    var mConvertedAmount: ObservableField<String> = ObservableField()


    init {
        btTitle.set(activity.getString(R.string.buy))
        layoutInflater = LayoutInflater.from(activity as Context)
        dialogPopUpBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_pop_up, null, false)
        dialogUtils = DialogUtils.createCustomDialog(activity as Context, dialogPopUpBinding.root)
    }


    fun onSaveClick() {
        CommonUtils.hideKeyboard(activity)
        if (btTitle.get().equals(activity.getString(R.string.buy))){
            fragmentNewsletterDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_newsletter_dialog, null, false)
            dialogUtilstwo = DialogUtils.createCustomDialog(activity, fragmentNewsletterDialogBinding.root)
            val subscribeNewsletterViewModel = SubscribeNewsletterViewModel(activity, dialogUtilstwo, fragmentNewsletterDialogBinding,  0)
            fragmentNewsletterDialogBinding.clickIcon = subscribeNewsletterViewModel
            dialogUtilstwo.show()
        }
        else {
            when {
                TextUtils.isEmpty(mWalletAddress.get()) -> {
                    popUpDialogViewModel = PopUpDialogViewModel(activity, dialogUtils, activity.getString(R.string.please_enter_wallet_Address), "")
                    dialogPopUpBinding.onClickOk = popUpDialogViewModel
                    dialogUtils.show()

                }
                TextUtils.isEmpty(mCtaAmount.get().toString()) -> {
                    popUpDialogViewModel = PopUpDialogViewModel(activity, dialogUtils, activity.getString(R.string.please_enter_cta_amount), "")
                    dialogPopUpBinding.onClickOk = popUpDialogViewModel
                    dialogUtils.show()

                }
                else -> {
                    popUpDialogViewModel = PopUpDialogViewModel(activity, dialogUtils, activity.getString(R.string.amount_send_successfully), "")
                    dialogPopUpBinding.onClickOk = popUpDialogViewModel
                    dialogUtils.show()
                }
            }
        }
    }

    fun onClickSend( ctaAddress:ObservableField<String>,dialog:Dialog) {
        activity.toast(ctaAddress.get()!!)

       dialog.dismiss()

    }
    fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


    fun onClickSave() {
        CommonUtils.hideKeyboard(activity)
        if (DashboardActivity.addContactModels.size == 0) {
            val layoutInflater: LayoutInflater = LayoutInflater.from(activity as Context)
            val dialogPopUpBinding = DataBindingUtil.inflate<DialogPopUpBinding>(layoutInflater, R.layout.dialog_pop_up, null, false)
            val dialogUtils = DialogUtils.createCustomDialog(activity as Context, dialogPopUpBinding.root)
            val popUpDialogViewModel = PopUpDialogViewModel(activity, dialogUtils, activity.getString(R.string.no_contact_added), "")
            dialogPopUpBinding.onClickOk = popUpDialogViewModel
            dialogUtils.show()
        } else {
            val bundle = Bundle()
            bundle.putString(AppConstants.FROM_SEND,"fromSend")
            DashboardActivity.fragment = AddAddressFragment()
            (DashboardActivity.fragment as AddAddressFragment).arguments=bundle
//            (DashboardActivity.fragment as AddAddressFragment).setTargetFragment(SendFragment(),AppConstants.SEND_ADDRESS_REQUEST_CODE)
            CommonUtils.setFragment(DashboardActivity.fragment as AddAddressFragment, true, activity as FragmentActivity, R.id.container)
            DashboardActivity.mBinding.llInner?.container?.visibility = View.VISIBLE
            DashboardActivity.mBinding.llInner?.bottomBar?.visibility = View.GONE
        }
    }


    fun updateFiatConversion() {
//        if (!mCtaAmount.get().isNullOrEmpty() && mCtaAmount.get().equals(".")) mCtaAmount.set("0.")
        mConvertedAmount.set(
                if (mCtaAmount.get().isNullOrEmpty() || mCtaAmount.get().equals(".") || mCtaAmount.get()?.toDouble()?.compareTo(0.0) == 0)
                    activity.getString(R.string._0_000000)
                else price?.let {
                    mCtaAmount.get()?.toDouble()?.times(it).toString()
                })

    }




}
