package com.cryptassist.viewModel

import android.app.Activity
import android.content.Intent
import android.databinding.BaseObservable
import com.amirarcane.lockscreen.activity.EnterPinActivity

import com.cryptassist.activity.DashboardActivity
import com.cryptassist.activity.MainActivity
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants

class TutorialScreenViewModel(private val activity: Activity) : BaseObservable() {
    private val prefManager = PrefManager.getInstance(activity)

    fun onSkipClick() {
        prefManager.savePreference(AppConstants.IS_APP_OLDER, true)
        val i = Intent(activity, MainActivity::class.java)
        activity.startActivity(i)
        activity.finish()

    }
}
