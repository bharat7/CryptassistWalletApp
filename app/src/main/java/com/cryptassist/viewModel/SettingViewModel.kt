package com.cryptassist.viewModel

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat

import com.cryptassist.R
import com.cryptassist.activity.PreferredLanguageActivity
import com.cryptassist.model.SettingModel

import java.util.ArrayList
import java.util.Arrays

class SettingViewModel : BaseObservable {

    lateinit var optionName: String

    lateinit var optionImage: Drawable

    constructor()

    constructor(settingModel: SettingModel) {
        this.optionName = settingModel.optionName
        this.optionImage = settingModel.optionImage

    }

    fun getPreferredLanguageModels(context: Context): ArrayList<SettingViewModel> {
        val settingViewModels = ArrayList<SettingViewModel>()
        val itemNameList = ArrayList<String>()
        itemNameList.addAll(Arrays.asList(*context.resources.getStringArray(R.array.setting_options)))
        val itemImageList = ArrayList<String>()
        itemImageList.addAll(Arrays.asList(*context.resources.getStringArray(R.array.setting_screen_icon)))
        val icons = loadScreenIcons(context)
        for (i in itemNameList.indices) {
            val settingViewModel = SettingViewModel(SettingModel(itemNameList[i], icons[i]!!))
            settingViewModels.add(settingViewModel)
        }

        return settingViewModels
    }

    private fun loadScreenIcons(context: Context): Array<Drawable?> {
        val ta = context.resources.obtainTypedArray(R.array.setting_screen_icon)
        val icons = arrayOfNulls<Drawable>(ta.length())
        for (i in 0 until ta.length()) {
            val id = ta.getResourceId(i, 0)
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(context, id)
            }
        }
        ta.recycle()
        return icons
    }

}
