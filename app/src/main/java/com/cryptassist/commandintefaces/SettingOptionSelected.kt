package com.cryptassist.commandintefaces

interface SettingOptionSelected {
    fun onSettingOptionSelected()
}
