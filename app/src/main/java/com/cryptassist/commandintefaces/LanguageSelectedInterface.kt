package com.cryptassist.commandintefaces

interface LanguageSelectedInterface  {
    fun onLanguageSelected()
}
