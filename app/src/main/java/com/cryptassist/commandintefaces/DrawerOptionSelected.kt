package com.cryptassist.commandintefaces

interface DrawerOptionSelected {

    fun onOptionSelected(position: Int)


}
