package com.cryptassist.commandintefaces

interface ProfileAddressSelectedInterface {
    fun onProfileAddressSelected()
}