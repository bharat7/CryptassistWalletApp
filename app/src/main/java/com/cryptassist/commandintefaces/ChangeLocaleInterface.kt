package com.cryptassist.commandintefaces

interface ChangeLocaleInterface  {
    fun onLanguageSelected(locale:String)
}
