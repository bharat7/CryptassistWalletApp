package com.cryptassist.commandintefaces

interface ImportPhrasesSelected  {
    fun onPhrasesRemoved()
}
