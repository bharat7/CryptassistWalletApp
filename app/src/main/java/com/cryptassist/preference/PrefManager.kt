package com.cryptassist.preference

import android.content.Context
import android.content.SharedPreferences

import com.google.gson.Gson

/**
 * Created by Ashutosh Kumar on 17/10/2017.
 */

class PrefManager public constructor(context: Context) {
    private val sharedPreferences: SharedPreferences


    init {
        val prefsFile = context.packageName
        sharedPreferences = context.getSharedPreferences(prefsFile, Context.MODE_PRIVATE)
        sharedPreferences.edit().apply()
    }


    fun deletePreference(key: String) {
        if (sharedPreferences.contains(key)) {
            sharedPreferences.edit().remove(key).apply()
        }
    }

    fun deleteAllPreference() {
        try {
            sharedPreferences.edit().clear().apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun savePreference(key: String, value: Any?) {
        deletePreference(key)
        if (value is Boolean) {
            sharedPreferences.edit().putBoolean(key, (value as Boolean?)!!).apply()
        } else if (value is Int) {
            sharedPreferences.edit().putInt(key, (value as Int?)!!).apply()
        } else if (value is Float) {
            sharedPreferences.edit().putFloat(key, (value as Float?)!!).apply()
        } else if (value is Long) {
            sharedPreferences.edit().putLong(key, (value as Long?)!!).apply()
        } else if (value is String) {
            sharedPreferences.edit().putString(key, value as String?).apply()
        } else if (value is Enum<*>) {
            sharedPreferences.edit().putString(key, value.toString()).apply()
        } else if (value != null) {
            sharedPreferences.edit().putString(key, Gson().toJson(value)).apply()
        } /*else if (value != null) {
            throw new RuntimeException("Attempting to save non-primitive preference");
        }*/

    }

    fun <T> getPreference(key: String): T {
        return sharedPreferences.all[key] as T
    }

    fun <T> getPreference(key: String, defValue: T): T {
        val returnValue = sharedPreferences.all[key] as T
        return returnValue ?: defValue
    }

    fun isPreferenceExists(key: String): Boolean {
        return sharedPreferences.contains(key)
    }

    companion object {

        private var sInstance: PrefManager? = null

        @Synchronized
        fun getInstance(context: Context): PrefManager {
            if (sInstance == null) {
                sInstance = PrefManager(context)
            }
            return sInstance as PrefManager
        }
    }


}
