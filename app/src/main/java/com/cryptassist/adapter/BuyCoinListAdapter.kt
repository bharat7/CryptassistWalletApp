package com.cryptassist.adapter

//import com.cryptassist.databinding.LanguageRowItemBinding;

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.activity.BuyCoinExchangeActivity
import com.cryptassist.commandintefaces.CoinSelectedInterface
import com.cryptassist.databinding.RowBuyCoinBinding

import com.cryptassist.databinding.RowProfileAddressBinding
import com.cryptassist.databinding.SuggestDialogBinding
import com.cryptassist.model.BuyCoinListModel
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.*
import java.util.*

class BuyCoinListAdapter(private val context: Context, private val buyCoinListModel: ArrayList<BuyCoinListModel>) : RecyclerView.Adapter<BuyCoinListAdapter.MyViewHolder>() {
    var isShowmore: Boolean = false
    private lateinit var dialogUtils: Dialog
    private lateinit var suggestDialogBinding:SuggestDialogBinding;
    private lateinit var layoutInflater: LayoutInflater


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowBuyCoinBinding = DataBindingUtil.inflate<RowBuyCoinBinding>(layoutInflater, R.layout.row_buy_coin, parent, false)
        return MyViewHolder(rowBuyCoinBinding)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val buyCoinListModels = buyCoinListModel[position]
        holder.bind(buyCoinListModels, position)
        if (!isShowmore&&position == 8) {
            buyCoinListModels.coinName.set(context.getString(R.string.show_more_coin))
           holder.rowBuyCoinBinding.ivIcon.setImageResource(R.drawable.show_more)
        }

        else if (position == buyCoinListModel.size-1) {
            buyCoinListModels.coinName.set(context.getString(R.string.suggest_another_coin))
            holder.rowBuyCoinBinding.ivIcon.setImageResource(R.drawable.suggest)
        }

    }


    override fun getItemCount(): Int {
        if (isShowmore || buyCoinListModel.size<9) {
            return buyCoinListModel.size
        } else {

            return 9
        }
    }

    inner class MyViewHolder( val rowBuyCoinBinding: RowBuyCoinBinding) : RecyclerView.ViewHolder(rowBuyCoinBinding.root) {

        fun bind(buyCoinViewModels: BuyCoinListModel, position: Int) {
            rowBuyCoinBinding.viewModel = buyCoinViewModels
            rowBuyCoinBinding.coinSelected = object : CoinSelectedInterface {
                override fun coinSelected() {
                    if (!isShowmore && position == 8) {
                        isShowmore = true
                        notifyDataSetChanged()
                    } else if (position == buyCoinListModel.size-1) {
                        suggestDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.suggest_dialog, null, false)
                        dialogUtils = DialogUtils.createCustomDialog(context, suggestDialogBinding.root)
                        val suggestCoinViewModel = SuggestCoinViewModel(context,dialogUtils)
                        suggestDialogBinding.viewModel=suggestCoinViewModel
                        dialogUtils.show()
                    } else {
                        var intent = Intent(context, BuyCoinExchangeActivity::class.java)
                        context.startActivity(intent)
                    }

                }


            }

        }
    }


}
