package com.cryptassist.adapter

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.cryptassist.R

import java.util.ArrayList

class SlidingImageAdapter(private val context: Context, private val IMAGES: ArrayList<Int>) : PagerAdapter() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return IMAGES.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.sliding_images_layout, view, false)!!



        try {
            val imageView = imageLayout
                    .findViewById<View>(R.id.image) as ImageView

            imageView.setImageResource(IMAGES[position])
            view.addView(imageLayout, 0)
        } catch (e: Exception) {
        }



        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }


}
