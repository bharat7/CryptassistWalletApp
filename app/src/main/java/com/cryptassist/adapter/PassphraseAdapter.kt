package com.cryptassist.adapter

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.databinding.RowPhraseBinding
import com.cryptassist.viewModel.PassphraseViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class PassphraseAdapter(private val context: Context, private val passphraseViewModels: ArrayList<PassphraseViewModel>) : RecyclerView.Adapter<PassphraseAdapter.MyViewHolder>() {
//    private val passphraseViewModel: PassphraseViewModel = PassphraseViewModel(context as Activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val phraseListBinding = DataBindingUtil.inflate<RowPhraseBinding>(layoutInflater, R.layout.row_phrase,null,false)

        return MyViewHolder(phraseListBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val preferredLanguageViewModel = passphraseViewModels[position]
        holder.bind(preferredLanguageViewModel)
        holder.phraseListBinding.phrases = preferredLanguageViewModel
    }


    override fun getItemCount(): Int {
        return passphraseViewModels.size
    }

    inner class MyViewHolder(internal val phraseListBinding: RowPhraseBinding) : RecyclerView.ViewHolder(phraseListBinding.root) {

        fun bind(preferredLanguageViewModel: PassphraseViewModel) {
            this.phraseListBinding.phrases = preferredLanguageViewModel
        }
    }


}
