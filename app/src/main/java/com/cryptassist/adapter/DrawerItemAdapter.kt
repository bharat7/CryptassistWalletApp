package com.cryptassist.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.cryptassist.commandintefaces.DrawerOptionSelected
import com.cryptassist.R
import com.cryptassist.databinding.ItemOptionBinding
import com.cryptassist.viewModel.DrawerViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class DrawerItemAdapter(private val context: Context, private val drawerViewModels: ArrayList<DrawerViewModel>,
                        private val listener: DrawerOptionSelected?) : RecyclerView.Adapter<DrawerItemAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemOptionBinding = DataBindingUtil.inflate<ItemOptionBinding>(layoutInflater, R.layout.item_option, parent, false)

        return MyViewHolder(itemOptionBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val drawerViewModel = drawerViewModels[position]
        holder.bind(drawerViewModel)


        holder.itemOptionBinding.llOptions.setOnClickListener {
            listener?.onOptionSelected(position)
        }
    }


    override fun getItemCount(): Int {
        return drawerViewModels.size
    }

    inner class MyViewHolder(internal val itemOptionBinding: ItemOptionBinding) : RecyclerView.ViewHolder(itemOptionBinding.root) {
        fun bind(drawerViewModel: DrawerViewModel) {
            this.itemOptionBinding.options = drawerViewModel
            itemOptionBinding.icon.setImageDrawable(drawerViewModel.optionImage)
        }

    }


}
