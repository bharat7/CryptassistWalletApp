package com.cryptassist.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cryptassist.R
import com.cryptassist.activity.PassPhraseActivity
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.RowPhraseSelectedBinding
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.PopUpDialogViewModel
import com.cryptassist.viewModel.ValidatePassphraseViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class ValidatePassphraseAdapter(private val context: Context, private val passphraseViewModels: ArrayList<ValidatePassphraseViewModel>,private val backupPhraseList:ArrayList<String>) : RecyclerView.Adapter<ValidatePassphraseAdapter.MyViewHolder>() {
    private var count = 0
    private var selectedCount = 0
    private val phraseListing = ArrayList<String>()
    private var isSelected = true
    private var phrasesNotMatched = false
    private val dialogPopUpBinding: DialogPopUpBinding
    private val dialogUtils: Dialog
    private var popUpDialogViewModel: PopUpDialogViewModel? = null

    init {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        dialogPopUpBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_pop_up, null, false)
        dialogUtils = DialogUtils.createCustomDialog(context, dialogPopUpBinding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val phraseListBinding = DataBindingUtil.inflate<RowPhraseSelectedBinding>(layoutInflater,R.layout.row_phrase_selected,null,false)

        return MyViewHolder(phraseListBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val preferedLanguageViewModel = passphraseViewModels[position]
        if (passphraseViewModels[position].count > selectedCount) {
            if (!isSelected) {
                passphraseViewModels[position].count--
            }
            if (passphraseViewModels[position].isSelected) {
                holder.phraseListBinding.tvCount.text = "" + passphraseViewModels[position].count
                holder.phraseListBinding.tvCount.visibility = View.VISIBLE
                holder.phraseListBinding.tvPhrase.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                holder.phraseListBinding.tvPhrase.background = ContextCompat.getDrawable(context, R.drawable.bg_selected_phrase)
            }
        } else if (passphraseViewModels[position].isSelected) {
            holder.phraseListBinding.tvCount.text = "" + passphraseViewModels[position].count
            holder.phraseListBinding.tvCount.visibility = View.VISIBLE
            holder.phraseListBinding.tvPhrase.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            holder.phraseListBinding.tvPhrase.background = ContextCompat.getDrawable(context, R.drawable.bg_selected_phrase)
        } else {
            holder.phraseListBinding.tvPhrase.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.phraseListBinding.tvPhrase.background = ContextCompat.getDrawable(context, R.drawable.bg_phrase)
            holder.phraseListBinding.tvCount.visibility = View.GONE
        }

        holder.phraseListBinding.tvPhrase.text = preferedLanguageViewModel.getCountryName()


        holder.bind()
        holder.phraseListBinding.tvPhrase.setOnClickListener {
            try {
                if (!passphraseViewModels[position].isSelected) {
                    count++
                    isSelected = true
                    phrasesNotMatched = false
                    phraseListing.add(passphraseViewModels[position].getCountryName())
                    passphraseViewModels[position].count = count
                    passphraseViewModels[position].isSelected = true
                    holder.phraseListBinding.tvPhrase.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    holder.phraseListBinding.tvPhrase.background = ContextCompat.getDrawable(context, R.drawable.bg_selected_phrase)
                    holder.phraseListBinding.tvCount.visibility = View.VISIBLE
                    holder.phraseListBinding.tvCount.text = "" + count
                    if (count == passphraseViewModels.size) {
                        for (i in 0 until backupPhraseList.size) {
                            if (backupPhraseList.get(i) != phraseListing[i]) {
                                phrasesNotMatched = true
                            }

                        }
                        if (phrasesNotMatched) {
                            popUpDialogViewModel = PopUpDialogViewModel(context as Activity, dialogUtils, context.getString(R.string.phrases_not_matched), "")
                            dialogPopUpBinding.onClickOk = popUpDialogViewModel
                            dialogUtils.show()
                        } else {
                            CommonUtils.savePreferencesBoolean(context, AppConstants.IS_BACKUP_SUCCESSFULL, true)
                            popUpDialogViewModel = PopUpDialogViewModel(context as Activity, dialogUtils, context.getString(R.string.your_wallet_successfully_backup), "")
                            dialogPopUpBinding.onClickOk = popUpDialogViewModel
                            dialogUtils.show()
                        }
                    }
                } else {
                    count--
                    isSelected = false
                    holder.phraseListBinding.tvPhrase.setTextColor(ContextCompat.getColor(context, R.color.white))
                    holder.phraseListBinding.tvPhrase.background = ContextCompat.getDrawable(context, R.drawable.bg_phrase)
                    holder.phraseListBinding.tvCount.visibility = View.GONE
                    passphraseViewModels[position].isSelected = false
                    selectedCount = passphraseViewModels[position].count
                    if (phraseListing.size > 0) {
                        for (i in phraseListing.indices) {
                            if (passphraseViewModels[position].getCountryName().equals(phraseListing[i], ignoreCase = true)) {
                                phraseListing.removeAt(i)
                            }
                        }
                    }
                    this.notifyDataSetChanged()

                }
            } catch (e: Exception) {
                this.notifyDataSetChanged()
            }
        }
    }


    override fun getItemCount(): Int {
        return passphraseViewModels.size
    }

    inner class MyViewHolder(internal val phraseListBinding: RowPhraseSelectedBinding) : RecyclerView.ViewHolder(phraseListBinding.root) {

        fun bind() {
            //            this.phraseListBinding.setValidatePhrases(preferedLanguageViewModel);
        }
    }


}
