package com.cryptassist.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.cryptassist.R
import com.cryptassist.activity.ContactDetailActivity
import com.cryptassist.commandintefaces.SendAddressInterface
import com.cryptassist.sectionheadercomponents.*
import com.cryptassist.utils.*
import com.cryptassist.viewModel.AddContactViewModel
import com.cryptassist.viewModel.DataRepository
import com.cryptassist.viewModel.LiveDataViewModel
import de.hdodenhof.circleimageview.CircleImageView
import java.io.ByteArrayOutputStream

import java.util.*

class AddressListAdapter(contacts: ArrayList<AddContactViewModel>, val context: Context, val layoutInflater: LayoutInflater, var sendAddressInterface: SendAddressInterface) : SearchablePinnedHeaderListViewAdapter<AddContactViewModel>() {


    private var mContacts: ArrayList<AddContactViewModel>? = null
    private val CONTACT_PHOTO_IMAGE_SIZE: Int
    private val PHOTO_TEXT_BACKGROUND_COLORS: IntArray
    val mAsyncTaskThreadPool = AsyncTaskThreadPool(1, 2, 10)

    override fun getSectionTitle(sectionIndex: Int): CharSequence {
        return (sections[sectionIndex] as StringArrayAlphabetIndexer.AlphaBetSection).name!!
    }

    init {
        setData(contacts)
        PHOTO_TEXT_BACKGROUND_COLORS = context.resources.getIntArray(R.array.contacts_text_background_colors)
        CONTACT_PHOTO_IMAGE_SIZE = context.resources.getDimensionPixelSize(R.dimen.list_item__contact_imageview_size)
    }

    fun setData(contacts: ArrayList<AddContactViewModel>) {
        this.mContacts = contacts
        val generatedContactNames = generateContactNames(contacts)
        setSectionIndexer(StringArrayAlphabetIndexer(generatedContactNames, true))
    }

    private fun generateContactNames(contacts: List<AddContactViewModel>?): Array<String> {
        val contactNames = ArrayList<String>()
        if (contacts != null)
            for (contactEntity in contacts)
                contactEntity.mName?.let { contactNames.add(it.get().toString()) }
        return contactNames.toTypedArray()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: ViewHolder
        val rootView: View
        if (convertView == null) {
            holder = ViewHolder()
            rootView = layoutInflater.inflate(R.layout.add_address_item, parent, false)
            holder.friendProfileCircularContactView = rootView
                    .findViewById<View>(R.id.listview_item__friendPhotoImageView) as CircleImageView
            try {
                val bm = BitmapFactory.decodeFile( mContacts!!.get(position).userImage.get())
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                holder.friendProfileCircularContactView!!.setImageBitmap(bm)
            } catch (e: Exception) {
            }

            holder.friendName = rootView
                    .findViewById<View>(R.id.listview_item__friendNameTextView) as TextView
            holder.tvAddress = rootView
                    .findViewById<View>(R.id.tvAddress) as TextView
            holder.llParent = rootView
                    .findViewById<View>(R.id.llParent) as LinearLayout
            holder.llParent!!.setOnClickListener {
                if (mContacts?.get(position)?.isFromSend?.get()!=null &&mContacts?.get(position)?.isFromSend?.get()!!){
                    sendAddressInterface.sendAddress(position)
//                    mContacts?.get(position)?.mAddress?.get()?.let { it1 -> Lin.insert(it1) }
                    DataRepository.getData(mContacts!!.get(position).mAddress)
                }
                else {
                    val intent = Intent(context, ContactDetailActivity::class.java)
                    intent.putExtra(AppConstants.USER_NAME, mContacts!!.get(position).mName.get())
                    intent.putExtra(AppConstants.USER_ADDRESS, mContacts!!.get(position).mAddress.get())
                    intent.putExtra(AppConstants.USER_EMAIL, mContacts!!.get(position).mEmail.get())
                    intent.putExtra(AppConstants.USER_IMAGE, mContacts!!.get(position).userImage.get())
                    context!!.startActivity(intent)
                }
            }
            holder.headerView = rootView.findViewById<View>(R.id.header_text) as TextView
            rootView.tag = holder
        } else {
            rootView = convertView
            holder = rootView.tag as ViewHolder
        }
        val contact = getItem(position)
        val name = contact!!.mName
        val address = contact.mAddress
        holder.friendName!!.text = name.get()
        holder.tvAddress!!.text = address.get()
        val hasPhoto = !TextUtils.isEmpty(contact.photoId.get())
        if (holder.updateTask != null && !holder.updateTask!!.isCancelled())
            holder.updateTask!!.cancel(true)
        val cachedBitmap = if (hasPhoto) ImageCache.INSTANCE.getBitmapFromMemCache(contact.photoId.get()!!) else null
        if (cachedBitmap != null)
//            holder.friendProfileCircularContactView!!.setImageBitmap(cachedBitmap)
        else {
            PHOTO_TEXT_BACKGROUND_COLORS[position % PHOTO_TEXT_BACKGROUND_COLORS.size]
            if (TextUtils.isEmpty(name.get())) {
//                holder.friendProfileCircularContactView!!.setImageResource(R.mipmap.contact_profile,
//                        backgroundColorToUse)
            } else {
                if (TextUtils.isEmpty(name.get())) {

                } else name.get()?.substring(0, 1)?.toUpperCase(Locale.getDefault())
//                holder.friendProfileCircularContactView!!.setTextAndBackgroundColor(characterToShow, backgroundColorToUse)
//                holder.headerView!!.setText(characterToShow);
            }
            if (hasPhoto) {
                holder.updateTask = object : AsyncTaskEx<Void, Void, Bitmap>() {

                    override fun doInBackground(vararg params: Void): Bitmap? {
                        if (isCancelled())
                            return null
                        val b = ContactImageUtil.loadContactPhotoThumbnail(context, contact.photoId.get()!!, CONTACT_PHOTO_IMAGE_SIZE)
                        return if (b != null) ThumbnailUtils.extractThumbnail(b, CONTACT_PHOTO_IMAGE_SIZE,
                                CONTACT_PHOTO_IMAGE_SIZE) else null
                    }

                    override fun onPostExecute(result: Bitmap?) {
                        super.onPostExecute(result)
                        if (result == null)
                            return
                        ImageCache.INSTANCE.addBitmapToCache(contact.photoId.get(), result)
//                        holder.friendProfileCircularContactView!!.setImageBitmap(result)
                    }
                }
                mAsyncTaskThreadPool.executeAsyncTask(holder.updateTask)
            }
        }
        bindSectionHeader(holder.headerView, null, position)
        return rootView
    }

    /*override fun doFilter(item: AddContactViewModel, constraint: CharSequence): Boolean {
        if (TextUtils.isEmpty(constraint))
            return true
        val name = item.mName
        return !TextUtils.isEmpty(name.get()) && name.get()?.toLowerCase(Locale.getDefault())?.contains(constraint.toString().toLowerCase(Locale.getDefault()))!!
    }

    override fun getOriginalList(): ArrayList<AddContactViewModel>? {
        return mContacts
    }*/

    override val originalList = mContacts!!

    override fun doFilter(item: AddContactViewModel, constraint: CharSequence?): Boolean {
        if (TextUtils.isEmpty(constraint))
            return true
        val name = item.mName
        return !TextUtils.isEmpty(name.get()) && name.get()?.toLowerCase(Locale.getDefault())?.contains(constraint.toString().toLowerCase(Locale.getDefault()))!!
    }

    class ViewHolder {
        var friendProfileCircularContactView: CircleImageView? = null
        internal var friendName: TextView? = null
        internal var headerView: TextView? = null
        internal var tvEmail: TextView? = null
        internal var tvAddress: TextView? = null
        internal var llParent: LinearLayout? = null
        var updateTask: AsyncTaskEx<Void, Void, Bitmap>? = null
    }

}
