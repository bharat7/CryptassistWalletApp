package com.cryptassist.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.cryptassist.databinding.TransactionListBinding
import com.cryptassist.viewModel.TransactionViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class TransactionAdapter(private val context: Context, private val transactionViewModels: ArrayList<TransactionViewModel>) : RecyclerView.Adapter<TransactionAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        //        LanguageRowItemBinding languageRowItemBinding= DataBindingUtil.inflate(layoutInflater, R.layout.language_row_item,parent,false);
        val transactionListBinding = TransactionListBinding.inflate(layoutInflater, parent, false)

        return MyViewHolder(transactionListBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val transactionViewModel = transactionViewModels[position]
        holder.bind(transactionViewModel)

    }


    override fun getItemCount(): Int {
        return transactionViewModels.size
    }

    inner class MyViewHolder(private val countryListBinding: TransactionListBinding) : RecyclerView.ViewHolder(countryListBinding.root) {
        fun bind(transactionViewModel: TransactionViewModel) {
            this.countryListBinding.transaction = transactionViewModel
        }
    }


}
