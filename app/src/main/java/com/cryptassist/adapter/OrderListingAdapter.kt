package com.cryptassist.adapter

import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.databinding.RowBuyCoinBinding
import com.cryptassist.databinding.RowOrderListBinding
import com.cryptassist.databinding.SuggestDialogBinding
import com.cryptassist.viewModel.*
import java.util.*

class OrderListingAdapter(private val context: Context, private val orderViewModel: ArrayList<OrderViewModel>) : RecyclerView.Adapter<OrderListingAdapter.MyViewHolder>() {
    private lateinit var layoutInflater: LayoutInflater


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowOrderListBinding = DataBindingUtil.inflate<RowOrderListBinding>(layoutInflater, R.layout.row_order_list, parent, false)
        return MyViewHolder(rowOrderListBinding)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val buyCoinListModels = orderViewModel[position]
        holder.bind(buyCoinListModels, position)
    }


    override fun getItemCount(): Int {
        return orderViewModel.size
    }

    inner class MyViewHolder(private val rowOrderListBinding: RowOrderListBinding) : RecyclerView.ViewHolder(rowOrderListBinding.root) {

        fun bind(orderViewModel: OrderViewModel, position: Int) {
            rowOrderListBinding.viewModel = orderViewModel


        }
    }


}
