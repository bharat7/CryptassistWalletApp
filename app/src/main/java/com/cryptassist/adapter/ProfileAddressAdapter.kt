package com.cryptassist.adapter

//import com.cryptassist.databinding.LanguageRowItemBinding;

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.commandintefaces.ProfileAddressSelectedInterface

import com.cryptassist.databinding.RowProfileAddressBinding
import com.cryptassist.viewModel.ProfileAddressListViewModel
import com.cryptassist.viewModel.ProfileAddressViewModel
import java.nio.channels.SelectableChannel
import java.util.*

class ProfileAddressAdapter(private val context: Context, private val profileAddressListViewModel: ArrayList<ProfileAddressListViewModel>) : RecyclerView.Adapter<ProfileAddressAdapter.MyViewHolder>() {
 var selectedpositiom:Int=-1
    lateinit var rowProfileAddressBinding :RowProfileAddressBinding;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
         rowProfileAddressBinding = DataBindingUtil.inflate<RowProfileAddressBinding>(layoutInflater, R.layout.row_profile_address, parent, false)
        return MyViewHolder(rowProfileAddressBinding)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val profileAddressListViewModels = profileAddressListViewModel[position]
        holder.bind(profileAddressListViewModels,position)
       if (selectedpositiom==position){
           holder.rowImportPhrasesBinding.cbSelect.isChecked=true
           profileAddressListViewModel.get(position).profileAddressViewModel?.myAddress?.set(  profileAddressListViewModel.get(position).mAddress.get())
       }
        else{
           holder.rowImportPhrasesBinding.cbSelect.isChecked=false

       }
    }


    override fun getItemCount(): Int {
        return profileAddressListViewModel.size
    }

    inner class MyViewHolder( val rowImportPhrasesBinding: RowProfileAddressBinding) : RecyclerView.ViewHolder(rowImportPhrasesBinding.root) {

        fun bind(profileAddressListViewModels: ProfileAddressListViewModel,position:Int) {
            this.rowImportPhrasesBinding.profileAddress = profileAddressListViewModels
            this.rowImportPhrasesBinding.onSelectAddress = object : ProfileAddressSelectedInterface{
                override fun onProfileAddressSelected(){

                        selectedpositiom=position;
                        profileAddressListViewModel.get(position).isSelected=true
                        notifyDataSetChanged()

                }
            }
        }
    }
    public interface SelectedAddressInterface {
        fun selectedAddress(address: String)
    }

}
