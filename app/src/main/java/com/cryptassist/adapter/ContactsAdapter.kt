package com.cryptassist.adapter

import android.content.Context
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.cryptassist.R
import com.cryptassist.activity.ContactActivity
import com.cryptassist.model.ContactModel
import com.cryptassist.model.InviteModel
import com.cryptassist.sectionheadercomponents.*
import com.cryptassist.utils.*

import java.util.*
import kotlin.collections.ArrayList

class ContactsAdapter(contacts: ArrayList<ContactModel>,val context: Context,val layoutInflater: LayoutInflater,val inviteList:ArrayList<InviteModel>) : SearchablePinnedHeaderListViewAdapter<ContactModel>() {



     private var mContacts: ArrayList<ContactModel>? = null
    private val CONTACT_PHOTO_IMAGE_SIZE: Int
    private lateinit var inviteModel: InviteModel
    private val PHOTO_TEXT_BACKGROUND_COLORS: IntArray
    val mAsyncTaskThreadPool = AsyncTaskThreadPool(1, 2, 10)

    override fun getSectionTitle(sectionIndex: Int): CharSequence {
        return (sections[sectionIndex] as StringArrayAlphabetIndexer.AlphaBetSection).name.toString()
    }

    init {
        setData(contacts)
        PHOTO_TEXT_BACKGROUND_COLORS = context.resources.getIntArray(R.array.contacts_text_background_colors)
        CONTACT_PHOTO_IMAGE_SIZE = context.resources.getDimensionPixelSize(R.dimen.list_item__contact_imageview_size)
    }

    fun setData(contacts: ArrayList<ContactModel>) {
        this.mContacts = contacts
        val generatedContactNames = generateContactNames(contacts)
        setSectionIndexer(StringArrayAlphabetIndexer(generatedContactNames, true))
    }

    private fun generateContactNames(contacts: List<ContactModel>?): Array<String> {
        val contactNames = ArrayList<String>()
        if (contacts != null)
            for (contactEntity in contacts)
                contactEntity.name?.let { contactNames.add(it) }
        return contactNames.toTypedArray()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: ContactActivity.ViewHolder
        val rootView: View
        if (convertView == null) {
            holder = ContactActivity.ViewHolder()
            rootView = layoutInflater.inflate(R.layout.listview_item, parent, false)
            holder.friendName = rootView
                    .findViewById<View>(R.id.listview_item__friendNameTextView) as TextView
            holder.headerView = rootView.findViewById<View>(R.id.header_text) as TextView
            rootView.tag = holder
        } else {
            rootView = convertView
            holder = rootView.tag as ContactActivity.ViewHolder
        }
        holder.cbSelect=rootView.findViewById(R.id.cbSelect) as CheckBox
        holder.cbSelect!!.isChecked = mContacts?.get(position)!!.isSelected
        holder.cbSelect!!.setOnClickListener {
            try {
                if (holder.cbSelect!!.isChecked){
                    mContacts!!.get(position).isSelected=true
                     inviteModel = InviteModel()
                    if(mContacts!!.get(position).emailAddress!=null){
                    inviteModel.emailId.set(mContacts!!.get(position).emailAddress!!)}
                    inviteModel.phoneNo.set(mContacts!!.get(position).phoneNo!!)
                        inviteList.add(inviteModel)
                }
                else {
                    mContacts!!.get(position).isSelected=false
                    if (inviteList != null && inviteList.size > 0) {
                        for (i in inviteList.indices) {
                          if (inviteList.get(i).emailId.get().equals(mContacts!!.get(position).emailAddress)||inviteList.get(i).phoneNo.get().equals(mContacts!!.get(position).phoneNo)){
                              inviteList.removeAt(i)
                          }
                        }
                    }
                }
                notifyDataSetChanged()
            } catch (e: Exception) {
            }

        }

        val contact = getItem(position)
        val name = contact!!.name
        holder.friendName!!.text = name
        val hasPhoto = !TextUtils.isEmpty(contact.photoId)
        if (holder.updateTask != null && !holder.updateTask!!.isCancelled)
            holder.updateTask!!.cancel(true)
        val cachedBitmap = if (hasPhoto) ImageCache.INSTANCE.getBitmapFromMemCache(contact.photoId.toString()) else null
        if (cachedBitmap != null)
//            holder.friendProfileCircularContactView!!.setImageBitmap(cachedBitmap)
        else {
            PHOTO_TEXT_BACKGROUND_COLORS[position % PHOTO_TEXT_BACKGROUND_COLORS.size]
            if (TextUtils.isEmpty(name)){
//                holder.friendProfileCircularContactView!!.setImageResource(R.mipmap.contact_profile,
//                        backgroundColorToUse)
            }
            else {
                if (TextUtils.isEmpty(name))  else name!!.substring(0, 1).toUpperCase(Locale.getDefault())
//                holder.friendProfileCircularContactView!!.setTextAndBackgroundColor(characterToShow, backgroundColorToUse)
//                holder.headerView!!.setText(characterToShow);
            }
            if (hasPhoto) {
                holder.updateTask = object : AsyncTaskEx<Void, Void, Bitmap>() {

                    override fun doInBackground(vararg params: Void): Bitmap? {
                        if (isCancelled)
                            return null
                        val b = ContactImageUtil.loadContactPhotoThumbnail(context, contact.photoId!!, CONTACT_PHOTO_IMAGE_SIZE)
                        return if (b != null) ThumbnailUtils.extractThumbnail(b, CONTACT_PHOTO_IMAGE_SIZE,
                                CONTACT_PHOTO_IMAGE_SIZE) else null
                    }

                    override fun onPostExecute(result: Bitmap?) {
                        super.onPostExecute(result)
                        if (result == null)
                            return
                        ImageCache.INSTANCE.addBitmapToCache(contact.photoId, result)
//                        holder.friendProfileCircularContactView!!.setImageBitmap(result)
                    }
                }
                mAsyncTaskThreadPool.executeAsyncTask(holder.updateTask)
            }
        }
        bindSectionHeader(holder.headerView, null, position)
        return rootView
    }

    /*override fun doFilter(item: ContactModel, constraint: CharSequence): Boolean {
        if (TextUtils.isEmpty(constraint))
            return true
        val name = item.name
        return !TextUtils.isEmpty(name) && name!!.toLowerCase(Locale.getDefault())
                .contains(constraint.toString().toLowerCase(Locale.getDefault()))
    }

    override fun getOriginalList(): ArrayList<ContactModel>? {
        return mContacts
    }*/

     override val originalList = mContacts!!

     override fun doFilter(item: ContactModel, constraint: CharSequence?): Boolean {
         if (TextUtils.isEmpty(constraint))
             return true
         val name = item.name
         return !TextUtils.isEmpty(name) && name!!.toLowerCase(Locale.getDefault())
                 .contains(constraint.toString().toLowerCase(Locale.getDefault()))
     }
}
