package com.cryptassist.adapter

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

import com.cryptassist.R
import com.cryptassist.commandintefaces.ImportPhrasesSelected
 import com.cryptassist.databinding.RowImportPhrasesBinding
import com.cryptassist.viewModel.ImportPhrasesViewModel
import com.cryptassist.viewModel.PassphraseViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class ImportPassphraseAdapter(private val context: Context, private val importPhrasesViewModels: ArrayList<ImportPhrasesViewModel>) : RecyclerView.Adapter<ImportPassphraseAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowImportPhrasesBinding = DataBindingUtil.inflate<RowImportPhrasesBinding>(layoutInflater, R.layout.row_import_phrases, parent, false)
        return MyViewHolder(rowImportPhrasesBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val importPhrasesViewModel = importPhrasesViewModels[position]
        holder.bind(importPhrasesViewModel, position)
    }


    override fun getItemCount(): Int {
        return importPhrasesViewModels.size
    }

    inner class MyViewHolder(private val rowImportPhrasesBinding: RowImportPhrasesBinding) : RecyclerView.ViewHolder(rowImportPhrasesBinding.root) {

        fun bind(importPhrasesViewModel: ImportPhrasesViewModel, position: Int) {
            this.rowImportPhrasesBinding.importPhrases = importPhrasesViewModel
            this.rowImportPhrasesBinding.phrasesRemove = object : ImportPhrasesSelected{
                override fun onPhrasesRemoved() {
                    importPhrasesViewModels.removeAt(position)

                }

            }
        }
        }

    fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


    }



