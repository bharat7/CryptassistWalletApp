package com.cryptassist.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.amirarcane.lockscreen.activity.EnterPinActivity
import com.cryptassist.R
import com.cryptassist.activity.*
import com.cryptassist.databinding.FragmentFeedbackDialogBinding
import com.cryptassist.databinding.FragmentNewsletterDialogBinding
import com.cryptassist.databinding.RowSettingListBinding
import com.cryptassist.fragment.SendFragment
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.FeedbackViewModel
import com.cryptassist.viewModel.SettingViewModel
import com.cryptassist.viewModel.SubscribeNewsletterViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class SettingAdapter(private val context: Context, private val settingViewModels: ArrayList<SettingViewModel>) : RecyclerView.Adapter<SettingAdapter.MyViewHolder>() {
    private lateinit var dialogUtils: Dialog
    private lateinit var fragmentFeedbackDialogBinding: FragmentFeedbackDialogBinding
    private lateinit var fragmentNewsletterDialogBinding: FragmentNewsletterDialogBinding
    private lateinit var layoutInflater: LayoutInflater
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        //        LanguageRowItemBinding languageRowItemBinding= DataBindingUtil.inflate(layoutInflater, R.layout.language_row_item,parent,false);
        val settingListBinding = DataBindingUtil.inflate<RowSettingListBinding>(layoutInflater, R.layout.row_setting_list, null, false)

        return MyViewHolder(settingListBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val settingViewModel = settingViewModels[position]


        holder.bind(settingViewModel)
        if (position == 6 || position == 8 || position == 9) {
            holder.settingListBinding.ivArrow.visibility = View.GONE
            holder.settingListBinding.btSwitch.visibility = View.VISIBLE
        } else if (position == 10) {
            holder.settingListBinding.ivArrow.visibility = View.GONE
            holder.settingListBinding.btSwitch.visibility = View.GONE
        } else {
            holder.settingListBinding.ivArrow.visibility = View.VISIBLE
            holder.settingListBinding.btSwitch.visibility = View.GONE
        }

        holder.settingListBinding.btSwitch.setOnCheckedChangeListener { _, _ -> setDialog(holder, position) }



        holder.settingListBinding.llOption.setOnClickListener { _ ->
            when (position) {
                0 -> {
                    setDialog(holder, position)
                }
               1 -> {
                    DashboardActivity.fragment = SendFragment()
                    val intent = Intent(context, OrderListingActivity::class.java)
                    context.startActivity(intent)
                }

                2 -> {
                    DashboardActivity.fragment = SendFragment()
                    val intent = Intent(context, ProfileActivity::class.java)
                    context.startActivity(intent)
                }
                3 -> {
                    DashboardActivity.fragment = SendFragment()
                    val intent = Intent(context, PreferredCurrencyActivity::class.java)
                    context.startActivity(intent)
                }
                4-> {
                    DashboardActivity.fragment = SendFragment()
                    val intent = Intent(context, PreferredLanguageActivity::class.java)
                    context.startActivity(intent)
                }
                5-> {
                    DashboardActivity.fragment = SendFragment()
                    //                    Intent intent = new Intent(context, PreferredLanguageActivity.class);
                    val intent = EnterPinActivity.getIntent(context, true)
                    context.startActivity(intent)
                }
                6-> {
                    holder.settingListBinding.btSwitch.isChecked = !holder.settingListBinding.btSwitch.isChecked
                    val prefManager = PrefManager.getInstance(context)
                    prefManager.savePreference(AppConstants.IS_FINGER_ENABLED, holder.settingListBinding.btSwitch.isChecked)
                }
                7 -> {
                    setDialog(holder, position)
                }
               8 -> {
                    holder.settingListBinding.btSwitch.isChecked = !holder.settingListBinding.btSwitch.isChecked
                    setDialog(holder, position)
                }
               9 -> {
                    holder.settingListBinding.btSwitch.isChecked = !holder.settingListBinding.btSwitch.isChecked
                   setDialog(holder, position)
                }

                else -> {
//                    Toast.makeText(context, "Work in Progress", Toast.LENGTH_SHORT).show()
                }
            }


        }


        /*holder.settingListBinding.optionSelected.apply  {

        }*/


    }

    override fun getItemCount(): Int {
        return settingViewModels.size
    }

    inner class MyViewHolder(internal val settingListBinding: RowSettingListBinding) : RecyclerView.ViewHolder(settingListBinding.root) {

        fun bind(settingViewModel: SettingViewModel) {
            this.settingListBinding.settingOption = settingViewModel
//            settingListBinding.ivOptionImage.setImageDrawable(settingViewModel.optionImage)

        }

    }

    private fun setDialog(holder: MyViewHolder, position: Int) {
        if (position == 0)  else if (position == 7) {{
            fragmentNewsletterDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_newsletter_dialog, null, false)
            dialogUtils = DialogUtils.createCustomDialog(context, fragmentNewsletterDialogBinding.root)
            val subscribeNewsletterViewModel = SubscribeNewsletterViewModel(context, dialogUtils, fragmentNewsletterDialogBinding,  position)
            fragmentNewsletterDialogBinding.clickIcon = subscribeNewsletterViewModel
            dialogUtils.show()
        }
            fragmentFeedbackDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_feedback_dialog, null, false)
            dialogUtils = DialogUtils.createCustomDialog(context, fragmentFeedbackDialogBinding.root)
            val feedbackViewModel = FeedbackViewModel(dialogUtils, fragmentFeedbackDialogBinding, context as Activity)
            fragmentFeedbackDialogBinding.clickIcon = feedbackViewModel
            dialogUtils.show()
        } else if (position == 8 ||position==9) {
            fragmentNewsletterDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_newsletter_dialog, null, false)
            dialogUtils = DialogUtils.createCustomDialog(context, fragmentNewsletterDialogBinding.root)
            val subscribeNewsletterViewModel = SubscribeNewsletterViewModel(context, dialogUtils, fragmentNewsletterDialogBinding, holder.settingListBinding, position)
            fragmentNewsletterDialogBinding.clickIcon = subscribeNewsletterViewModel
            if (holder.settingListBinding.btSwitch.isChecked) {
                holder.settingListBinding.btSwitch.isChecked = true
                dialogUtils.show()
            } else {
                holder.settingListBinding.btSwitch.isChecked = false
                dialogUtils.dismiss()
            }
        }

    }

}
