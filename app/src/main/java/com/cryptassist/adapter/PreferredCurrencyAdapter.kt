package com.cryptassist.adapter

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.databinding.CurrencyRowItemBinding
import com.cryptassist.viewModel.PreferedCurrencyViewModel

import java.util.ArrayList

//import com.cryptassist.databinding.LanguageRowItemBinding;

class PreferredCurrencyAdapter(private val context: Context, private val preferredLanguageModel: ArrayList<PreferedCurrencyViewModel>) : RecyclerView.Adapter<PreferredCurrencyAdapter.MyViewHolder>() {
    private val preferredCurrencyViewModel: PreferedCurrencyViewModel = PreferedCurrencyViewModel(context as Activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val currencyRowItemBinding = DataBindingUtil.inflate<CurrencyRowItemBinding>(layoutInflater, R.layout.currency_row_item,parent, false)

        return MyViewHolder(currencyRowItemBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val preferedLanguageViewModel = preferredLanguageModel[position]
        holder.bind(preferedLanguageViewModel)
        holder.currencyListBinding.onClick = preferredCurrencyViewModel
    }


    override fun getItemCount(): Int {
        return preferredLanguageModel.size
    }

    inner class MyViewHolder(val currencyListBinding: CurrencyRowItemBinding) : RecyclerView.ViewHolder(currencyListBinding.root) {
        fun bind(preferredLanguageViewModel: PreferedCurrencyViewModel) {
            this.currencyListBinding.currency = preferredLanguageViewModel
        }
    }


}
