package com.cryptassist.adapter

import android.app.Activity
import android.app.Application
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.preference.PreferenceManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.commandintefaces.ChangeLocaleInterface
import com.cryptassist.commandintefaces.LanguageSelectedInterface
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.databinding.LanguageRowItemBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.LanguageViewModel
import com.cryptassist.viewModel.PopUpDialogViewModel
import com.cryptassist.viewModel.PreferredLanguageViewModel
import java.util.*

class PreferedLanguageAdapter(private val context: Context, private val preferredLanguageModel: ArrayList<PreferredLanguageViewModel>,
                              private val changeLocaleInterface: ChangeLocaleInterface, private var viewModel: LanguageViewModel) : RecyclerView.Adapter<PreferedLanguageAdapter.MyViewHolder>() {
    private val preferredLanguageViewModel1: PreferredLanguageViewModel = PreferredLanguageViewModel(context as Activity)
    private val prefManager = PrefManager.getInstance(context)
    private var myLocale: Locale? = null
    private lateinit var dialogPopUpBinding: DialogPopUpBinding
    private lateinit var dialogUtils: Dialog
    private var popUpDialogViewModel: PopUpDialogViewModel? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val languageRowItemBinding = DataBindingUtil.inflate<LanguageRowItemBinding>(layoutInflater, R.layout.language_row_item, parent, false)
        dialogPopUpBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_pop_up, null, false)
        dialogUtils = DialogUtils.createCustomDialog(context, dialogPopUpBinding.root)
        return MyViewHolder(languageRowItemBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val preferredLanguageViewModel = preferredLanguageModel[position]
        try {
            if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE, ""))) {
                holder.languageRowItemBinding.cbSelect.isChecked = preferredLanguageViewModel.getCountryName().equals(prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE, ""))
            } else {
                holder.languageRowItemBinding.cbSelect.isChecked = preferredLanguageViewModel.getCountryName().equals("English")
            }
        } catch (e: Exception) {
        }
        holder.bind(preferredLanguageViewModel)
        holder.languageRowItemBinding.onClick = object : LanguageSelectedInterface {
            override fun onLanguageSelected() {
                try {
                    if (holder.languageRowItemBinding.cbSelect.isChecked) {
                        prefManager.savePreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE, context.resources.getStringArray(R.array.prefer_languages_codes)[position])
                        prefManager.savePreference(AppConstants.SELECTED_PREFERRED_LANGUAGE, preferredLanguageViewModel.getCountryName())
                        CommonUtils.changeLang(context as Activity, context.resources.getStringArray(R.array.prefer_languages_codes)[position])
                        popUpDialogViewModel = PopUpDialogViewModel(context, dialogUtils, context.getString(R.string.change_language_successfully), "")
                        dialogPopUpBinding.onClickOk = popUpDialogViewModel
                        dialogUtils.show()
//                        var languageViewModel = LanguageViewModel(context.application)
//                        viewModel.users?.value =
                        viewModel.setLanguage(context.resources.getStringArray(R.array.prefer_languages_codes)[position])
//                        languageViewModel.setLanguage(context.resources.getStringArray(R.array.prefer_languages_codes)[position])
                        notifyDataSetChanged()
                    } else {

                        prefManager.savePreference(AppConstants.SELECTED_PREFERRED_LANGUAGE, "")
                        prefManager.savePreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE, "")
                    }
                } catch (e: Exception) {
                }
            }

        }


    }


    override fun getItemCount(): Int {
        return preferredLanguageModel.size
    }

    inner class MyViewHolder(val languageRowItemBinding: LanguageRowItemBinding) : RecyclerView.ViewHolder(languageRowItemBinding.root) {
        fun bind(preferredLanguageViewModel: PreferredLanguageViewModel) {
            this.languageRowItemBinding.country = preferredLanguageViewModel
        }
    }


}
