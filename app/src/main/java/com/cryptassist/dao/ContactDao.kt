package com.cryptassist.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.cryptassist.model.AddContactModel

@Dao
interface ContactDao {

    @Insert
    fun insert(addContactModel: AddContactModel)

    @Query("SELECT * from contact_table")
    fun getContacts(): LiveData<List<AddContactModel>>
}