package com.cryptassist.factory

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.cryptassist.viewModel.SendFragmentViewModel

@Suppress("UNCHECKED_CAST")
class CustomViewModelFactory(private val activity: Activity) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SendFragmentViewModel(activity) as T
    }


}