package com.cryptassist.assistivewidget

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.provider.Settings
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast


import com.cryptassist.R

import org.greenrobot.eventbus.EventBus


/**
 * Created by 司维 on 2017/10/11.
 */

class MultiTaskMainView(private val mContext: Context, attrs: AttributeSet?) : RelativeLayout(mContext, attrs) {
    private var imageView_home: ImageView? = null
    private var imageView_favor: ImageView? = null
    private var imageView_back: ImageView? = null
    private var imageView_recents: ImageView? = null
    private var favor_back: ImageView? = null
    private var mLayoutParams: WindowManager.LayoutParams? = null
    private var linearLayout_mutitaskview: LinearLayout? = null
    private var relativeLayout_mutitaskview: View? = null
    private var relativeLayout_LayoutParams: RelativeLayout.LayoutParams? = null

    private val onClickListener = OnClickListener { view ->
        Log.d(TAG, "onClickListener")
        when (view.id) {
            R.id.image_home -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.addCategory(Intent.CATEGORY_HOME)
                mContext.startActivity(intent)
                MyViewHolder.hideMutiTaskMainView()
                MyViewHolder.showEasyTouchView()
            }
            R.id.image_back -> {
                handleAccessibilityClick(GLOBAL_ACTION_BACK)
                MyViewHolder.hideMutiTaskMainView()
                MyViewHolder.showEasyTouchView()
            }
            R.id.image_recents -> {
                handleAccessibilityClick(GLOBAL_ACTION_RECENTS)
                MyViewHolder.hideMutiTaskMainView()
                MyViewHolder.showEasyTouchView()
            }
            R.id.image_star -> replaceToFavorView()
            R.id.favor_back -> replaceToMainView()
            R.id.add_0 -> {
            }
            else -> {
            }
        }
    }

    init {
        initLayoutParams()
        initImageView()
    }

    fun initImageView() {
        imageView_home = findViewById(R.id.image_home)
        imageView_favor = findViewById(R.id.image_star)
        imageView_back = findViewById(R.id.image_back)
        imageView_recents = findViewById(R.id.image_recents)
        imageView_home!!.setOnClickListener(onClickListener)
        imageView_favor!!.setOnClickListener(onClickListener)
        imageView_back!!.setOnClickListener(onClickListener)
        imageView_recents!!.setOnClickListener(onClickListener)
    }

    fun initFavor() {
        favor_back = findViewById(R.id.favor_back)
        favor_back!!.setOnClickListener(onClickListener)
    }

    fun initLayoutParams() {
        mLayoutParams = WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                0,
                0,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT
        )
        mLayoutParams!!.gravity = Gravity.CENTER
        relativeLayout_mutitaskview = LayoutInflater.from(mContext).inflate(R.layout.mutitask_main_layout, null)
        relativeLayout_LayoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        relativeLayout_LayoutParams!!.addRule(RelativeLayout.CENTER_IN_PARENT)
        this.addView(relativeLayout_mutitaskview, relativeLayout_LayoutParams)
        linearLayout_mutitaskview = findViewById(R.id.linearlayout_view)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (!isInMyView(event)) {//未点击view中区域
                replaceToMainView()
                MyViewHolder.hideMutiTaskMainView()
                MyViewHolder.showEasyTouchView()
                Log.d(TAG, "onTouchEvent")
            }
        }
        return super.onTouchEvent(event)
    }

    fun isInMyView(event: MotionEvent): Boolean {
        val location = IntArray(2)
        linearLayout_mutitaskview!!.getLocationOnScreen(location)
        val x = location[0]
        val y = location[1]
        return event.x > x &&
                event.x < x + linearLayout_mutitaskview!!.width &&
                event.y > y &&
                event.y < y + linearLayout_mutitaskview!!.height
    }

    fun getmLayoutParams(): WindowManager.LayoutParams? {
        return mLayoutParams
    }

    private fun handleAccessibilityClick(message: String) {
        if (MyViewHolder.isServiceRunning) {
            if (message === GLOBAL_ACTION_BACK) {
                EventBus.getDefault().post(MessageEvent(GLOBAL_ACTION_BACK))
            }
            if (message === GLOBAL_ACTION_RECENTS) {
                EventBus.getDefault().post(MessageEvent(GLOBAL_ACTION_RECENTS))
            }
        } else {
            Toast.makeText(mContext, "tpast", Toast.LENGTH_SHORT).show()
            val i = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(i)
        }
    }

    private fun replaceToFavorView() {
        this.removeView(relativeLayout_mutitaskview)
        relativeLayout_mutitaskview = LayoutInflater.from(mContext).inflate(R.layout.mutitask_favor_layout, null)
        this.addView(relativeLayout_mutitaskview, relativeLayout_LayoutParams)
        initFavor()
    }

    private fun replaceToMainView() {
        this.removeView(relativeLayout_mutitaskview)
        relativeLayout_mutitaskview = LayoutInflater.from(mContext).inflate(R.layout.mutitask_main_layout, null)
        this.addView(relativeLayout_mutitaskview, relativeLayout_LayoutParams)
        initImageView()
    }

    companion object {
        private val TAG = "MutiTaskView"
        val GLOBAL_ACTION_BACK = "BACK"
        val GLOBAL_ACTION_RECENTS = "RECENTS"
    }
}
