package com.cryptassist.assistivewidget

import android.app.Activity
import android.databinding.DataBindingUtil
import android.support.v4.view.GravityCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.WindowManager
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.databinding.FragmentFeedbackDialogBinding
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.FeedbackViewModel

/**
 * Created by 司维 on 2017/10/11.
 */

class MyViewHolder(context2: Activity, isDashBoard: Boolean) {

    init {


        activity = context2/*.getApplicationContext()*/
        if (isDashBoard) {
            mEasyTouchView = EasyTouchView(activity, null)
            mutiTaskMainView = MultiTaskMainView(activity, null)
            WindowManagerInstance.setApplicationContext(activity)
            mWindowManager = WindowManagerInstance.newInstance()

        }
    }

    companion object {
        private val TAG = "MyViewHolder"
        private lateinit var activity: Activity
        private var mEasyTouchView: EasyTouchView? = null
        private var mutiTaskMainView: MultiTaskMainView? = null
        private var mWindowManager: WindowManager? = null
        var isServiceRunning = false

        fun showEasyTouchView() {
            try {
                if (!EasyTouchView.isAlive) {//未存在
                    val layoutParams = mEasyTouchView?.getmLayoutParams()
                    EasyTouchView.isAlive = true
                    mWindowManager!!.addView(mEasyTouchView, layoutParams)
                    Log.d(TAG, "show")
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun hideEasyTouchView() {
            try {
                if (EasyTouchView.isAlive) {//已存在
                    EasyTouchView.isAlive = false
                    mWindowManager!!.removeView(mEasyTouchView)
                    Log.d(TAG, "hide")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun showMutiTaskMainView() {
            val layoutParams = mutiTaskMainView!!.getmLayoutParams()
            mWindowManager!!.addView(mutiTaskMainView, layoutParams)
        }

        fun hideMutiTaskMainView() {
            mWindowManager!!.removeView(mutiTaskMainView)
        }

        fun openMutiTaskWindow() {
            DashboardActivity.drawerLayout.closeDrawer(GravityCompat.START)
            hideEasyTouchView()
            val layoutInflater: LayoutInflater
            layoutInflater = LayoutInflater.from(activity)
            val fragmentFeedbackDialogBinding = DataBindingUtil.inflate<FragmentFeedbackDialogBinding>(layoutInflater, R.layout.fragment_feedback_dialog, null, false)
            val dialogUtils = DialogUtils.createCustomDialog(activity, fragmentFeedbackDialogBinding.root)
            val feedbackViewModel = FeedbackViewModel(dialogUtils, fragmentFeedbackDialogBinding, activity)
            fragmentFeedbackDialogBinding.clickIcon = feedbackViewModel
            dialogUtils.show()
            //        showMutiTaskMainView();
        }

        fun setIsServiceRunning(isRunning: Boolean) {
            isServiceRunning = isRunning
        }
    }

    //    private static void showPromptingDialog(){
    //        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
    //        alertDialog.setTitle(mContext.getResources().getString(R.string.hint));
    //        alertDialog.setMessage(mContext.getResources().getString(R.string.hint_content_accessibility));
    //        alertDialog.setCancelable(false);
    //        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
    //            @Override
    //            public void onClick(DialogInterface dialogInterface, int i) {
    //                mContext.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
    //                Toast.makeText(mContext, R.string.toast, Toast.LENGTH_LONG).show();
    //                isDialogShow = false;
    //            }
    //        });
    //        alertDialog.setNegativeButton(mContext.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
    //            @Override
    //            public void onClick(DialogInterface dialogInterface, int i) {
    //                isDialogShow = false;
    //            }
    //        });
    //        alertDialog.show();
    //    }
}
