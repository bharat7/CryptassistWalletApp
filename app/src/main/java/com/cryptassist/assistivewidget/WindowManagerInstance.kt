package com.cryptassist.assistivewidget

import android.content.Context
import android.view.WindowManager

/**
 * Created by 司维 on 2017/10/12.
 */

object WindowManagerInstance {
    private var windowManager: WindowManager? = null
    private var applicationContext: Context? = null

    fun newInstance(): WindowManager {
        if (windowManager == null) {
            windowManager = applicationContext!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        }
        return windowManager as WindowManager
    }

    fun setApplicationContext(context: Context) {
        applicationContext = context
    }
}
