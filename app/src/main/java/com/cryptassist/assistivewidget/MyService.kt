package com.cryptassist.assistivewidget

import android.accessibilityservice.AccessibilityService
import android.content.Intent
import android.util.Log
import android.view.accessibility.AccessibilityEvent

import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class MyService : AccessibilityService() {

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        EventBus.getDefault().unregister(this)
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        MyViewHolder.setIsServiceRunning(true)
        Log.d(TAG, "isServiceRunning")
    }

    override fun onAccessibilityEvent(accessibilityEvent: AccessibilityEvent) {

    }

    override fun onInterrupt() {
        Log.d(TAG, "onInterrupt")
    }

    override fun onUnbind(intent: Intent): Boolean {
        MyViewHolder.setIsServiceRunning(false)
        Log.d(TAG, intent.toString())
        return super.onUnbind(intent)
    }

    @Subscribe
    fun onEventMainThread(event: MessageEvent) {
        val msg = event.message
        if (MultiTaskMainView.GLOBAL_ACTION_BACK == msg) {
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
        }
        if (MultiTaskMainView.GLOBAL_ACTION_RECENTS == msg) {
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS)
        }
    }

    companion object {
        private val TAG = "MyService"
    }

}
