package com.cryptassist.assistivewidget

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.PixelFormat
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.WindowManager
import android.widget.LinearLayout

import com.cryptassist.R


/**
 * Created by 司维 on 2017/10/11.
 */

class EasyTouchView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    private var mLayoutParams: WindowManager.LayoutParams? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.easytouch_view_layout, this)
        screenWidth = this.resources.displayMetrics.widthPixels
        screenHeight = this.resources.displayMetrics.heightPixels
        initLayoutParams()

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        viewWidth = this.width
        viewHeight = this.height
    }

    fun initLayoutParams() {
        mLayoutParams = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                0,
                0,
                PixelFormat.TRANSPARENT
        )
        mLayoutParams!!.type = WindowManager.LayoutParams.TYPE_PHONE
        mLayoutParams!!.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        mLayoutParams!!.gravity = Gravity.TOP or Gravity.LEFT
        mLayoutParams!!.x = screenWidth
        mLayoutParams!!.y = screenHeight / 10
        /* int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mLayoutParams = new WindowManager.LayoutParams();
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;

        } else {
            mLayoutParams = new WindowManager.LayoutParams();
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;

        }
        mLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);*/
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                xInView = event.x
                yInView = event.y
                xDownInScreen = event.rawX
                yDownInScreen = event.rawY - getStatusBarHeight()
                xInScreen = event.rawX
                yInScreen = event.rawY - getStatusBarHeight()
            }
            MotionEvent.ACTION_MOVE -> {
                xInScreen = event.rawX
                yInScreen = event.rawY - getStatusBarHeight()
                mLayoutParams!!.x = (xInScreen - xInView).toInt()
                mLayoutParams!!.y = (yInScreen - yInView).toInt()
                updateViewPosition()
            }
            MotionEvent.ACTION_UP -> if (xInScreen == xDownInScreen && yInScreen == yDownInScreen) {//表示并未发生move事件，可看作点击事件
                MyViewHolder.openMutiTaskWindow()
            } else {//发生了move事件，需要进行回归两边的动画
                startViewPositionAnimator()
            }
        }
        return super.onTouchEvent(event)
    }

    private fun updateViewPosition() {
        val windowManager = WindowManagerInstance.newInstance()
        windowManager.updateViewLayout(this, mLayoutParams)
    }

    private fun startViewPositionAnimator() {
        val valueAnimator = ObjectAnimator.ofFloat(0f,  1f)
        //Duration 默认300ms
        valueAnimator.addUpdateListener { animation ->
            val value = animation.animatedValue as Float
            if (mLayoutParams!!.x + viewWidth / 2 <= screenWidth / 2) {//向左
                mLayoutParams!!.x = (mLayoutParams!!.x.toFloat() * (1 - value)).toInt()
            } else {
                mLayoutParams!!.x += ((screenWidth - mLayoutParams!!.x).toFloat() * value).toInt()
            }
            updateViewPosition()
        }
        valueAnimator.start()
    }

    private fun getStatusBarHeight(): Float {//获取状态栏高度
        val rectangle = Rect()
        this.getWindowVisibleDisplayFrame(rectangle)
        statusBarHeight = rectangle.top
        return statusBarHeight.toFloat()
    }

    fun getmLayoutParams(): WindowManager.LayoutParams? {
        return mLayoutParams
    }

    companion object {
        private val TAG = "EasyTouchView"
        private var statusBarHeight: Int = 0
        private var screenWidth: Int = 0
        private var screenHeight: Int = 0
        private var viewWidth: Int = 0
        private var viewHeight: Int = 0
        var isAlive = false
        /** 
         *  记录当前手指位置在屏幕上的横坐标值 
         */
        private var xInScreen: Float = 0.toFloat()
        /**
         *  记录当前手指位置在屏幕上的纵坐标值 
         */
        private var yInScreen: Float = 0.toFloat()
        /**
         *  记录手指按下时在屏幕上的横坐标的值  
         */
        private var xDownInScreen: Float = 0.toFloat()
        /**
         *  记录手指按下时在屏幕上的纵坐标的值  
         */
        private var yDownInScreen: Float = 0.toFloat()
        /**
         *  记录手指按下时在小悬浮窗的View上的横坐标的值 
         */
        private var xInView: Float = 0.toFloat()
        /**
         *  记录手指按下时在小悬浮窗的View上的纵坐标的值 
         */
        private var yInView: Float = 0.toFloat()
    }

}
