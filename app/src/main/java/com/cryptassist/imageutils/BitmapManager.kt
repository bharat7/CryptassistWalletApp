/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cryptassist.imageutils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log

import java.io.FileDescriptor
import java.util.WeakHashMap

/**
 * This class provides several utilities to cancel bitmap decoding.
 *
 *
 * The function decodeFileDescriptor() is used to decode a bitmap. During
 * decoding if another thread wants to cancel it, it calls the function
 * cancelThreadDecoding() specifying the Thread which is in decoding.
 *
 *
 * cancelThreadDecoding() is sticky until allowThreadDecoding() is called.
 *
 *
 * You can also cancel decoding for a set of threads using ThreadSet as
 * the parameter for cancelThreadDecoding. To put a thread into a ThreadSet,
 * use the add() method. A ThreadSet holds (weak) references to the threads,
 * so you don't need to remove Thread from it if some thread dies.
 */
class BitmapManager private constructor() {
    private val lock = java.lang.Object()

    private val mThreadStatus = WeakHashMap<Thread, ThreadStatus>()

    private enum class State {
        CANCEL, ALLOW
    }

    private class ThreadStatus {

        var mState = State.ALLOW
        var mOptions: BitmapFactory.Options? = null

        override fun toString(): String {

            var s: String
            if (mState == State.CANCEL) {
                s = "Cancel"
            } else if (mState == State.ALLOW) {
                s = "Allow"
            } else {
                s = "?"
            }
            s = "thread state = $s, options = $mOptions"
            return s
        }
    }

    class ThreadSet : Iterable<Thread> {

        private val mWeakCollection = WeakHashMap<Thread, Any>()

        fun add(t: Thread) {

            mWeakCollection[t] = null
        }

        fun remove(t: Thread) {

            mWeakCollection.remove(t)
        }

        override fun iterator(): Iterator<Thread> {

            return mWeakCollection.keys.iterator()
        }
    }

    /**
     * Get thread status and create one if specified.
     */
    @Synchronized
    private fun getOrCreateThreadStatus(t: Thread): ThreadStatus {

        var status: ThreadStatus? = mThreadStatus[t]
        if (status == null) {
            status = ThreadStatus()
            mThreadStatus[t] = status
        }
        return status
    }

    /**
     * The following three methods are used to keep track of
     * BitmapFaction.Options used for decoding and cancelling.
     */
    @Synchronized
    private fun setDecodingOptions(t: Thread,
                                   options: BitmapFactory.Options) {

        getOrCreateThreadStatus(t).mOptions = options
    }

    @Synchronized
    internal fun getDecodingOptions(t: Thread): BitmapFactory.Options? {

        val status = mThreadStatus[t]
        return status?.mOptions
    }

    @Synchronized
    internal fun removeDecodingOptions(t: Thread) {

        val status = mThreadStatus[t]
        status?.mOptions = null
    }

    /**
     * The following two methods are used to allow/cancel a set of threads
     * for bitmap decoding.
     */
    @Synchronized
    fun allowThreadDecoding(threads: ThreadSet) {

        for (t in threads) {
            allowThreadDecoding(t)
        }
    }

    @Synchronized
    fun cancelThreadDecoding(threads: ThreadSet) {

        for (t in threads) {
            cancelThreadDecoding(t)
        }
    }

    /**
     * The following three methods are used to keep track of which thread
     * is being disabled for bitmap decoding.
     */
    @Synchronized
    fun canThreadDecoding(t: Thread): Boolean {

        val status = mThreadStatus[t]
                ?: // allow decoding by default
                return true

        return status.mState != State.CANCEL
    }

    @Synchronized
    fun allowThreadDecoding(t: Thread) {

        getOrCreateThreadStatus(t).mState = State.ALLOW
    }

    @Synchronized
    fun cancelThreadDecoding(t: Thread) {

        val status = getOrCreateThreadStatus(t)
        status.mState = State.CANCEL
        if (status.mOptions != null) {
            status.mOptions!!.requestCancelDecode()
        }

        // Wake up threads in waiting list
        lock.notifyAll()
    }

    /**
     * A debugging routine.
     */
    @Synchronized
    fun dump() {

        val i = mThreadStatus.entries.iterator()

        while (i.hasNext()) {
            val entry = i.next()
            Log.v(TAG, "[Dump] Thread " + entry.key + " ("
                    + entry.key.id
                    + ")'s status is " + entry.value)
        }
    }

    /**
     * The real place to delegate bitmap decoding to BitmapFactory.
     */
    fun decodeFileDescriptor(fd: FileDescriptor,
                             options: BitmapFactory.Options): Bitmap? {

        if (options.mCancel) {
            return null
        }

        val thread = Thread.currentThread()
        if (!canThreadDecoding(thread)) {
            // Log.d(TAG, "Thread " + thread + " is not allowed to decode.");
            return null
        }

        setDecodingOptions(thread, options)
        val b = BitmapFactory.decodeFileDescriptor(fd, null, options)

        removeDecodingOptions(thread)
        return b
    }

    companion object {

        private val TAG = "BitmapManager"

        private var sManager: BitmapManager? = null

        @Synchronized
        fun instance(): BitmapManager {

            if (sManager == null) {
                sManager = BitmapManager()
            }
            return sManager as BitmapManager
        }
    }
}
