package com.cryptassist.imageutils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment

import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

/** this class is used for image operation  */

object TakePictureUtilsOld {

    val TAKE_PICTURE = 1
    val PICK_GALLERY = 2
    val CROP_FROM_CAMERA = 3
    val bytesRead = 0
    fun takePicture(context: Activity, fileName: String) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        try {
//            val imagesFolder = File(Environment.getExternalStorageDirectory(), "MyCameraImages")
//            imagesFolder.mkdirs()
//            val image = File(imagesFolder, "image.jpg")
//            val uriSavedImage = Uri.fromFile(image)
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
//            context.startActivityForResult(intent, TAKE_PICTURE)
            val mImageCaptureUri = Uri.fromFile(File(context!!.getExternalFilesDir("temp"), fileName!! + ".jpg"))
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri)
            intent.putExtra("return-data", true)
            context.startActivityForResult(intent, TAKE_PICTURE)


        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }

    }

    /**
     * this method is used for take picture from gallery
     */
    fun openGallery(context: Activity) {

        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        context.startActivityForResult(photoPickerIntent, PICK_GALLERY)
    }

    fun openGalleryFragment(context: Fragment) {

        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        context.startActivityForResult(photoPickerIntent, PICK_GALLERY)
    }


    /**
     * this method is used for open crop image
     */
    fun startCropImage(context: Activity, fileName: String) {
        val intent = Intent(context, CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, File(context.getExternalFilesDir("temp"), fileName).path)
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 1)
        intent.putExtra(CropImage.ASPECT_Y, 1)
        intent.putExtra(CropImage.OUTPUT_X, 600)
        intent.putExtra(CropImage.OUTPUT_Y, 600)
        context.startActivityForResult(intent, CROP_FROM_CAMERA)
    }


    @Throws(IOException::class)
    fun copyStream(input: InputStream, output: OutputStream) {

        val buffer = ByteArray(1024)
        var bytesRead:Int =0
        while(input.read(buffer).also { bytesRead = it } >=0) {
            output.write(buffer, 0, bytesRead)
        }

    }


}
