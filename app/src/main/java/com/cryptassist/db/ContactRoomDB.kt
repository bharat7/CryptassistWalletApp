package com.cryptassist.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.cryptassist.dao.ContactDao
import com.cryptassist.model.AddContactModel

@Database(entities = [AddContactModel::class], version = 1, exportSchema = false)
abstract class ContactRoomDB : RoomDatabase() {
    abstract fun contactDao(): ContactDao


    companion object {
        private var INSTANCE: ContactRoomDB? = null

        fun getDataBase(context: Context): ContactRoomDB? {
            if (INSTANCE == null) {
                synchronized(ContactRoomDB::class) {

                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context, ContactRoomDB::class.java, "contact_database")
                                .fallbackToDestructiveMigration()
//                                .addCallback(sRoomDatabaseCallback)
                                .build()
                    }
                }
            }
            return INSTANCE
        }
//        private val sRoomDatabaseCallback = object  : RoomDatabase.Callback(){
//
//            override fun onOpen(db: SupportSQLiteDatabase) {
//                super.onOpen(db)
//                PopulateDbAsync()
//            }
//        }
    }

    /*private class PopulateDbAsync internal constructor(db: ContactRoomDB): AsyncTask<Void, Void, Void>() {


        private val mDao: ContactDao
        init {
            mDao = db.contactDao()
        }

        override fun doInBackground(vararg params: Void?): Void {

        }
    }*/
}