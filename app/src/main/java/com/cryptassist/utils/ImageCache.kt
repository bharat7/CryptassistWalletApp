/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cryptassist.utils

import android.graphics.Bitmap
import android.os.Build
import android.support.v4.util.LruCache
import android.util.Log

import com.cryptassist.BuildConfig


/**
 * This class holds our bitmap caches (memory and disk).
 */
class ImageCache private constructor(memCacheSizePercent: Float) {
    private var mMemoryCache: LruCache<String, Bitmap>? = null

    init {
        init(memCacheSizePercent)
    }

    private fun init(memCacheSizePercent: Float) {
        val memCacheSize = calculateMemCacheSize(memCacheSizePercent)
        // Set up memory cache
        if (BuildConfig.DEBUG)
            Log.d(TAG, "Memory cache created (size = $memCacheSize)")
        mMemoryCache = object : LruCache<String, Bitmap>(memCacheSize) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int {
                val bitmapSize = getBitmapSize(bitmap) / 1024
                return if (bitmapSize == 0) 1 else bitmapSize
            }
        }
    }

    fun addBitmapToCache(data: String?, bitmap: Bitmap?) {
        if (data == null || bitmap == null)
            return
        // Add to memory cache
        if (mMemoryCache != null && mMemoryCache!!.get(data) == null)
            mMemoryCache!!.put(data, bitmap)
    }

    fun getBitmapFromMemCache(data: String): Bitmap? {
        if (mMemoryCache != null) {
            val memBitmap = mMemoryCache!!.get(data)
            if (memBitmap != null) {
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "Memory cache hit")
                return memBitmap
            }
        }
        return null
    }

    companion object {
        private val CACHE_PERCENTAGE = 0.1f
        private val TAG = "ImageCache"
        val INSTANCE = ImageCache(CACHE_PERCENTAGE)

        fun getBitmapSize(bitmap: Bitmap?): Int {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) bitmap!!.byteCount else bitmap!!.rowBytes * bitmap.height
            // Pre HC-MR1
        }

        fun calculateMemCacheSize(percent: Float): Int {
            if (percent < 0.05f || percent > 0.8f) {
                throw IllegalArgumentException("setMemCacheSizePercent - percent must be " + "between 0.05 and 0.8 (inclusive)")
            }
            return Math.round(percent * Runtime.getRuntime().maxMemory() / 1024)
        }
    }

}
