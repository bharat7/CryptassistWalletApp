package com.cryptassist.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout

import com.cryptassist.R
import com.cryptassist.activity.AddContactActivity
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.databinding.CommonToolbarBinding
import com.cryptassist.databinding.FragmentToolbarBinding
import com.cryptassist.fragment.AddAddressFragment


class ToolbarFragment : Fragment(), View.OnClickListener {

    internal var rootView: View? = null
    internal var context: Context? = null
    internal var isContactAdded: Boolean = false
    internal var fragment: Fragment? = null
    internal var fragmentTransaction: FragmentTransaction? = null
    internal lateinit var mFragmentManager: FragmentManager
    //Toolbar
    private var commonToolbarBinding: FragmentToolbarBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        commonToolbarBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_toolbar, container, false)
        //        rootView = inflater.inflate(R.layout.common_toolbar, container, false);
        context = activity
        mFragmentManager = activity!!.supportFragmentManager
        commonToolbarBinding!!.rlToggle.setOnClickListener(this)
        commonToolbarBinding!!.ivRight.setOnClickListener(this)
        if (DashboardActivity.fragment != null && DashboardActivity.fragment is AddAddressFragment && DashboardActivity.addContactModels.size > 0) {
            isContactAdded=true
            commonToolbarBinding!!.ivRight.visibility = View.VISIBLE
        } else {
            commonToolbarBinding!!.ivRight.visibility = View.GONE
        }
        return commonToolbarBinding!!.root
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.rlToggle -> {
                CommonUtils.hideKeyPad(context as Activity)
                DashboardActivity.drawerLayout.openDrawer(GravityCompat.START)
            }

            R.id.ivRight -> {
                CommonUtils.hideKeyPad(context as Activity)
                val intent = Intent(context, AddContactActivity::class.java)
                context!!.startActivity(intent)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        if (DashboardActivity.fragment != null && DashboardActivity.fragment is AddAddressFragment && DashboardActivity.addContactModels.size > 0) {
            commonToolbarBinding!!.ivRight.visibility = View.VISIBLE
        } else {
            commonToolbarBinding!!.ivRight.visibility = View.GONE
        }
    }
}// Required empty public constructor

