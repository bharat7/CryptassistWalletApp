package com.cryptassist.utils

import android.util.Log

/**
 * Created by Ashutosh Kumar on 20/10/2017.
 */
object LogUtils {


    private val DEBUG = true

    fun LOGD(tag: String, message: String) {
        if (DEBUG) {
            Log.d(tag, message)
        }
    }

    fun LOGV(tag: String, message: String) {
        if (DEBUG) {
            Log.v(tag, message)
        }
    }

    fun LOGI(tag: String, message: String) {
        if (DEBUG) {
            Log.i(tag, message)
        }
    }

    fun LOGW(tag: String, message: String) {
        if (DEBUG) {
            Log.w(tag, message)
        }
    }

    fun LOGE(tag: String, message: String) {
        if (DEBUG) {
            Log.e(tag, message)
        }
    }

}
