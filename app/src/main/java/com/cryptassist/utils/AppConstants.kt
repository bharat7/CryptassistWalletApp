package com.cryptassist.utils

object AppConstants {
    const val PERMISSION_REQUEST_CODE = 100
    const val SEND_ADDRESS_REQUEST_CODE = 102
    const val PERMISSION_REQUEST_SCANNER = 101
    const val USER_NAME = "user_name"
    const val USER_ADDRESS = "user_address"
    const val USER_IMAGE = "user_image"
    const val USER_EMAIL = "user_email"
    const val NOTIFICATION_ID = "notification_id"
    const val SENDER_ID = "479209210545" // Project ID of App from Google Console
    const val REQUEST_CONTACT_CODE = 101
    const val IS_APP_OLDER = "is_first_time_opened"
    const val IS_WALLET_CREATED = "is_wallet_created"
    const val TOUCHID_NOT_SELECTED = "touchid_not_selected"
    const val TOUCHID_SELECTED = "touchid_selected"
    const val SELECTED_PREFERRED_LANGUAGE = "selected_preferred_language"
    const val SELECTED_PREFERRED_LANGUAGE_CODE = "selected_preferred_language_code"
    const val IS_BACKUP_SUCCESSFULL = "is_backup_successfull"
    const val REQUEST_CODE_PASS_CODE = 123
    const val IS_FINGER_ENABLED = "is_finger_enabled"
    const val VALIDATE_PHRASE_LIST = "validate_phrase_list"
    const val CONTACT_NAME = "contact_name"
    const val CONTACT_ADDRESS = "contact_address"
    const val CONTACT_EMAIL = "contact_email"
    const val CTA_ADDRESS = "cta_address"
    const val FROM_SEND = "from_send"


    const val AUTH_EMAIL = "authUser@cryptassist.io"/*"AgH3Cjx2B7cMgSRyBCoPg5AL30tUDmUOGVH0yFJrfytDXBYUqO8X7nk1lV6X/S/1eINKVlwZotX/k51zgSZHZGtY30L2vCmGSmLXGThRAIBwoSaQQNnmAidWqrMOPd8oKy8="*/
    const val AUTH_PASSWORD = "fucked!@#+_*&fuck)"/*"AgGGmJjtFcBeen0iy6t8db/CQ3eZpDgofYcGo8mraO4IWr/lpkZxv9ntzW5EQiFyxRWXESJb6HvAme28gIdNgYIOmiaUM/dpOLX87wWkIGl891MNg/xlgak6NwxGlrCWJQU="*/
    const val ABOUT_US = "about_us"
    var AUTH_TOKEN: String? = null
    const val USER_ID = "user_id"
    const val EMAIL_ID = "email_id"

    /*It is using for Web services communication */

    const val SUCCESS_CODE = 200
    const val SUCCESS_CODE_201 = 201
    const val SUCCESS_CODE_204 = 204
    const val NOT_VERIFIED = 307
    const val DEACTIVATED_CODE = 401
    const val INVALID_CODE = 501
    const val INVALID_CODE_500 = 500
    const val AUTHENTICATION_ERROR_CODE = 400
}