package com.cryptassist.utils

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.cryptassist.R

object DialogUtils {


       fun createCustomDialog(context: Context, layoutResourceId: View): Dialog {
           //        AlertDialog.Builder builder = new AlertDialog.Builder(context);
           //        builder.setView(layoutResourceId);
           val dialog = Dialog(context, R.style.PauseDialogAnimation)
           dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
           dialog.setCanceledOnTouchOutside(false)
           //        dialog.setCancelable(false);
           dialog.setContentView(layoutResourceId)
           dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
           val layoutParams = dialog.window!!.attributes
           layoutParams.dimAmount = .7f
           layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
           layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
           dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
           dialog.window!!.attributes = layoutParams
           return dialog

       }



}
