package com.cryptassist.utils

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout

import com.cryptassist.R
import com.cryptassist.activity.*
import com.cryptassist.databinding.CommonToolbarBinding
import kotlinx.android.synthetic.main.common_toolbar.*
import com.cryptassist.assistivewidget.MessageEvent
import com.cryptassist.databinding.ActivityProfileBinding
import com.cryptassist.viewModel.ProfileAddressViewModel
import org.greenrobot.eventbus.EventBus


class ActivityToolbarFragment : Fragment(), View.OnClickListener {
    lateinit var activityProfileBinding: ActivityProfileBinding
    internal var context: Context? = null
    internal var fragment: Fragment? = null
    internal var fragmentTransaction: FragmentTransaction? = null
    internal lateinit var mFragmentManager: FragmentManager
    private var profileAddressviewModel: ProfileAddressViewModel? = null

    //Toolbar
    internal lateinit var mBinding:CommonToolbarBinding

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.context=context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding=DataBindingUtil.inflate(inflater,R.layout.common_toolbar,container,false)

//        activityProfileBinding=DataBindingUtil.inflate(inflater,R.layout.activity_profile,null,false)
//        profileAddressviewModel = ProfileAddressViewModel(activity as Activity,activityProfileBinding)
        if ((context is BuyCoinActivity)||(context is BuyCoinExchangeActivity)||(context is BuyCoinSummaryActivity)){
                 mBinding.tvTitle.visibility=View.VISIBLE

        }
        else{
            mBinding.tvTitle.visibility=View.GONE
        }
        mBinding. ivNavigation.setImageResource(R.mipmap.back_arrow)
        mFragmentManager = activity!!.supportFragmentManager
        mBinding.rlToggle.setOnClickListener(this)
        if (context != null && context is ProfileActivity ) {
            mBinding.ivRight.setImageResource(R.drawable.edit_profile);
            mBinding.ivRight.visibility = View.VISIBLE
        } else {
            mBinding.ivRight.visibility = View.GONE
        }


        return mBinding.root
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.rlToggle -> activity!!.finish()
        }
    }
}
