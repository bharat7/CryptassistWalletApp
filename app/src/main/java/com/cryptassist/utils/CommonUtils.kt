package com.cryptassist.utils


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Typeface
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.Spanned
import android.text.format.DateFormat
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.cryptassist.R
import tgio.rncryptor.RNCryptorNative
import java.io.*
import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


@SuppressLint("StaticFieldLeak")
object CommonUtils {

    var Tag: String? = null
    var accept: Boolean = false
    var imageNameLocal: String? = null
    var tvYes: TextView? = null
    var tvNo: TextView? = null
    var tvEnableGPS: TextView? = null
    private  var  dialogProgress: Dialog? = null


    private val USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$"

    val timeStamp: String
        get() {
            val timestamp = System.currentTimeMillis() / 1000L
            val tsTemp = "" + timestamp
            return "" + tsTemp
        }

    val timeZone: String
        get() = TimeZone.getDefault().id.toString()


    fun nameValidate(text: String): Boolean {
        val pattern = Pattern.compile("^[a-z0-9_-]{3,15}$")
        val matcher = pattern.matcher(text)
        return matcher.find()
    }

    fun showProgressDialog(context: Context) {
        if (context != null) {

             dialogProgress =Dialog(context, R.style.PauseDialogAnimation);
            dialogProgress?.setContentView(R.layout.progress_dialog)
            dialogProgress?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            val layoutParams = dialogProgress?.window!!.attributes
            layoutParams.dimAmount = .4f
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            dialogProgress?.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            dialogProgress?.window!!.attributes = layoutParams
            dialogProgress?.show()

        }
    }
    fun hideProgressDialog(context: Context) {
        if (context != null) {
            if (dialogProgress!=null) {


                dialogProgress?.dismiss()
            }

        }
    }


    fun getDateCurrentTimeZone(timestamp: Long): String {
        try {
            val calendar = Calendar.getInstance()
            val tz = TimeZone.getDefault()
            calendar.timeInMillis = timestamp * 1000
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.timeInMillis))
            val sdf = SimpleDateFormat("dd MMM yyyy")
            val currenTimeZone = calendar.time as Date
            return sdf.format(currenTimeZone)
        } catch (e: Exception) {
        }

        return ""
    }

    fun saveLocale(activity: Activity, lang: String) {
        val langPref = "Language"
        val prefs = activity.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(langPref, lang)
        editor.commit()
    }

    fun changeLang(activity: Activity, lang: String) {
        if (lang.equals("", ignoreCase = true))
            return
        var myLocale = Locale(lang)
        saveLocale(activity, lang)
        Locale.setDefault(myLocale)
        val config = android.content.res.Configuration()
        config.locale = myLocale
        activity.resources.updateConfiguration(config, activity.resources.displayMetrics)

    }

    fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()




    fun isOnline(context: Context): Boolean {
        if (!CheckIfNull(context)) {
            val conMgr = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = conMgr.activeNetworkInfo

            if (netInfo == null || !netInfo.isConnected
                    || !netInfo.isAvailable) {

                return false
            }
        }

        return true
    }

    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun saveStringPreferences(context: Context, key: String, value: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun setRegular(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/MyriadPro-Regular.otf")
    }


    fun requestPermission(context: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
        }
    }


    fun requestPermissionStorage(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), AppConstants.PERMISSION_REQUEST_CODE)
        }
    }

    fun requestPermissionCamera(activity: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA), AppConstants.PERMISSION_REQUEST_CODE)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA), AppConstants.PERMISSION_REQUEST_CODE)
        }
    }

    fun checkPermissionCamera(context: Activity): Boolean {
        val result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
        return result == PackageManager.PERMISSION_GRANTED
    }


    fun <T> CheckIfNull(objectToCheck: T?): Boolean {
        return if (objectToCheck == null) true else false
    }

    fun checkPermissionStorage(context: Activity): Boolean {
        val result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            result1 == PackageManager.PERMISSION_GRANTED

        } else {
            false
        }
    }

    fun StringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }

    fun BitMapToString(bitmap: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, clientMarketCap a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }


    fun getTimeStamp(date_time: String, format: String): String {

        val formatter = SimpleDateFormat(format)
        formatter.timeZone = TimeZone.getDefault()
        val datee: Date
        try {
            datee = formatter.parse(date_time) as Date
            var timestamp = "" + datee.time
            if (timestamp.length > 10) {
                timestamp = "" + java.lang.Long.parseLong(timestamp) / 1000L
            }
            return timestamp
        } catch (pe: ParseException) {
            pe.printStackTrace()
            return ""
        }

    }

    fun hideKeyPad(activity: Activity) {
        try {
            val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getDateTimeOfTimestampDateTime(timeStamp: Long): String {
        val objFormatter = SimpleDateFormat("dd MMM yy, hh:mm a")

        val objCalendar = Calendar.getInstance()

        objCalendar.timeInMillis = timeStamp * 1000//edit
        val result = objFormatter.format(objCalendar.time)
        objCalendar.clear()
        return result
    }

    fun getDateTimeOfTimestamp2(timeStamp: Long): String {
        val objFormatter = SimpleDateFormat("EEE, dd/MM/yyyy")

        val objCalendar = Calendar.getInstance()

        objCalendar.timeInMillis = timeStamp * 1000//edit
        val result = objFormatter.format(objCalendar.time)
        objCalendar.clear()
        return result
    }

    fun getDateOfTimestamp(timeStamp: Long): String {
        val objFormatter = SimpleDateFormat("dd MMM yyyy, hh:mm a")

        val objCalendar = Calendar.getInstance()

        objCalendar.timeInMillis = timeStamp * 1000//edit
        val result = objFormatter.format(objCalendar.time)
        objCalendar.clear()
        return result
    }

    fun getTime(timeStamp: Long): String {
        val objFormatter = SimpleDateFormat("dd/MM/yyyy")

        val objCalendar = Calendar.getInstance()

        objCalendar.timeInMillis = timeStamp * 1000//edit
        val result = objFormatter.format(objCalendar.time)
        objCalendar.clear()
        return result
    }


    fun hideKeyboard(activity: Activity) {
        val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused com.blockworkout.view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no com.blockworkout.view currently has focus, clientMarketCap a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboard(context: Context) {
        val inputMethodManager = (context as Activity).getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused com.blockworkout.view, so we can grab the correct window token from it.
        var view = context.currentFocus
        //If no com.blockworkout.view currently has focus, clientMarketCap a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(context)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun getTimestampOfDate(str_date: String): String {
        val formatter = SimpleDateFormat("dd-MM-yyy")
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = formatter.parse(str_date) as Date

        } catch (e1: ParseException) {
            e1.printStackTrace()
        }

        val value = date!!.time / 1000L
        val timestampValue = value.toString()
        val cal = Calendar.getInstance()
        val tz = cal.timeZone
        return timestampValue
    }

    fun getTimestampOfDateYYYMMDD(str_date: String): String {
        val formatter = SimpleDateFormat("yyy-MM-dd")
        var date: Date? = null
        try {
            date = formatter.parse(str_date) as Date

        } catch (e1: ParseException) {
            e1.printStackTrace()
        }

        val value = date!!.time / 1000L
        return value.toString()
    }

    fun addDayToDate(date: String, noOfDay: Int): String {
        val sdf = SimpleDateFormat("dd-MM-yyy")
        var dtStartDate: Date? = null
        try {
            dtStartDate = sdf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val c = Calendar.getInstance()
        c.time = dtStartDate
        c.add(Calendar.DATE, noOfDay)  // number of days to add
        return sdf.format(c.time)
    }

    /**
     * this method is used to convert the HTML text.
     *
     * @param html
     * @return
     */
    fun fromHtml(html: String): Spanned {
        var html = html
        val result: Spanned
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            html = html.replace("</li>", "<br><>br")
            result = Html.fromHtml(html)

        }
        return result
    }

    fun fromHtmlss(html: String): Spanned {
        var html = html
        val result: Spanned
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html = html.replace("</p><p>", "<br>")
            html = html.replace("<p>", "")
            html = html.replace("</p>", "")
            html = html.replace("<br><br><br>", "<br><br>")
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            html = html.replace("<p>", "")
            html = html.replace("</p>", "<br><br>")
            html = html.replace("</li>", "<br><br>")
            html = html.replace("<li>", "\u2022")
            result = Html.fromHtml(html)

        }
        return result
    }

    fun addDayToDateOtherFormat(date: String, noOfDay: Int): String {
        val sdf = SimpleDateFormat("dd-MM-yyy")
        var dtStartDate: Date? = null
        try {
            dtStartDate = sdf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val c = Calendar.getInstance()
        c.time = dtStartDate
        c.add(Calendar.DATE, noOfDay)  // number of days to add
        return sdf.format(c.time)
    }


    fun saveIntPreferences(context: Context, key: String, value: Int) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.commit()
    }

    fun getIntPreferences(context: Context, key: String): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getInt(key, 0)
    }

    fun savePreferencesBoolean(context: Context, key: String, value: Boolean) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getPreferencesBoolean(context: Context, key: String): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(key, false)
    }

    fun setFragment(fragment: Fragment, removeStack: Boolean, activity: FragmentActivity, mContainer: Int) {
        val fragmentManager = activity.supportFragmentManager
        val ftTransaction = fragmentManager.beginTransaction()
        if (removeStack) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            ftTransaction.replace(mContainer, fragment)
        } else {

            ftTransaction.replace(mContainer, fragment)
            ftTransaction.addToBackStack(null)

        }
        ftTransaction.commit()
    }

    fun setFragmentNew(fragment: Fragment, removeStack: Boolean, activity: AppCompatActivity, mContainer: Int) {
        val fragmentManager = activity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.add(mContainer, fragment)
        fragmentTransaction.commit()
    }


    /*public static void setFragment(Fragment fragment, boolean removeStack, Activity activity, int mContainer) {
        android.app.FragmentManager mFragmentManager = activity.getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.add(mContainer, fragment);
        fragmentTransaction.commit();
    }*/


    fun isValidPhone(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            android.util.Patterns.PHONE.matcher(target)
                    .matches() && target.length >= 10 && target.length <= 20
        }
    }

    fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter ?: return

        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }

        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
    }

    fun getMessageInTimeStamps(context: Context, currentDayTimeStamp: Long, targetDayTimeStamp: Long): String? {
        var currentDayTimeStamp = currentDayTimeStamp
        var targetDayTimeStamp = targetDayTimeStamp

        currentDayTimeStamp = getTimeStampInFormat(currentDayTimeStamp)

        targetDayTimeStamp = getTimeStampInFormat(targetDayTimeStamp)
        val currentDay: Int
        val currentMonth: Int
        val currentYear: Int
        val currentHours: Int
        val currentMinutes: Int

        val targetDay: Int
        val targetMonth: Int
        val targetYear: Int
        val targetHours: Int
        val targetMinutes: Int


        currentDay = getDay(currentDayTimeStamp)
        currentMonth = getMonth(currentDayTimeStamp)
        currentYear = getYear(currentDayTimeStamp)
        currentHours = getHour(currentDayTimeStamp)
        currentMinutes = getMinutes(currentDayTimeStamp)

        targetDay = getDay(targetDayTimeStamp)
        targetMonth = getMonth(targetDayTimeStamp)
        targetYear = getYear(targetDayTimeStamp)
        targetHours = getHour(targetDayTimeStamp)
        targetMinutes = getMinutes(targetDayTimeStamp)

        if (currentDay == targetDay && currentMonth == targetMonth && currentYear == targetYear) {

            return getMeesageByTimeEquivalent(currentHours, currentMinutes, targetHours, targetMinutes)

        } else {
            if (currentYear == targetYear) {
                if (currentMonth == targetMonth) {
                    if (currentDay < targetDay) {

                        return if (targetDay - currentDay == 1) {
                            "Tomorrow" + " " + CommonUtils.getTimeInAMandPmFromTimestamp(targetDayTimeStamp)

                        } else if (targetDay - currentDay < 7) {
                            getDayOfWeek(targetDayTimeStamp)
                        } else {
                            targetDay.toString() + "/" + targetMonth + "/" + targetYear
                        }

                    } else if (currentDay > targetDay) {

                        return if (currentDay - targetDay == 1) {
                            CommonUtils.getDateTimeOfTimestamp(context, targetDayTimeStamp)

                        } else if (currentDay - targetDay < 7) {

                            getDayOfWeek(targetDayTimeStamp)

                        } else {
                            targetDay.toString() + "/" + targetMonth + "/" + targetYear
                        }
                    }
                } else return if (currentMonth > targetMonth) {
                    CommonUtils.getDateTimeOfTimestamp(context, targetDayTimeStamp)
                } else {
                    CommonUtils.getDateTimeOfTimestamp(context, targetDayTimeStamp)
                }
            } else return if (currentYear < targetYear) {
                CommonUtils.getDateTimeOfTimestamp(context, targetDayTimeStamp)
            } else {
                CommonUtils.getDateTimeOfTimestamp(context, targetDayTimeStamp)
            }
        }

        return null
    }

    private fun getMonth(timeStamp: Long): Int {

        val formatter1 = SimpleDateFormat("MM", Locale.ENGLISH)

        return Integer.parseInt(formatter1.format(java.sql.Date(java.lang.Long.valueOf(timeStamp) * 1000)))

    }

    private fun getHour(timeStamp: Long): Int {
        val calendar = Calendar.getInstance()

        calendar.time = Date(timeStamp * 1000)
        return calendar.get(Calendar.HOUR_OF_DAY)
    }

    private fun getMinutes(timeStamp: Long): Int {
        val calendar = Calendar.getInstance()
        calendar.time = Date(timeStamp * 1000)
        return calendar.get(Calendar.MINUTE)
    }


    private fun getYear(timeStamp: Long): Int {

        val formatter1 = SimpleDateFormat("yyyy", Locale.ENGLISH)

        return Integer.parseInt(formatter1.format(java.sql.Date(java.lang.Long.valueOf(timeStamp) * 1000)))

    }


    private fun getDay(timeStamp: Long): Int {

        val formatter1 = SimpleDateFormat("dd", Locale.ENGLISH)

        return Integer.parseInt(formatter1.format(Date(java.lang.Long.valueOf(timeStamp) * 1000)))

    }

    private fun getMeesageByTimeEquivalent(currentHour: Int, currentMinutes: Int, targetHour: Int, targetMinutes: Int): String {

        return if (targetHour > currentHour) {

            if (targetHour - currentHour == 1) {
                if (60 - currentMinutes + targetMinutes < 60) {
                    (60 - currentMinutes + targetMinutes).toString() + " minutes."
                } else {
                    ((60 - currentMinutes + targetMinutes) / 60).toString() + " hour and " + (60 - currentMinutes + targetMinutes - 60) + " minutes."

                }
            } else {
                (targetHour - currentHour).toString() + " hours"
            }
        } else if (targetHour < currentHour) {

            if (currentHour - targetHour == 1) {
                if (60 - targetMinutes + currentMinutes < 60) {
                    (60 - targetMinutes + currentMinutes).toString() + " minutes."
                } else {
                    ((60 - targetMinutes + currentMinutes) / 60).toString() + " hour and " + (60 - targetMinutes + currentMinutes - 60) + " minutes."

                }
            } else {
                (currentHour - targetHour).toString() + " hours"
            }
        } else {
            if (currentMinutes > targetMinutes) {
                (currentMinutes - targetMinutes).toString() + " minutes."
            } else if (currentMinutes < targetMinutes) {
                (targetMinutes - currentMinutes).toString() + " minutes."
            } else {

                "In " + CommonUtils.getTimeInAMandPmFromTimestamp(targetMinutes.toLong())
            }
        }
    }

    private fun getDateTimeOfTimestamp(context: Context, timeStamp: Long): String {
        val objFormatter: java.text.DateFormat
        objFormatter = SimpleDateFormat("d MMM yyyy , kk:mm ")

        val objCalendar = Calendar.getInstance()

        objCalendar.timeInMillis = timeStamp * 1000
        val result = objFormatter.format(objCalendar.time)
        objCalendar.clear()
        return result
    }

    private fun getTimeStampInFormat(currentDayTimeStamp: Long): Long {
        val timestamps = currentDayTimeStamp.toString() + ""

        return if (timestamps.trim { it <= ' ' }.length == 10) {
            currentDayTimeStamp
        } else if (timestamps.trim { it <= ' ' }.length == 13) {
            currentDayTimeStamp / 1000
        } else {
            currentDayTimeStamp
        }
    }

    private fun getDayOfWeek(timeStamp: Long): String {
        val calendar = Calendar.getInstance()

        calendar.time = Date(timeStamp * 1000)

        return getWeekDay(calendar.get(Calendar.DAY_OF_WEEK)) + " " + CommonUtils.getTimeInAMandPmFromTimestamp(timeStamp)
    }

    private fun getWeekDay(i: Int): String? {

        when (i) {
            1 ->

                return "Sunday"
            2 ->

                return "Monday"

            3 ->

                return "Tuesday"

            4 ->

                return "Wednesday"

            5 ->

                return "Thursday"

            6 ->

                return "Friday"

            7 ->

                return "Saturday"
        }

        return null
    }


    private fun getTimeInAMandPmFromTimestamp(timeStamp: Long): String {
        var timeStamp = timeStamp
        timeStamp = timeStamp * 1000
        val delegate = "hh:mm aaa"
        return DateFormat.format(delegate, Date(timeStamp)) as String
    }

    fun SendEmail(context: Activity, To: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, To)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "")
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(context, "There is no mEmail clientMarketCap installed.", Toast.LENGTH_SHORT).show()
        }

    }

    fun isValidEmail(target: CharSequence): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(target)
        return matcher.matches()
    }

    fun savePreferencesString(context: Context, key: String, value: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getPreferences(context: Context, key: String): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(key, "")
    }

    fun removePreferences(context: Activity, key: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.remove(key)
    }

    fun getPreferencesBoolean(context: Activity, key: String): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(key, false)
    }

    fun getPreferencesString(context: Context, key: String): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(key, "")
    }

    fun getDate(context: Context, timestamp_in_string: String): String {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val df = Date(dv)
/*
			String[] bits = str.split("-");
			 String mnth = bits[0];

		 */
        return SimpleDateFormat("MMM dd/yyyy,hh:mma").format(df)
    }

    fun getDateTimeFromTimStampChat(timestamp_in_string: String): String? {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val df = Date(dv)
        //@SuppressLint("SimpleDateFormat") String vv = new SimpleDateFormat("MMM dd,yyyy,hh:mma").format(df);
        @SuppressLint("SimpleDateFormat") var vv: String? = null
        try {
            vv = SimpleDateFormat("MMMM dd, yyyy hh:mm a").format(df)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return vv
    }

    fun getDateTimeFromTimStamp(timestamp_in_string: String): String? {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val df = Date(dv)
        //@SuppressLint("SimpleDateFormat") String vv = new SimpleDateFormat("MMM dd,yyyy,hh:mma").format(df);
        @SuppressLint("SimpleDateFormat") var vv: String? = null
        try {
            vv = SimpleDateFormat("dd MMM yyyy, hh:mm a").format(df)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return vv
    }

    fun getDateFromStamp(timestamp_in_string: String): String {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = dv
        return DateFormat.format("dd-MM-yyyy", cal).toString()
    }

    fun getTime(timestamp_in_string: String): String {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = dv
        return DateFormat.format("dd/MM/yyyy", cal).toString()
    }

    fun getDateTime(timestamp_in_string: String): String {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = dv
        return DateFormat.format("dd/MM/yyyy hh:mm aa", cal).toString()
    }

    fun getTimeFormat(timestamp_in_string: String): String {
        val dv = java.lang.Long.valueOf(timestamp_in_string) * 1000// its need to be in milisecond
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = dv
        return DateFormat.format("hh:mm aa", cal).toString()
    }

    fun isDateToday(milliSeconds: Long): Boolean {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds

        val getDate = calendar.time

        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        val startDate = calendar.time

        return getDate.compareTo(startDate) > 0
    }


    fun getHoursFromMillis(milliseconds: Long): String {
        return "" + (milliseconds / (1000 * 60 * 60) % 24).toInt()
    }

    fun getBitMapFromImageURl(imagepath: String, activity: Activity): Bitmap? {

        var bitmapFromMapActivity: Bitmap? = null
        var bitmapImage: Bitmap? = null
        try {

            val file = File(imagepath)
            // We need to recyle unused bitmaps
            bitmapImage?.recycle()
            bitmapImage = reduceImageSize(file, activity)
            var exifOrientation = 0
            try {
                val exif = ExifInterface(imagepath)
                exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            var rotate = 0

            when (exifOrientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90

                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180

                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
            }

            if (rotate != 0) {
                val w = bitmapImage!!.width
                val h = bitmapImage.height
                val mtx = Matrix()
                mtx.preRotate(rotate.toFloat())
                val myBitmap = Bitmap.createBitmap(bitmapImage, 0, 0, w, h,
                        mtx, false)
                bitmapFromMapActivity = myBitmap
            } else {
                val SCALED_PHOTO_WIDTH = 150
                val SCALED_PHOTO_HIGHT = 200
                val myBitmap = Bitmap.createScaledBitmap(bitmapImage!!,
                        SCALED_PHOTO_WIDTH, SCALED_PHOTO_HIGHT, true)
                bitmapFromMapActivity = myBitmap
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return bitmapFromMapActivity
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }

    fun reduceImageSize(f: File, context: Context): Bitmap? {

        var m: Bitmap? = null
        try {

            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(FileInputStream(f), null, o)

            val REQUIRED_SIZE = 150

            var width_tmp = o.outWidth
            var height_tmp = o.outHeight

            var scale = 1
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break
                width_tmp /= 2
                height_tmp /= 2
                scale *= 2
            }
            // Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            o2.inPreferredConfig = Bitmap.Config.ARGB_8888
            m = BitmapFactory.decodeStream(FileInputStream(f), null, o2)
        } catch (e: FileNotFoundException) {
        } catch (e: Exception) {

        }

        return m
    }

    fun printKeyHash(context: Context): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            val packageName = context.applicationContext.packageName

            packageInfo = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)


            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }

        } catch (e1: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        } catch (e: Exception) {
        }

        return key
    }


    fun dp2px(resources: Resources, dp: Float): Float {
        val scale = resources.displayMetrics.density
        return dp * scale + 0.5f
    }

    fun sp2px(resources: Resources, sp: Float): Float {
        val scale = resources.displayMetrics.scaledDensity
        return sp * scale
    }

    fun getScreenWidthResolution(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val width = metrics.widthPixels
        val height = metrics.heightPixels
        return width
    }

    fun getScreenHeightResolution(context: Context): String {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)
        val width = metrics.widthPixels
        val height = metrics.heightPixels
        return height.toString() + ""
    }


    fun AvenirRoman(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "font/Avenir_Roman.ttf")
    }

    fun AvenirHeavy(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "font/Avenir-Heavy.ttf")
    }

    fun AvenirMedium(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "font/Avenir-Medium.ttf")
    }


    /**
     * Called for checking internet connection
     */
    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }


    fun printLog(tag: String, msg: String) {}

    /*  public static void setLanguage(Context context, String mode) {
        locale = new Locale(mode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        printLog("CommonUtils  setLanguage : ", mode);
        savePreferencesString(context, AppConstant.PREF_LANGUAGE, mode);
    }

    public static String getLanguage(Context context){
        // SharedPreferences info = context.getSharedPreferences(AppConstant.PREF_LANGUAGE, Context.MODE_PRIVATE);
        printLog("CommonUtils getLanguage : ", getPreferencesString(context, AppConstant.PREF_LANGUAGE));
        return getPreferencesString(context, AppConstant.PREF_LANGUAGE);
    }

    */
    /**
     * @param context
     * @param login_status
     *//*
    public static void saveLoginStatus(Context context, boolean login_status) {
        SharedPreferences info = context.getSharedPreferences(AppConstant.PREF_USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.putBoolean(AppConstant.PREF_KEYS.LOGIN_STATUS, login_status);
        editor.commit();
    }

    */
    /**
     * @param context
     * @return
     *//*
    public static boolean getLoginStatus(Context context) {
        SharedPreferences info = context.getSharedPreferences(AppConstant.PREF_USER_INFO, Context.MODE_PRIVATE);
        return info.getBoolean(AppConstant.PREF_KEYS.LOGIN_STATUS, false);
    }*/

    fun encrypt(stringToBeEncrypted: String): String {
        val password = "base64:OrC1q3MNTUtKJpFaHMBmU036W+x3o+/DvPdXZt9lOVY="
        return try {
            String(RNCryptorNative().encrypt(stringToBeEncrypted, password), Charset.defaultCharset())
        } catch (e: Exception) {
            ""
        }
    }

    fun decrypt(stringToBeDecrypted: String): String {
        val password = "base64:OrC1q3MNTUtKJpFaHMBmU036W+x3o+/DvPdXZt9lOVY="
        return try {
            RNCryptorNative().decrypt(stringToBeDecrypted, password)
        } catch (e: Exception) {
            ""
        }

    }
}