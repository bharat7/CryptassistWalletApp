package com.cryptassist.utils

import android.content.Context
import android.widget.Toast

/**
 * Created by Ashutosh Kumar on 17/12/16.
 */

class ToastUtils private constructor() {

    init {
        throw Error("U will not able to instantiate it")
    }

    companion object {

        /**
         * @param context
         * @param message
         * @return
         */
        fun showToastShort(context: Context, message: String) {
            if (message.isNotEmpty()) Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

        /**
         * @param context
         * @param message
         * @return
         */
        fun showToastLong(context: Context, message: String) {
            if (message.isNotEmpty()) Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }

}
