package com.cryptassist.fragment


import android.app.Activity
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.cryptassist.R
import com.cryptassist.databinding.FragmentBackupWalletBinding
import com.cryptassist.databinding.ScreenshotDialogBinding
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.ScreenshotDialogViewModel


/**
 * Created by yarolegovich on 25.03.2017.
 */

class BackupFragment : Fragment() {
    //    FragmentBackupWalletBinding fragmentReceiveBinding;
    private lateinit var fragmentBackupWalletBinding: FragmentBackupWalletBinding
    private lateinit var screenshotDialogBinding: ScreenshotDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentBackupWalletBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_backup_wallet, container, false)
        if (CommonUtils.getPreferencesBoolean(activity as Activity, AppConstants.IS_BACKUP_SUCCESSFULL)) {
            fragmentBackupWalletBinding.llBackup.visibility = View.VISIBLE
            fragmentBackupWalletBinding.llParent.visibility = View.GONE
        } else {
            fragmentBackupWalletBinding.llBackup.visibility = View.GONE
            fragmentBackupWalletBinding.llParent.visibility = View.VISIBLE
        }

        return fragmentBackupWalletBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        screenshotDialogBinding = DataBindingUtil.inflate(layoutInflater!!, R.layout.screenshot_dialog, null, false)
        val dialogUtils = DialogUtils.createCustomDialog(activity as Activity, screenshotDialogBinding.root)
        val screenshotDialogViewModel = ScreenshotDialogViewModel(dialogUtils, activity as Activity)
        screenshotDialogBinding.onClick = screenshotDialogViewModel
        val btGotIt = view.findViewById<View>(R.id.btGotIt) as Button
        btGotIt.setOnClickListener { dialogUtils.show() }
    }

    override fun onResume() {
        super.onResume()
        if (CommonUtils.getPreferencesBoolean(activity as Activity, AppConstants.IS_BACKUP_SUCCESSFULL)) {
            fragmentBackupWalletBinding.llBackup.visibility = View.VISIBLE
            fragmentBackupWalletBinding.llParent.visibility = View.GONE
        } else {
            fragmentBackupWalletBinding.llBackup.visibility = View.GONE
            fragmentBackupWalletBinding.llParent.visibility = View.VISIBLE
        }
    }


}
