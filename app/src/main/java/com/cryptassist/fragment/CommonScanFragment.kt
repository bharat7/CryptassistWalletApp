package com.cryptassist.fragment


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.cryptassist.utils.AppConstants

import com.cryptassist.utils.CheckPermission
import com.google.zxing.Result

import me.dm7.barcodescanner.zxing.ZXingScannerView


class CommonScanFragment : Fragment(), ZXingScannerView.ResultHandler {
    private var scannerView: ZXingScannerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        scannerView = ZXingScannerView(activity)

        if (CheckPermission.checkCameraPermission(activity as  Activity)) {
            scannerView!!.stopCamera()
            scannerView!!.setResultHandler(this)
            scannerView!!.startCamera()
        } else {
            CheckPermission.requestCameraPermission(activity as Activity)
        }

        return scannerView
    }

    override fun onStart() {
        super.onStart()
        scannerView!!.setResultHandler(this)
        scannerView!!.startCamera()

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1001 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    return
                }

            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (CheckPermission.checkPermissionStorage(activity as Activity)) {
            scannerView!!.setResultHandler(this)
            scannerView!!.startCamera()
        } else {
            CheckPermission.requestCameraPermission(activity as Activity)
        }

    }

    override fun onResume() {
        super.onResume()
        scannerView!!.setResultHandler(this)
        scannerView!!.startCamera()
    }

    override fun handleResult(result: Result) {
        val myText = result.text
        scannerView!!.startCamera()
        val returnIntent = Intent()
        returnIntent.putExtra(AppConstants.CTA_ADDRESS, result.text)
        activity?.setResult(Activity.RESULT_OK, returnIntent)
        activity?.finish()
    }
}
