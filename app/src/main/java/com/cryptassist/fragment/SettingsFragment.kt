package com.cryptassist.fragment


import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cryptassist.R
import com.cryptassist.activity.SplashActivity
import com.cryptassist.adapter.SettingAdapter
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.viewModel.LanguageViewModel
import com.cryptassist.viewModel.SettingViewModel

import java.util.ArrayList


class SettingsFragment : Fragment() {

    private val settingViewModel = SettingViewModel()
    private var settingViewModels = ArrayList<SettingViewModel>()
    private var settingAdapter: SettingAdapter? = null
    private lateinit var prefManager: PrefManager
    private lateinit var activity:Activity
    lateinit var languageViewModel: LanguageViewModel
  lateinit var  rvOptionList:RecyclerView;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvOptionList = activity.findViewById<View>(R.id.rvOptionList) as RecyclerView
        languageViewModel = ViewModelProviders.of(this).get(LanguageViewModel::class.java)
        languageViewModel.users?.observe(this, Observer {  })
        languageViewModel= ViewModelProviders.of(this).get(LanguageViewModel::class.java)
        languageViewModel.users?.observe(this, Observer { users ->


        })
        setListItems(rvOptionList)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.activity=context as Activity
        prefManager = PrefManager.getInstance(activity)
    }

    private fun setListItems(rvOptionList: RecyclerView) {
        settingViewModels.clear()
        settingViewModels = settingViewModel.getPreferredLanguageModels(activity)
        settingAdapter = SettingAdapter(activity, settingViewModels)
        rvOptionList.layoutManager = LinearLayoutManager(activity)
        rvOptionList.adapter = settingAdapter
    }

    override fun onResume() {
        super.onResume()

//        setListItems(rvOptionList)
        MyViewHolder.hideEasyTouchView()
    }
}
