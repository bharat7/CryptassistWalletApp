package com.cryptassist.fragment


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cryptassist.R

import com.cryptassist.adapter.TransactionAdapter

import com.cryptassist.databinding.FragmentTransactionBinding
import com.cryptassist.viewModel.TransactionViewModel

import java.util.ArrayList


class TransactionFragment : Fragment() {

    private var transactionFragmentBinding: FragmentTransactionBinding? = null

    private val transactionViewModel = TransactionViewModel()
    private var transactionViewModels = ArrayList<TransactionViewModel>()
    private var transactionAdapter: TransactionAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        transactionFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transaction, container, false)
        return transactionFragmentBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        transactionFragmentBinding!!.rvTransactionList.layoutManager = LinearLayoutManager(activity)
        transactionViewModels = transactionViewModel.preferredLanguageModels
        transactionAdapter = TransactionAdapter(activity!!, transactionViewModels)
        transactionFragmentBinding!!.rvTransactionList.adapter = transactionAdapter

    }
}
