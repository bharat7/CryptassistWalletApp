package com.cryptassist.fragment


import android.app.Activity
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cryptassist.R
import com.cryptassist.databinding.FragmentReceiveBinding
import com.cryptassist.viewModel.ReceiveViewModel


/**
 * Created by yarolegovich on 25.03.2017.
 */

class ReceiveFragment : Fragment() {
    private lateinit var fragmentReceiveBinding: FragmentReceiveBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentReceiveBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_receive, container, false)
        val receiveViewModel = ReceiveViewModel(activity as Activity)
        fragmentReceiveBinding.share = receiveViewModel
        return fragmentReceiveBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {}
}
