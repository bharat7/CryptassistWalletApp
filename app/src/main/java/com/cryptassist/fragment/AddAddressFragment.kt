package com.cryptassist.fragment


import android.app.Activity
import android.app.Activity.RESULT_OK
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.adapter.*
import com.cryptassist.commandintefaces.SendAddressInterface
import com.cryptassist.databinding.FragmentContactBinding
import com.cryptassist.model.AddContactModel
import com.cryptassist.model.ContactModel
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.AddAddressViewModel
import com.cryptassist.viewModel.AddContactViewModel
import com.cryptassist.viewModel.AddressViewModel
import java.util.*
import kotlin.collections.ArrayList
import android.content.Intent
import android.databinding.ObservableField


/**
 * Created by yarolegovich on 25.03.2017.
 */

class AddAddressFragment : Fragment(), TextWatcher {


    lateinit var mBinding: FragmentContactBinding
    lateinit var mInflater: LayoutInflater
    private var mAdapter: AddressListAdapter? = null
    lateinit var viewModel: AddressViewModel
    lateinit var viewModel2: AddAddressViewModel;
    private lateinit var newContactModels: ArrayList<AddContactViewModel>
    private lateinit var sendAddressInterface: SendAddressInterface
    private  var contacts: ArrayList<AddContactViewModel> = ArrayList()
    private var contactModels: List<AddContactModel> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)
        newContactModels = ArrayList()
        setAddress()
        viewModel2=AddAddressViewModel(activity as Activity)
        viewModel.allContacts.observe(this, Observer { allContacts ->
            this.contactModels = allContacts!!
            contacts = getContacts()!!
            if (arguments!=null && arguments!!.getString(AppConstants.FROM_SEND)!=null){
                for (i in contacts.indices){
                    contacts.get(i).isFromSend.set(true)

                }
            }
             setAdapter(contacts!!,mInflater!!)
            mAdapter!!.setPinnedHeaderTextColor(ContextCompat.getColor(activity!!, R.color.pinned_header_text))
            mBinding.rvList.setPinnedHeaderView(mInflater.inflate(R.layout.pinned_header_listview_side_header,  mBinding.rvList, false))
            mBinding.etSearch.addTextChangedListener(this)
            mBinding.rvList.adapter = mAdapter
            mBinding.rvList.setOnScrollListener(mAdapter)
            mBinding.rvList.setEnableHeaderTransparencyChanges(false)
            if (allContacts.isNotEmpty()){
                mBinding.llParent.visibility=View.GONE;
                mBinding.clParent.visibility=View.VISIBLE;
            }
            else{
                mBinding.llParent.visibility=View.VISIBLE;
                mBinding.clParent.visibility=View.GONE;
            }
        })


    }

    private fun setAddress() {
        sendAddressInterface = object : SendAddressInterface{
            override  fun sendAddress(position : Int){
                ctaAddress.set(contacts.get(position).mAddress.get())
                activity?.onBackPressed()
//                val intent = Intent(context, AddAddressFragment::class.java)
//                intent.putExtra(AppConstants.CTA_ADDRESS,contacts.get(position).mAddress.get())
//                targetFragment!!.onActivityResult(AppConstants.SEND_ADDRESS_REQUEST_CODE, RESULT_OK, intent)
//                activity?.onBackPressed()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false)
        mInflater=inflater
        mBinding.addContact=viewModel2;
        return mBinding.root
    }

    private fun setAdapter(contacts: ArrayList<AddContactViewModel>, layoutInflater: LayoutInflater) {
        contacts!!.sortWith(Comparator { lhs, rhs ->
            val lhsFirstLetter = if (TextUtils.isEmpty(lhs.mName.toString())) ' ' else lhs.mName.toString()[0]
            val rhsFirstLetter = if (TextUtils.isEmpty(rhs.mName.toString())) ' ' else rhs.mName.toString()[0]
            val firstLetterComparison = Character.toUpperCase(lhsFirstLetter) - Character.toUpperCase(rhsFirstLetter)
            if (firstLetterComparison == 0) lhs.mName.get().toString()?.compareTo(rhs.mName.get().toString()) else firstLetterComparison
        })
        mAdapter =  AddressListAdapter(contacts, activity!!, mInflater,sendAddressInterface)
        mBinding.rvList.adapter = mAdapter
        (mBinding.rvList.adapter as AddressListAdapter).notifyDataSetChanged()

    }

    private fun getContacts(): ArrayList<AddContactViewModel>? {
//        Int
        val result = ArrayList<AddContactViewModel>()
        result.clear()
        for (i in contactModels.indices){
            val contact = AddContactViewModel()
            contact.mName.set(contactModels[i].name)
            contact.mAddress.set(contactModels[i].address)
            contact.mEmail.set(contactModels[i].email)
            contact.userImage.set(contactModels[i].imagePath)
            result.add(contact)
        }

        return result
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


    }

    override fun afterTextChanged(s: Editable?) {
        var chk: String = ""
        var length: Int = s.toString().length
       if (length > 0) {

            newContactModels.clear()
            for (i in contactModels.indices) {
                try {
                    if (contactModels[i].name != null) {
                        chk =contactModels[i].name!!.substring(0, length)
                    }
                    if (chk.toLowerCase() == s.toString() || chk == s.toString()) {
                        val contact = AddContactViewModel()

                        contact.mName.set(contactModels[i].name)
                        contact.mAddress.set(contactModels[i].address)
                        contact.mEmail.set(contactModels[i].email)
                        newContactModels.add(contact)


                    }

                } catch (e: Exception) {
                    e.printStackTrace() }
            }
            setAdapter(newContactModels,mInflater!!)
            if (newContactModels.size > 0) {
                mBinding.rvList.setVisibility(View.VISIBLE);
            } else {
                mBinding.rvList.setVisibility(View.GONE);
            }
        } else {
            val contacts = getContacts()
           mBinding.rvList.setVisibility(View.VISIBLE);
           setAdapter(contacts!!,mInflater!!)
            }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
    companion object {
        var ctaAddress = ObservableField<String>()

    }

}
