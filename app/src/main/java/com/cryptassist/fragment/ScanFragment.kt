package com.cryptassist.fragment


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.cryptassist.R
import com.cryptassist.databinding.ScannerDialogBinding


import com.cryptassist.utils.CheckPermission
import com.cryptassist.sectionheadercomponents.IntentIntegrator
import com.cryptassist.utils.DialogUtils
import com.google.zxing.Result

import org.json.JSONException
import org.json.JSONObject

import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.view.WindowManager
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.activity.ScannerActivity
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.ScannerDialogViewModel
import com.cryptassist.viewModel.SendFragmentViewModel


class ScanFragment : Fragment(), ZXingScannerView.ResultHandler {

    private lateinit var scannerDialogBinding: ScannerDialogBinding
    private lateinit var dialogUtils: Dialog
    private lateinit var inflaters:LayoutInflater
    lateinit var scannerView: ZXingScannerView
    private lateinit var scannerDialogViewModel: ScannerDialogViewModel
    private lateinit var sendFragmentViewModel: SendFragmentViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        inflaters =inflater

        scannerView = ZXingScannerView(activity)

        if (CheckPermission.checkCameraPermission(activity as Activity)) {
            scannerView = ZXingScannerView(activity)

        } else {
            CheckPermission.requestCameraPermission(activity as Activity)
        }

        return scannerView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data!!)
        if (result != null) {
            //if qrcode has nothing in it
            if (result.contents == null) {
                Toast.makeText(activity, getString(R.string.result_not_found), Toast.LENGTH_LONG).show()
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    val obj = JSONObject(result.contents)
                    //setting values to textviews

                } catch (e: JSONException) {
                    e.printStackTrace()
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(activity, result.contents, Toast.LENGTH_LONG).show()
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1001 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    return
                }

            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (CheckPermission.checkPermissionStorage(activity as Activity)) {
            scannerView.setResultHandler(this)
            scannerView.startCamera()
        } else {
            CheckPermission.requestCameraPermission(activity as Activity)
        }

    }

    override fun onResume() {
        super.onResume()
        scannerView.setResultHandler(this)
        scannerView.startCamera()
    }

    override fun handleResult(result: Result) {
        val myText = result.text

          if (DashboardActivity.fragment is ScanFragment) {
              scannerDialogBinding = DataBindingUtil.inflate(inflaters, R.layout.scanner_dialog, null, false)
              dialogUtils = DialogUtils.createCustomDialog(activity as Activity, scannerDialogBinding.root)
              scannerDialogViewModel = ScannerDialogViewModel(activity as Activity, dialogUtils, scannerView)
              scannerView.setResultHandler(this)
              scannerDialogViewModel.ctaAddress.set(myText)
              scannerDialogBinding.scanDialog = scannerDialogViewModel
              val window = dialogUtils.getWindow()
              val wlp = window.getAttributes()
              wlp.gravity = Gravity.BOTTOM
              wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
              window.setAttributes(wlp)
              dialogUtils.show()
          }


    }


}
