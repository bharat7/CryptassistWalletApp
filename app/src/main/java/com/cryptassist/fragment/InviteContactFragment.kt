package com.cryptassist.fragment

import android.app.Activity
import android.databinding.DataBindingUtil
import android.databinding.generated.callback.OnClickListener
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.adapter.ContactsAdapter
import com.cryptassist.databinding.FragmentInviteBinding
import com.cryptassist.model.ContactModel
import com.cryptassist.sectionheadercomponents.PinnedHeaderListView
import java.util.*
import kotlin.collections.ArrayList
import android.content.Intent
import android.net.Uri
import android.telephony.SmsManager
import com.cryptassist.model.InviteModel
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.telephony.SmsMessage
import android.widget.Toast
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.PopUpDialogViewModel
import okhttp3.internal.Util


class InviteContactFragment : Fragment(), TextWatcher, View.OnClickListener {


    internal var show_fab_1: Animation? = null
    internal var hide_fab_1: Animation? = null
    internal var show_fab_2: Animation? = null
    internal var hide_fab_2: Animation? = null
    internal var show_fab_3: Animation? = null
    internal var hide_fab_3: Animation? = null
    internal var rotate_forward: Animation? = null
    internal var rotate_backward: Animation? = null
    private var FAB_Status = false
    private var mInflater: LayoutInflater? = null
    private var mListView: PinnedHeaderListView? = null
    private var mAdapter: ContactsAdapter? = null
    private lateinit var mBinding: FragmentInviteBinding
    private lateinit var newContactModels: ArrayList<ContactModel>
    private lateinit var inviteList: ArrayList<InviteModel>

    //2
    companion object {

        fun newInstance(): InviteContactFragment {
            return InviteContactFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mInflater = LayoutInflater.from((activity as Context?)!!)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_invite, container, false)
        newContactModels = ArrayList()
        inviteList = ArrayList()
        val contacts = getContacts()
        setAdapter(contacts!!, inflater)

        mBinding.rvList.setOnScrollListener(mAdapter)
        mBinding.llFab!!.fab1.setOnClickListener(this)
        mBinding.llFab!!.fab2.setOnClickListener(this)
        mBinding.llFab!!.fab3.setOnClickListener(this)
        mBinding.etSearch.addTextChangedListener(this)
        show_fab_1 = AnimationUtils.loadAnimation(activity, R.anim.fab1_show)
        hide_fab_1 = AnimationUtils.loadAnimation(activity, R.anim.fab1_hide)
        show_fab_2 = AnimationUtils.loadAnimation(activity, R.anim.fab2_show)
        hide_fab_2 = AnimationUtils.loadAnimation(activity, R.anim.fab2_hide)
        show_fab_3 = AnimationUtils.loadAnimation(activity, R.anim.fab3_show)
        hide_fab_3 = AnimationUtils.loadAnimation(activity, R.anim.fab3_hide)
        rotate_backward = AnimationUtils.loadAnimation(activity, R.anim.rotate_backward)
        rotate_forward = AnimationUtils.loadAnimation(activity, R.anim.rotate_forward)
        return mBinding.root
    }

    private fun setAdapter(contacts: ArrayList<ContactModel>, layoutInflater: LayoutInflater) {
        contacts.sortWith(Comparator { lhs, rhs ->
            val lhsFirstLetter = if (TextUtils.isEmpty(lhs.name)) ' ' else lhs.name!![0]
            val rhsFirstLetter = if (TextUtils.isEmpty(rhs.name)) ' ' else rhs.name!![0]
            val firstLetterComparison = Character.toUpperCase(lhsFirstLetter) - Character.toUpperCase(rhsFirstLetter)
            if (firstLetterComparison == 0) lhs.name!!.compareTo(rhs.name!!) else firstLetterComparison
        })

        mBinding.rvList.setEnableHeaderTransparencyChanges(false)
        mAdapter = ContactsAdapter(contacts, activity!!, layoutInflater, inviteList)
        mAdapter!!.setPinnedHeaderTextColor(ContextCompat.getColor(activity!!, R.color.pinned_header_text))
        mBinding.rvList.setPinnedHeaderView(mInflater!!.inflate(R.layout.pinned_header_listview_side_header, mBinding.rvList, false))
        mBinding.rvList.adapter = mAdapter
        mAdapter?.notifyDataSetChanged()
        mBinding.fab.setOnClickListener(View.OnClickListener {
            if (!FAB_Status) {
                mBinding.fab.startAnimation(rotate_forward)
                expandFAB()
                FAB_Status = true
            } else {
                mBinding.fab.startAnimation(rotate_backward)
                hideFAB()
                FAB_Status = false
            }
        })
    }


    private fun getContacts(): ArrayList<ContactModel>? {
//        val i = Int
        val result = ArrayList<ContactModel>()

        for (i in DashboardActivity.contactModels.indices) {
            val contact = ContactModel()

            contact.name = DashboardActivity.contactModels[i].firstLetterName
            contact.photoId = contact.firstLetterName
            contact.emailAddress = DashboardActivity.contactModels[i].emailAddress
            contact.phoneNo = DashboardActivity.contactModels[i].phoneNo
            result.add(contact)
        }

        return result
    }

    override fun afterTextChanged(s: Editable?) {
        var chk: String = ""
        var length: Int = s.toString().length
        if (length > 0) {

            newContactModels.clear()
            for (i in DashboardActivity.contactModels.indices) {
                try {
                    if (DashboardActivity.contactModels[i].name != null) {
                        chk = DashboardActivity.contactModels[i].name!!.substring(0, length)
                    }
                    if (chk.toLowerCase() == s.toString() || chk == s.toString()) {
                        val contact = ContactModel()

                        contact.name = DashboardActivity.contactModels[i].name
                        contact.phoneNo = DashboardActivity.contactModels[i].phoneNo
                        newContactModels.add(contact)


                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            setAdapter(newContactModels, mInflater!!)
            if (newContactModels.size > 0) {
//                tvNoContact.setVisibility(View.GONE);
//                mListView.setVisibility(View.VISIBLE);
//                getContactList(newContactModels);
            } else {
//                tvNoContact.setVisibility(View.VISIBLE);
//                mListView.setVisibility(View.GONE);
            }
        } else {
            val contacts = getContacts()
            setAdapter(contacts!!, mInflater!!)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    private fun expandFAB() {

        //Floating Action Button 1
        val layoutParams = mBinding.llFab?.fab1?.layoutParams as FrameLayout.LayoutParams
        layoutParams.rightMargin += (mBinding.llFab?.fab1?.width!! * 1.7).toInt()
        layoutParams.bottomMargin += (mBinding.llFab?.fab1?.height!! * 0.25).toInt()
        mBinding.llFab?.fab1?.layoutParams = layoutParams
        mBinding.llFab?.fab1?.startAnimation(show_fab_1)
        mBinding.llFab?.fab1?.isClickable = true

        //Floating Action Button 2
        val layoutParams2 = mBinding.llFab?.fab2?.layoutParams as FrameLayout.LayoutParams
        layoutParams2.rightMargin += (mBinding.llFab?.fab2?.width!! * 1.5).toInt()
        layoutParams2.bottomMargin += (mBinding.llFab?.fab2?.height!! * 1.5).toInt()
        mBinding.llFab?.fab2?.layoutParams = layoutParams2
        mBinding.llFab?.fab2?.startAnimation(show_fab_2)
        mBinding.llFab?.fab2?.isClickable = true

        //Floating Action Button 3
        val layoutParams3 = mBinding.llFab?.fab3?.layoutParams as FrameLayout.LayoutParams
        layoutParams3.rightMargin += (mBinding.llFab?.fab3?.width!! * 0.25).toInt()
        layoutParams3.bottomMargin += (mBinding.llFab?.fab3?.height!! * 1.7).toInt()
        mBinding.llFab?.fab3?.layoutParams = layoutParams3
        mBinding.llFab?.fab3?.startAnimation(show_fab_3)
        mBinding.llFab?.fab3?.isClickable = true
    }


    private fun hideFAB() {

        //Floating Action Button 1
        val layoutParams = mBinding.llFab?.fab1?.layoutParams as FrameLayout.LayoutParams
        layoutParams.rightMargin -= (mBinding.llFab?.fab1?.width!! * 1.7).toInt()
        layoutParams.bottomMargin -= (mBinding.llFab?.fab1?.height!! * 0.25).toInt()
        mBinding.llFab?.fab1?.layoutParams = layoutParams
        mBinding.llFab?.fab1?.startAnimation(hide_fab_1)
        mBinding.llFab?.fab1?.isClickable = false

        //Floating Action Button 2
        val layoutParams2 = mBinding.llFab?.fab2?.layoutParams as FrameLayout.LayoutParams
        layoutParams2.rightMargin -= (mBinding.llFab?.fab2?.width!! * 1.5).toInt()
        layoutParams2.bottomMargin -= (mBinding.llFab?.fab2?.height!! * 1.5).toInt()
        mBinding.llFab?.fab2?.layoutParams = layoutParams2
        mBinding.llFab?.fab2?.startAnimation(hide_fab_2)
        mBinding.llFab?.fab2?.isClickable = false

        //Floating Action Button 3
        val layoutParams3 = mBinding.llFab?.fab3?.layoutParams as FrameLayout.LayoutParams
        layoutParams3.rightMargin -= (mBinding.llFab?.fab3?.width!! * 0.25).toInt()
        layoutParams3.bottomMargin -= (mBinding.llFab?.fab3?.height!! * 1.7).toInt()
        mBinding.llFab?.fab3?.layoutParams = layoutParams3
        mBinding.llFab?.fab3?.startAnimation(hide_fab_3)
        mBinding.llFab?.fab3?.isClickable = false
    }

    override fun onClick(v: View?) {
         if (v!!.id == R.id.fab_1) {
             try {
                 val builder = StringBuilder()
                 var delim = ""
                 if (inviteList.size > 0) {
                     for (i: Int in inviteList.indices) {
                         //  For every phone number in our list
                         if (!inviteList.get(i).phoneNo.get().isNullOrBlank()) {
                             builder.append(delim).append(inviteList.get(i).phoneNo.get())
                             delim = ","
                         }
                     }


                     /** creates an sms uri */
                     val data = Uri.parse("sms:" + builder)
                     val intent = Intent(Intent.ACTION_SENDTO, data )
                     /** Setting sms uri to the intent */
                     intent.putExtra("sms_body", "dsdd");
                     intent.setPackage("com.whatsapp");
//                     intent.data = data
                     startActivity(intent)
                 } else {
                     noContactAdded()
                 }

             } catch (e: Exception) {
             }
         }

         else if (v!!.id == R.id.fab_3) {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.type = "text/plain"
            intent.data = (Uri.parse("mailto:"));
            val builder = StringBuilder()
            var delim = ""

            if (inviteList.size > 0) {
                for (i: Int in inviteList.indices) {
                    //  For every phone number in our list
                    if (!inviteList.get(i).emailId.get().isNullOrBlank()) {
                        builder.append(delim).append(inviteList.get(i).emailId.get())
                        delim = ","
                    }
                }

                val TO = arrayOf(builder.toString())
                intent.putExtra(Intent.EXTRA_EMAIL, TO)
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.")
                startActivity(Intent.createChooser(intent, "Send Email"))
                builder.setLength(0)
                delim = ""
            } else {
                noContactAdded()
            }

        } else if (v!!.id == R.id.fab_2) {
            try {
                val builder = StringBuilder()
                var delim = ""
                if (inviteList.size > 0) {
                    for (i: Int in inviteList.indices) {
                        //  For every phone number in our list
                        if (!inviteList.get(i).phoneNo.get().isNullOrBlank()) {
                            builder.append(delim).append(inviteList.get(i).phoneNo.get())
                            delim = ","
                        }
                    }
                val intent = Intent("android.intent.action.VIEW")

                /** creates an sms uri */
                val data = Uri.parse("sms:" + builder)

                /** Setting sms uri to the intent */
                intent.data = data
                startActivity(intent)
            } else {
                noContactAdded()
            }

            } catch (e: Exception) {
            }
        }

    }



    private fun noContactAdded() {
        val layoutInflater: LayoutInflater = LayoutInflater.from(activity as Context)
        val dialogPopUpBinding = DataBindingUtil.inflate<DialogPopUpBinding>(layoutInflater, R.layout.dialog_pop_up, null, false)
        val dialogUtils = DialogUtils.createCustomDialog(activity as Context, dialogPopUpBinding.root)
        val popUpDialogViewModel = PopUpDialogViewModel(activity!!, dialogUtils, activity!!.getString(R.string.no_contact_added), "")
        dialogPopUpBinding.onClickOk = popUpDialogViewModel
        dialogUtils.show()
    }

    fun whatsapp() {
        try {
            val builder = StringBuilder()
            var delim = ""
            if (inviteList.size > 0) {
                for (i: Int in inviteList.indices) {
                    //  For every phone number in our list
                    if (!inviteList.get(i).phoneNo.get().isNullOrBlank()) {
                        builder.append(delim).append(inviteList.get(i).phoneNo.get())
                        delim = ","
                    }
                }
                val sendIntent = Intent("android.intent.action.MAIN")
                sendIntent.setComponent(ComponentName("com.whatsapp", "com.whatsapp.Conversation"))
                sendIntent.setAction(Intent.ACTION_SEND)
                sendIntent.setType("text/plain")
                sendIntent.putExtra(Intent.EXTRA_TEXT, "")
                sendIntent.putExtra("kjjkjk", "9654159896"/*+builder */+ "@s.whatsapp.net")
                sendIntent.setPackage("com.whatsapp")
                activity!!.startActivity(sendIntent)
            }
        }
        catch(e:Exception)
        {
            Toast.makeText(activity,"Error/n"+ e.toString(),Toast.LENGTH_SHORT).show();
        }
    }
}



