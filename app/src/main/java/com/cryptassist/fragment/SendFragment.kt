package com.cryptassist.fragment


import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.arch.lifecycle.extensions.R.id.time
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.activity.ScannerActivity
import com.cryptassist.databinding.FragmentSendBinding
import com.cryptassist.factory.CustomViewModelFactory
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.viewModel.AddressViewModel
import com.cryptassist.viewModel.LiveDataViewModel
import com.cryptassist.viewModel.ScannerDialogViewModel
import com.cryptassist.viewModel.SendFragmentViewModel
import com.cryptassist.webservices.APIExecutor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import android.widget.TextView
import com.cryptassist.viewModel.DataRepository




class SendFragment : Fragment() {

    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: LiveDataViewModel
    private var disposable: Disposable? = null
    private lateinit var sendFragmentViewModel: SendFragmentViewModel
    private var binding: FragmentSendBinding? = null
    var selectedString: String? = null
    var mContext: Context? = null

    private val apiClient by lazy {
        APIExecutor.clientMarketCap()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.mContext = context
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send, container, false)
        sendFragmentViewModel = ViewModelProviders.of(this, CustomViewModelFactory(activity!!)).get(SendFragmentViewModel::class.java)
        binding?.onClickConfirm = sendFragmentViewModel
        DataRepository.getData(sendFragmentViewModel.mWalletAddress).observe(this, Observer {time ->
            if (time!=null && !time.equals("")) {
                sendFragmentViewModel.mWalletAddress.set("" + time)
            }} )
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ArrayAdapter(activity, R.layout.simple_spinner_item, resources.getStringArray(R.array.currency_types))
        binding?.ivScan?.setOnClickListener(View.OnClickListener {
            val intent = Intent(activity, ScannerActivity::class.java)
           startActivityForResult(intent, AppConstants.PERMISSION_REQUEST_SCANNER)
        })

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding?.spCurrency?.adapter = adapter
        binding?.spCurrency?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedString = binding?.spCurrency?.selectedItem.toString()
                callGetFiatApi(selectedString!!)
            }

        }
    }


    private fun callGetFiatApi(fiat: String) {
        val priceKey = "price_$selectedString".toLowerCase()
        CommonUtils.showProgressDialog(mContext!!)
        disposable = apiClient.fetchCoinDataService(fiat)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(mContext!!)
                            val jsonObject = result[0]
                            sendFragmentViewModel.price = jsonObject.get(priceKey)?.asDouble
                            sendFragmentViewModel.updateFiatConversion()
                            binding?.etCta?.setSelection(binding?.etCta?.text.toString().trim().length)
                        },
                        { error ->
                            CommonUtils.hideProgressDialog(mContext!!)
                            ToastUtils.showToastShort(mContext!!, error.message!!) },
                        { disposable?.dispose() }
                )
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AppConstants.PERMISSION_REQUEST_SCANNER) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                  binding?.etWallet?.setText(data?.getStringExtra(AppConstants.CTA_ADDRESS))
                } catch (e: Exception) {

                }

            }
        }
        else if (requestCode == AppConstants.SEND_ADDRESS_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    binding?.etWallet?.setText(data?.getStringExtra(AppConstants.CTA_ADDRESS))
                } catch (e: Exception) {

                }

            }
        }


    }

    override fun onResume() {
        super.onResume()

    }
}
