package com.cryptassist.fragment


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ArrayAdapter
import android.widget.Toast

import com.cryptassist.R
import com.cryptassist.databinding.FragmentHelpSupportBinding
import com.cryptassist.databinding.FragmentSendBinding

import java.security.PrivateKey


class HelpSupportFragment : Fragment() {
    private var fragmentHelpSupportBinding: FragmentHelpSupportBinding? = null
    private var progDailog: ProgressDialog? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentHelpSupportBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_help_support, container, false)
        progDailog = ProgressDialog.show(activity, "Loading", "Please wait...", true)
        progDailog!!.setCancelable(false)
        return fragmentHelpSupportBinding!!.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var url = ""
        if (arguments != null && arguments!!.getString("about_us") != null) {
            url = "https://travelflex.org/index.php#about"
        } else {
            url = "https://chatserver.comm100.com/chatWindow.aspx?siteId=229906&planId=415#"
        }
        //        String url="https://travelflex.org/index.php#about";
        fragmentHelpSupportBinding!!.webView.settings.javaScriptEnabled = true
        fragmentHelpSupportBinding!!.webView.settings.domStorageEnabled = true
        fragmentHelpSupportBinding!!.webView.settings.loadWithOverviewMode = true
        fragmentHelpSupportBinding!!.webView.settings.useWideViewPort = true
        fragmentHelpSupportBinding!!.webView.settings.builtInZoomControls = true
        fragmentHelpSupportBinding!!.webView.settings.pluginState = WebSettings.PluginState.ON
        fragmentHelpSupportBinding!!.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                progDailog!!.show()
                view.loadUrl(url)

                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                progDailog!!.dismiss()
            }
        }

        fragmentHelpSupportBinding!!.webView.loadUrl(url)


    }
}