package com.cryptassist.fragment


import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cryptassist.R
import com.cryptassist.databinding.FragmentAboutUsBinding
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.viewModel.AboutUsViewModel
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class AboutUsFragment : Fragment() {
    lateinit var viewModel: AboutUsViewModel

    private var mContext: Context? = null

    private var disposable: Disposable? = null
    private var disposable2: Disposable? = null
    private val apiClient by lazy {
        APIExecutor.clientApi()
    }


    lateinit var binding: FragmentAboutUsBinding
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AboutUsViewModel::class.java)
//        viewModel = AboutUsViewModel((mContext as Activity).application)




        if (AppConstants.AUTH_TOKEN == null) {
            val mJsonObject = JsonObject()
            mJsonObject.addProperty("email", CommonUtils.encrypt(AppConstants.AUTH_EMAIL))
            mJsonObject.addProperty("password", CommonUtils.encrypt(AppConstants.AUTH_PASSWORD))
            CommonUtils.showProgressDialog(mContext!!)
            apiClient.createTokenService(mJsonObject)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                result -> if (result.responseCode == AppConstants.SUCCESS_CODE) AppConstants.AUTH_TOKEN = result.token
                            },
                            { error -> ToastUtils.showToastShort(mContext!!, error.message!!) }

                    )
        }

        CommonUtils.showProgressDialog(mContext!!)
        val mJsonObject = JsonObject()
        mJsonObject.addProperty("page", CommonUtils.encrypt(AppConstants.ABOUT_US))
        disposable2 = apiClient.getContentService(mJsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(mContext!!)
                            if (result.responseCode == 200) viewModel.aboutUs.set(CommonUtils.decrypt(result.content))
                            else ToastUtils.showToastShort(mContext!!, CommonUtils.decrypt(result.responseMessage))
                        },
                        {
                            error -> ToastUtils.showToastShort(mContext!!, error.message!!)
                            CommonUtils.hideProgressDialog(mContext!!)},
                        { disposable2?.dispose() }
                )


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_us, container, false)
        binding.viewModel = viewModel
        return binding.root
    }


}
