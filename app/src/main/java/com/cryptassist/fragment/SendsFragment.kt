package com.cryptassist.fragment


import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.cryptassist.R
import com.cryptassist.activity.DashboardActivity
import com.cryptassist.databinding.FragmentSendBinding
import com.cryptassist.viewModel.SendViewModel


/**
 * Created by yarolegovich on 25.03.2017.
 */

class SendsFragment : Fragment() {
    lateinit var binding: FragmentSendBinding
    lateinit var viewModel: SendViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewModel = ViewModelProvider s.of(this).get(SendViewModel::class.java)
        viewModel = SendViewModel(activity as Context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send, container, false)
//        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ArrayAdapter(activity, R.layout.simple_spinner_item, resources.getStringArray(R.array.currency_types))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spCurrency.adapter = adapter
        binding.ivScan.setOnClickListener {
            DashboardActivity.mBinding.llInner?.vpMain?.currentItem = 2
        }
    }

    /*companion object {

        private val EXTRA_TEXT = "text"

        fun createFor(text: String): SendsFragment {
            val fragment = SendsFragment()
            val args = Bundle()
            args.putString(EXTRA_TEXT, text)
            fragment.arguments = args
            return fragment
        }
    }*/
}
