package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.cryptassist.R
import com.cryptassist.databinding.ActivityOrderListingBinding
import com.cryptassist.viewModel.OrderListingViewModel
import com.cryptassist.viewModel.OrderViewModel

class OrderListingActivity : AppCompatActivity() {
 lateinit var mBinding:ActivityOrderListingBinding
    lateinit var orderViewModel: OrderListingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(this,R.layout.activity_order_listing)
        orderViewModel= OrderListingViewModel(this);
        mBinding.viewModel=orderViewModel

    }
}
