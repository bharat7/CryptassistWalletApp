package com.cryptassist.activity

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View

import com.cryptassist.R
import com.cryptassist.adapter.ProfileAddressAdapter
import com.cryptassist.databinding.ActivityProfileBinding
import com.cryptassist.imageutils.CropImage
import com.cryptassist.imageutils.TakePictureUtilsOld
import com.cryptassist.imageutils.TakePictureUtilsOld.TAKE_PICTURE
import com.cryptassist.utils.AppConstants

import java.util.ArrayList
import com.cryptassist.viewModel.ProfileAddressViewModel
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream


class ProfileActivity : BaseActivity() {
    private var mContext: ProfileActivity? = null
    private var profileAddressedModel: ProfileAddressViewModel? = null
    private var profileAddressModelList = ArrayList<ProfileAddressViewModel>()
    lateinit var mBinding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
      mContext = this@ProfileActivity
        profileAddressedModel= ProfileAddressViewModel(mContext as Activity,mBinding)
        mBinding.viewModel = profileAddressedModel
        mBinding.llInner?.viewModelToolbar=profileAddressedModel

    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == TakePictureUtilsOld.PICK_GALLERY) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    val inputStream = mContext!!.contentResolver.openInputStream(data!!.data!!)
                    val fileOutputStream = FileOutputStream(File(mContext!!.getExternalFilesDir("temp"), profileAddressedModel?.imageName!! + ".jpg"))
                    TakePictureUtilsOld.copyStream(inputStream!!, fileOutputStream)
                    fileOutputStream.close()
                    inputStream.close()
                    TakePictureUtilsOld.startCropImage(mContext as Activity, profileAddressedModel?.imageName!! + ".jpg")
                } catch (e: Exception) {

                }

            }
        } else if (requestCode == TakePictureUtilsOld.TAKE_PICTURE) {
            // imageName="picture";
            if (resultCode == Activity.RESULT_OK) {
                TakePictureUtilsOld.startCropImage(mContext as Activity, profileAddressedModel?.imageName!! + ".jpg")
            }


        } else if (requestCode == TakePictureUtilsOld.CROP_FROM_CAMERA) {
            //  imageName="picture";
            if (resultCode == Activity.RESULT_OK) {

                var path: String? = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)

                }
                if (path == null) {
                    return
                }
                val bm = BitmapFactory.decodeFile(path)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                mBinding!!.ivProfileImage.setImageBitmap(bm)
                profileAddressedModel?.imagePath?.set(path)

            }
        }
    }

}
