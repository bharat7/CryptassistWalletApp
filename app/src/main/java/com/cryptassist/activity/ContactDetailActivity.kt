package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.cryptassist.R
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.databinding.ActivityContactDetailBinding
import com.cryptassist.databinding.FragmentContactBinding
import com.cryptassist.viewModel.ContactDetailViewModel

  class ContactDetailActivity : BaseActivity() {
    lateinit var viewModel2: ContactDetailViewModel;
    lateinit var binding: ActivityContactDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=DataBindingUtil.setContentView(this,R.layout.activity_contact_detail)
        viewModel2= ContactDetailViewModel(this);
        binding.contactDetail=viewModel2


    }
}
