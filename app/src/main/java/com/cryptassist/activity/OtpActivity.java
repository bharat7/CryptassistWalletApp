package com.cryptassist.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cryptassist.R;
import com.cryptassist.databinding.ActivityOtpBinding;
import com.cryptassist.viewModel.OtpViewModel;

public class OtpActivity extends BaseActivity {
    private ActivityOtpBinding mBinding;
    private OtpViewModel otpViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        otpViewModel = new OtpViewModel(this);
        mBinding.setOtpViewmodel(otpViewModel);
    }
}
