package com.cryptassist.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View

import com.cryptassist.R
import com.cryptassist.databinding.ActivityTouchIdBinding
import com.cryptassist.dragopenutils.DragListener
import com.cryptassist.dragopenutils.DragToClose
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.TouchedViewModel


class TouchIdActivity : BaseActivity() {
    private val mBinding: ActivityTouchIdBinding? = null
    private val touchedViewModel: TouchedViewModel? = null
    private var dragToClose: DragToClose?=null
    private lateinit var prefManager: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_touch_id)
        dragToClose = findViewById(R.id.drag_to_close)
        prefManager = PrefManager.getInstance(this)
        //        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_touch_id);
        //        touchedViewModel=new TouchedViewModel(this,mBinding.dragToClose);
        //        mBinding.setCloseDrag(touchedViewModel);
        dragToClose?.setDragListener(object : DragListener {
            override fun onStartDraggingView() {
                Log.d("TAG", "onStartDraggingView()")
            }

            override fun onViewCosed() {
                Log.d("TAG", "onViewCosed()")
            }
        })
    }

    fun clickNotNow(view: View) {
        prefManager.savePreference(AppConstants.TOUCHID_NOT_SELECTED, true)
        dragToClose?.closeDraggableContainer()
    }

    fun clickTouchId(view: View) {
        prefManager.savePreference(AppConstants.TOUCHID_NOT_SELECTED, true)
        prefManager.savePreference(AppConstants.IS_FINGER_ENABLED, true)
        dragToClose?.closeDraggableContainer()
    }

}
