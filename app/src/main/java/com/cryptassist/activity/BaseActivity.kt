package com.cryptassist.activity

import android.app.Activity
import android.app.ActivityManager
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.amirarcane.lockscreen.activity.EnterPinActivity
import com.cryptassist.CryptassistApplication
import com.cryptassist.R
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.LanguageViewModel


abstract class BaseActivity : AppCompatActivity() {

    //    @Inject
    lateinit var prefs: SharedPreferences
    lateinit var languageViewModel: LanguageViewModel

    companion object {
        protected val TAG = BaseActivity::class.java.name
        private var myViewHolder: MyViewHolder? = null

        var isAppWentToBg = false

        var isWindowFocused = false

        var isMenuOpened = false

        var isBackPressed = false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        languageViewModel = ViewModelProviders.of(this).get(LanguageViewModel::class.java)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        myViewHolder = MyViewHolder(this, false)

        if (checkFloatWindowPermission()) {//已授权
            MyViewHolder.showEasyTouchView()
        } else {
            showPromptingDialog()
        }


    }


    private fun checkFloatWindowPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(this)) {//已拥有权限
                return true
            }
        } else {//23以下版本只要安装就已被授权，并且无法撤销
            return true
        }
        return false
    }

    override fun onStart() {
        Log.d(TAG, "onStart isAppWentToBg $isAppWentToBg")

        applicationWillEnterForeground()

        super.onStart()
    }

    private fun applicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false

            MyViewHolder.showEasyTouchView()

// start the activity, It handles the setting and checking
//            val intent = Intent(/*BaseActivity.*/this, EnterPinActivity::class.java)
//            startActivityForResult(intent, AppConstants.REQUEST_CODE_PASS_CODE)

            /*if (prefs.getBoolean(SharedPrefModule.IS_PASS_CODE_SET, false)) {
                // set pin instead of checking it
                val intent = EnterPinActivity.getIntent(this, true)
                startActivity(intent)
            } else {
                // start the activity, It handles the setting and checking
                val intent = Intent(this, EnterPinActivity::class.java)
                //startActivity(intent);

                // for handling back press
                startActivityForResult(intent, AppConstants.REQUEST_CODE_PASS_CODE)
            }*/

        }
    }

    override fun onResume() {
        super.onResume()
        if (checkFloatWindowPermission()) {//已授权
            MyViewHolder.showEasyTouchView()
        }


    }


    override fun onStop() {
        super.onStop()

        Log.d(TAG, "onStop ")
        applicationdidenterbackground()
    }

    fun applicationdidenterbackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true
            MyViewHolder.hideEasyTouchView()
            /*Toast.makeText(getApplicationContext(),
                    "App is Going to Background", Toast.LENGTH_SHORT).show();*/
        }
    }

    private fun showPromptingDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle(resources.getString(R.string.alert))
        alertDialog.setMessage(resources.getString(R.string.allow_content_window))
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(resources.getString(R.string.ok)) { _, _ ->
            val intent = Intent(android.provider.Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
            startActivityForResult(intent, 100)
        }
        alertDialog.setNegativeButton(resources.getString(R.string.cancel)) { _, _ -> }
        alertDialog.show()
    }

    override fun onBackPressed() {

        if (this is DashboardActivity) {

        } else {
            isBackPressed = true

        }

        Log.d(TAG,
                "onBackPressed " + isBackPressed + ""
                        + this.localClassName)
        super.onBackPressed()
    }


    override fun onWindowFocusChanged(hasFocus: Boolean) {

        isWindowFocused = hasFocus

        if (isBackPressed && !hasFocus) {
            isBackPressed = false
            isWindowFocused = true
        }

        super.onWindowFocusChanged(hasFocus)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            AppConstants.REQUEST_CODE_PASS_CODE -> if (resultCode == EnterPinActivity.RESULT_BACK_PRESSED) {
//                Toast.makeText(this, "back pressed", Toast.LENGTH_LONG).show()
            } else if (resultCode == Activity.RESULT_OK) {
//                Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show()
                if ((getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).getRunningTasks(1)[0].topActivity.className != "EnterPinActivity") isAppWentToBg = false
            }
        }
    }


}
