package com.cryptassist.activity

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater

import com.cryptassist.R
import com.cryptassist.databinding.ActivityBuyCoinBinding
import com.cryptassist.databinding.BuyStepsDialogBinding
import com.cryptassist.databinding.SuggestDialogBinding
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.BuyCoinViewModel
import com.cryptassist.viewModel.BuyStepViewModel
import com.cryptassist.viewModel.SuggestCoinViewModel

class BuyCoinActivity : BaseActivity() {
  lateinit var buyCoinViewModel:BuyCoinViewModel
    private lateinit var dialogUtils: Dialog
    private lateinit var buyStepsDialogBinding: BuyStepsDialogBinding;
    lateinit var mBinding: ActivityBuyCoinBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_buy_coin)
        buyCoinViewModel = BuyCoinViewModel(this)
        mBinding.viewModel = buyCoinViewModel
        val layoutInflater = LayoutInflater.from(this)
        buyStepsDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.buy_steps_dialog, null, false)
        dialogUtils = DialogUtils.createCustomDialog(this, buyStepsDialogBinding.root)
        val buyStepViewModel = BuyStepViewModel(this, dialogUtils)
        buyStepsDialogBinding.viewModel = buyStepViewModel
        dialogUtils.show()

    }


}
