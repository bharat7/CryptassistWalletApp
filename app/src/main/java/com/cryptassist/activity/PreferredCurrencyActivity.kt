package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager

import com.cryptassist.R
import com.cryptassist.adapter.PreferredCurrencyAdapter
import com.cryptassist.databinding.ActivityPreferedLanguageBinding
import com.cryptassist.viewModel.PreferedCurrencyViewModel


import java.util.ArrayList


class PreferredCurrencyActivity : BaseActivity() {
    private var preferredLanguageBinding: ActivityPreferedLanguageBinding? = null
    private var preferredCurrencyViewModel: PreferedCurrencyViewModel? = null
    private var preferredLanguageViewModels = ArrayList<PreferedCurrencyViewModel>()
    private var preferredLanguageAdapter: PreferredCurrencyAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferredLanguageBinding = DataBindingUtil.setContentView(this@PreferredCurrencyActivity, R.layout.activity_prefered_language)
        preferredCurrencyViewModel = PreferedCurrencyViewModel(this@PreferredCurrencyActivity)
        preferredLanguageBinding!!.rvList.layoutManager = LinearLayoutManager(this@PreferredCurrencyActivity)
        preferredLanguageViewModels = preferredCurrencyViewModel!!.preferedLanguageModels
        preferredLanguageAdapter = PreferredCurrencyAdapter(this@PreferredCurrencyActivity, preferredLanguageViewModels)
        preferredLanguageBinding!!.rvList.adapter = preferredLanguageAdapter
    }


}
