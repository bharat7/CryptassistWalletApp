package com.cryptassist.activity

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import com.ToxicBakery.viewpager.transforms.RotateDownTransformer
import com.cryptassist.R
import com.cryptassist.adapter.SlidingImageAdapter
import com.cryptassist.databinding.ActivityTutorialScreenBinding
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.TutorialScreenViewModel
import com.viewpagerindicator.CirclePageIndicator

import java.util.ArrayList
import java.util.Timer
import java.util.TimerTask

class TutorialScreenActivity : AppCompatActivity() {
    private var tutorialScreenViewModel: TutorialScreenViewModel? = null
    private val ImagesArray = ArrayList<Int>()
    private var mBinding: ActivityTutorialScreenBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_tutorial_screen)
        tutorialScreenViewModel = TutorialScreenViewModel(this@TutorialScreenActivity)
        mBinding!!.onClickSkip = tutorialScreenViewModel
        init()
    }

    private fun init() {
        for (i in IMAGES.indices)
            ImagesArray.add(IMAGES[i])

        mBinding!!.pager.adapter = SlidingImageAdapter(this, ImagesArray)

        mBinding!!.indicator.setViewPager(mBinding!!.pager)


        //Set circle indicator radius


        NUM_PAGES = IMAGES.size

        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            try {
                mBinding!!.pager.setCurrentItem(currentPage++, true)
            } catch (e: Exception) {
            }
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        mBinding!!.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })
        mBinding!!.pager.setPageTransformer(true, RotateDownTransformer())
    }

    companion object {
        private var currentPage = 0
        private var NUM_PAGES = 0
        private val IMAGES = arrayOf(R.drawable.send_screen, R.drawable.transaction_screen, R.drawable.scan_screen, R.drawable.recive_screen)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            AppConstants.REQUEST_CODE_PASS_CODE -> if (resultCode == Activity.RESULT_OK) {
//                Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show()
                val i = Intent(this, DashboardActivity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()
            }
        }
    }


}
