package com.cryptassist.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import com.amirarcane.lockscreen.activity.EnterPinActivity
import com.crashlytics.android.Crashlytics

import com.cryptassist.R
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import io.fabric.sdk.android.Fabric
import java.util.*

class SplashActivity : AppCompatActivity() {
    private var backPressed = false
    private lateinit var prefManager: PrefManager
    private var context: Context? = null
    private var myLocale: Locale? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        context = this@SplashActivity
        try {
//            Fabric.with(this, Crashlytics())
        } catch (e: Exception) {
        }
        prefManager = PrefManager.getInstance(context as SplashActivity)
        if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE,""))) {
            CommonUtils.changeLang(this, prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE,""))
        }
        else{
            CommonUtils.changeLang(this,"en")
        }
        val handler = Handler()
        handler.postDelayed({
            if ((!backPressed)) {
//              finish()
                var i: Intent
               if (prefManager.getPreference(AppConstants.IS_APP_OLDER, false)) {

                   if (prefManager.getPreference(AppConstants.IS_WALLET_CREATED, false)) {
                       i = Intent(context, /*EnterPinActivity*/DashboardActivity::class.java)
                       startActivityForResult(i, AppConstants.REQUEST_CODE_PASS_CODE)
                   }
                   else {
                       i = Intent(context, MainActivity::class.java)
                       i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                       startActivity(i)
                       finish()
                   }

                }

                else {
                    i = Intent(context, TutorialScreenActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(i)
                    finish()
                }
            }
        }, 3000)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        backPressed = true
        backPressed()
    }

    fun backPressed() {
        System.exit(0)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            AppConstants.REQUEST_CODE_PASS_CODE -> if (resultCode == Activity.RESULT_OK) {
//                Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show()
                val i = Intent(context, DashboardActivity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()
            }
        }
    }


}
