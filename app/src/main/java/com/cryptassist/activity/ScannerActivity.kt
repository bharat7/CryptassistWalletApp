package com.cryptassist.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.support.v4.app.ActivityCompat
import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager

import com.cryptassist.R
import com.cryptassist.databinding.ActivityScannerBinding
import com.cryptassist.fragment.ScanFragment
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.DialogUtils
import com.cryptassist.viewModel.ScannerDialogViewModel
import com.cryptassist.viewModel.SendFragmentViewModel
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScannerActivity : BaseActivity() {
    private var activityScannerBinding: ActivityScannerBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityScannerBinding = DataBindingUtil.setContentView(this, R.layout.activity_scanner)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1001 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    activityScannerBinding = DataBindingUtil.setContentView(this, R.layout.activity_scanner)
                    return
                }

            }
        }
    }

}
