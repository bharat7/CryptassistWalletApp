package com.cryptassist.activity

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.view.MotionEvent

import com.cryptassist.R
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.databinding.ActivityAddContactBinding
import com.cryptassist.imageutils.CropImage
import com.cryptassist.imageutils.TakePictureUtilsOld
import com.cryptassist.utils.CheckPermission
import com.cryptassist.sectionheadercomponents.PinnedHeaderListView
import com.cryptassist.viewModel.AddContactViewModel


import com.cryptassist.imageutils.TakePictureUtilsOld.startCropImage
import com.cryptassist.utils.AppConstants
import java.io.*

class AddContactActivity : BaseActivity() {
    private var mContext: AddContactActivity? = null
    val TAKE_PICTURE = 1
    private var activityAddContactBinding: ActivityAddContactBinding? = null
    private var imageName: String? = null
    private var addContactViewModel: AddContactViewModel? = null
    private val myViewHolder: MyViewHolder? = null
    internal var pinnedHeaderListView: PinnedHeaderListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*  if (myViewHolder == null) {
            myViewHolder = new MyViewHolder(getApplicationContext(),this);
        }*/
        mContext = this@AddContactActivity
        activityAddContactBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_contact)
        addContactViewModel = AddContactViewModel(this, activityAddContactBinding!!)
        activityAddContactBinding!!.onClickAddress = addContactViewModel
        activityAddContactBinding!!.ivCamera.setOnClickListener { marshMellow() }





        activityAddContactBinding!!.etAddress.setOnTouchListener { _, event ->


            val DRAWABLE_RIGHT = 2

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= activityAddContactBinding!!.etAddress.right - activityAddContactBinding!!.etAddress.compoundDrawables[DRAWABLE_RIGHT].bounds.width()) {
                    // your action here
                    val intent = Intent(mContext, ScannerActivity::class.java)
                    mContext!!.startActivityForResult(intent, AppConstants.PERMISSION_REQUEST_SCANNER)
                    true
                }
            }
            false
        }
    }

    protected fun addPhotoDialog() {
        val items = arrayOf<CharSequence>(mContext!!.getString(R.string.take_photo), mContext!!.getString(R.string.choose_from_lib), mContext!!.getString(R.string.cancel))

        val builder = AlertDialog.Builder(mContext!!)
        builder.setTitle(mContext!!.getString(R.string.add_photo))
        builder.setItems(items) { dialog, item ->
            if (items[item] == mContext!!.getString(R.string.take_photo)) {
                try {
                    //                                        dialog.dismiss();
                    imageName = System.currentTimeMillis().toString()
                    takePicture(imageName)
                    //                                        TakePictureUtilsOld.takePicture((Activity) mContext, imageName);
                } catch (e: Exception) {
                    e.printStackTrace()

                }

            } else if (items[item] == mContext!!.getString(R.string.choose_from_lib)) {
                imageName = "delivery_picture"
                dialog.dismiss()
                imageName = "picture_" + System.currentTimeMillis()
                TakePictureUtilsOld.openGallery((mContext as Activity?)!!)

            } else {
                dialog.dismiss()
            }
        }
        builder.create().show()
        builder.setCancelable(true)
    }

    private fun marshMellow() {

        if (CheckPermission.checkCameraPermission((mContext as Activity?)!!)) {
            addPhotoDialog()
        } else {
            CheckPermission.requestCameraPermission((mContext as Activity?)!!)
        }
    }

    @Throws(IOException::class)
    fun takePicture(fileName: String?) {
        val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            val mImageCaptureUri = Uri.fromFile(File(mContext!!.getExternalFilesDir("temp"), fileName!! + ".jpg"))
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri)
            intent.putExtra("return-data", true)
            startActivityForResult(intent, TAKE_PICTURE)

        } catch (ex: Exception) {
            ex.printStackTrace()

        }

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AppConstants.PERMISSION_REQUEST_SCANNER) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    addContactViewModel?.mAddress?.set(data?.getStringExtra(AppConstants.CTA_ADDRESS))
                } catch (e: Exception) {

                }

            }
        } else if (requestCode == TakePictureUtilsOld.PICK_GALLERY) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    val inputStream = mContext!!.contentResolver.openInputStream(data!!.data!!)
                    val fileOutputStream = FileOutputStream(File(mContext!!.getExternalFilesDir("temp"), imageName!! + ".jpg"))
                    TakePictureUtilsOld.copyStream(inputStream!!, fileOutputStream)
                    fileOutputStream.close()
                    inputStream.close()
                    startCropImage(mContext as Activity, imageName!! + ".jpg")
                } catch (e: Exception) {

                }
            }
        } else if (requestCode == TAKE_PICTURE) {
            // imageName="picture";
            if (resultCode == Activity.RESULT_OK) {
                startCropImage(mContext as Activity, imageName!! + ".jpg")
            }


        } else if (requestCode == TakePictureUtilsOld.CROP_FROM_CAMERA) {
            //  imageName="picture";
            if (resultCode == Activity.RESULT_OK) {

                var path: String? = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)

                }
                if (path == null) {
                    return
                }
                val bm = BitmapFactory.decodeFile(path)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                activityAddContactBinding!!.ivProfileImage.setImageBitmap(bm)
//                addContactViewModel!!.userImage=CommonUtils.BitMapToString(bm)
                addContactViewModel!!.userImage.set(path)


            }
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        addPhotoDialog()
    }
}
