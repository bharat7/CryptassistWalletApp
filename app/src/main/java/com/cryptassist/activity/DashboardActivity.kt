package com.cryptassist.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.provider.Settings
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.Toast
import com.amirarcane.lockscreen.activity.EnterPinActivity
import com.crashlytics.android.Crashlytics

import com.cryptassist.commandintefaces.DrawerOptionSelected
import com.cryptassist.R
import com.cryptassist.adapter.DrawerItemAdapter
import com.cryptassist.assistivewidget.MyViewHolder
import com.cryptassist.databinding.ActivityDashboardBinding
import com.cryptassist.fragment.*
import com.cryptassist.menu.DrawerAdapter
import com.cryptassist.model.AddContactModel
import com.cryptassist.model.ContactModel
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CheckPermission
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.viewModel.AddressViewModel
import com.cryptassist.viewModel.DrawerViewModel
import com.cryptassist.webservices.APIExecutor

import java.util.ArrayList
import com.google.gson.JsonObject
import io.fabric.sdk.android.Fabric
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DashboardActivity : BaseActivity(), DrawerAdapter.OnItemSelectedListener, DrawerOptionSelected {
    lateinit var viewModel: AddressViewModel
    private var ICONS: IntArray? = null
    private lateinit var prefManager: PrefManager
    private var doubleBackToExitPressedOnce: Boolean = false
    private var myViewHolder: MyViewHolder? = null
    private val drawerViewModel = DrawerViewModel()
    private val drawerViewModels = ArrayList<DrawerViewModel>()
    private var drawerItemAdapter: DrawerItemAdapter? = null
    var activity: DashboardActivity? = null
    private val apiClient by lazy {
        APIExecutor.clientApi()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)
        prefManager = PrefManager.getInstance(this)
        mBinding = DataBindingUtil.setContentView(this@DashboardActivity, R.layout.activity_dashboard)
//        if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE,""))&&prefManager.getPreference((AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE),"").equals("he")){
//            mBinding.navViewRight.foregroundGravity=Gravity.END
//            mBinding.navViewRight
//        }
        Fabric.with(this, Crashlytics())

        overridePendingTransition(R.anim.slide_in_up, 0)
//        val handler = Handler()
        drawerLayout = mBinding.drawerLayout
        viewModel.allContacts.observe(
                this, Observer { allContacts ->
            allContacts?.let {
                addContactModels.addAll(it)
            }
        })
        if (intent != null && intent.getStringExtra("openSettingFragment") != null) {
            MyViewHolder.hideEasyTouchView()
            fragment = SettingsFragment()
            mBinding.llInner?.container?.visibility = View.VISIBLE
            CommonUtils.setFragment(fragment as SettingsFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        }


        val jsonObject = JsonObject()
        jsonObject.addProperty("email", CommonUtils.encrypt("authUser@cryptassist.io"))
        jsonObject.addProperty("password", CommonUtils.encrypt("fucked!@#+_*&fuck)"))

//        viewModel.createTokenService(jsonObject)

        myViewHolder = MyViewHolder(this, true)
        if (checkFloatWindowPermission()) {
            MyViewHolder.showEasyTouchView()
        }



        mBinding.rvList.layoutManager = LinearLayoutManager(this@DashboardActivity)
        drawerViewModels.addAll(drawerViewModel.getPreferredLanguageModels(this@DashboardActivity))
        drawerItemAdapter = DrawerItemAdapter(this@DashboardActivity, drawerViewModels, this)
        mBinding.rvList.adapter = drawerItemAdapter

        mBinding.llInner?.vpMain?.let { setupViewPager(it) }
        setNavigationBar()
        setBottomBarSelected()
        if (CheckPermission.checkAllPermission(this)) {
            addContactList()
        } else {
            CheckPermission.requestAllPermission(this)
        }
        mBinding.llInner?.ivNavigation?.setOnClickListener {
            mBinding.drawerLayout.openDrawer(GravityCompat.START)
            runLayoutAnimation(mBinding.rvList)
        }
        mBinding.llList.setOnClickListener {
            CommonUtils.hideKeyboard(this@DashboardActivity)
            mBinding.drawerLayout.closeDrawer(GravityCompat.START)
        }
        if (!prefManager.getPreference(AppConstants.TOUCHID_NOT_SELECTED, false)) {
            val i = Intent(this, TouchIdActivity::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.slide_in_up, 0)
        }

        drawerItems()
        languageViewModel.users?.observe(
                this, Observer { users ->
            //            users.let {
            if (users?.isNotEmpty()!!) {
            }

        })


        if (AppConstants.AUTH_TOKEN == null) {
            val mJsonObject = JsonObject()
            mJsonObject.addProperty("email", CommonUtils.encrypt(AppConstants.AUTH_EMAIL))
            mJsonObject.addProperty("password", CommonUtils.encrypt(AppConstants.AUTH_PASSWORD))

            apiClient.createTokenService(mJsonObject)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result -> if (result.responseCode == AppConstants.SUCCESS_CODE) AppConstants.AUTH_TOKEN = result.token },
                            { error -> ToastUtils.showToastShort(this, error.message!!) }

                    )
        }
    }

    private fun drawerItems() {
        setNavigationBar()
        setBottomBarSelected()
        drawerViewModels.clear()
        mBinding.rvList.layoutManager = LinearLayoutManager(this@DashboardActivity)
        drawerViewModels.addAll(drawerViewModel.getPreferredLanguageModels(this@DashboardActivity))
        drawerItemAdapter = DrawerItemAdapter(this@DashboardActivity, drawerViewModels, this)
        mBinding.rvList.adapter = drawerItemAdapter
    }


    private fun checkFloatWindowPermission(): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(this)) {//已拥有权限
                    return true
                }
            } else {//23以下版本只要安装就已被授权，并且无法撤销
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }


    override fun onResume() {
        super.onResume()
        myViewHolder = MyViewHolder(this, false)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CheckPermission.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            addContactList()
        }
    }

    private fun addContactList() {
        contactModels.clear()
        try {
            val cr = contentResolver
            val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)

            if (cur?.count ?: 0 > 0) {
                while (cur!= null && cur.moveToNext()) {
                    val contactModel = ContactModel()
                    val id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID))
                    contactModel.name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME))
                    contactModel.firstLetterName = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME))
                    val cur1 = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            arrayOf(id), null)

                    if ( cur1!!.moveToNext()) {
                        val email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                        contactModel.emailAddress = email
                        cur1.close()
                    }


                    if (cur.getInt(cur.getColumnIndex(
                                    ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        val pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                arrayOf(id), null)
                        while (pCur!!.moveToNext()) {
                            contactModel.phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER))


                        }
                        contactModels.add(contactModel)
                        pCur.close()

                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun setBottomBarSelected() {
        mBinding.llInner?.bottomBar?.setOnTabReselectListener { tabId -> bottomBarPressed(tabId) }

        mBinding.llInner?.bottomBar?.setOnTabSelectListener { tabId -> bottomBarPressed(tabId) }
    }

    fun bottomBarPressed(tabId: Int) {
        if (tabId == R.id.tab_send) {
            mBinding.llInner?.vpMain?.currentItem = 0
        } else if (tabId == R.id.tab_transaction) {
            mBinding.llInner?.vpMain?.currentItem = 1
        } else if (tabId == R.id.tab_scan) {
            fragment=ScanFragment()
            mBinding.llInner?.vpMain?.currentItem = 2
        } else if (tabId == R.id.tab_receive) {

            mBinding.llInner?.vpMain?.currentItem = 3

        }
    }

    private fun setNavigationBar() {
        mBinding.llInner?.vpMain?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                mBinding.llInner!!.bottomBar.verticalScrollbarPosition = position
                mBinding.llInner!!.bottomBar.selectTabAtPosition(position, false)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        mBinding.llInner?.tlMain?.setupWithViewPager(mBinding.llInner!!.vpMain)
        ICONS = intArrayOf(R.mipmap.send, R.mipmap.transaction, R.mipmap.send, R.mipmap.recive)

        mBinding.llInner?.tlMain?.getTabAt(0)!!.setIcon(ICONS!![0] )
        mBinding.llInner?.tlMain?.getTabAt(1)!!.setIcon(ICONS!![1])
        mBinding.llInner?.tlMain?.getTabAt(2)!!.setIcon(ICONS!![2])
        mBinding.llInner?.tlMain?.getTabAt(3)!!.setIcon(ICONS!![3])

        mBinding.llInner?.vpMain?.offscreenPageLimit = 4

        mBinding.llInner!!.tlMain.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                //                fragmentContainer.setVisibility(View.GONE);

                val params = mBinding.llInner!!.vpMain.layoutParams
                val layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layoutParams.setMargins(0, 0, 0, 0)
                mBinding.llInner!!.vpMain.layoutParams = params

                if (tab.position == 0) {

                } else {

                }

                if (tab.position == 1) {

                } else {

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    private fun setupViewPager(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(supportFragmentManager)
        //        adapter.addFragment(new HomeFragment(), getResources().getString(R.string.bottom_menu_home));
        adapter.addFragment(SendFragment(), resources.getString(R.string.send))
        adapter.addFragment(TransactionFragment(), resources.getString(R.string.transaction))
        adapter.addFragment(ScanFragment(), resources.getString(R.string.scan))
        adapter.addFragment(ReceiveFragment(), resources.getString(R.string.recieve))
        viewPager.adapter = adapter

    }


    override fun onItemSelected(position: Int) {


    }

    private fun runLayoutAnimation(recyclerView: RecyclerView) {
        val context = recyclerView.context
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)

        recyclerView.layoutAnimation = controller
        recyclerView.adapter?.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }


    override fun onOptionSelected(position: Int) {
        MyViewHolder.showEasyTouchView()
        mBinding.drawerLayout.closeDrawer(GravityCompat.START)
        mBinding.llInner?.container?.visibility = View.VISIBLE
        if (position == POS_Profile) {
            mBinding.llInner?.container?.visibility = View.GONE
            mBinding.llInner?.vpMain?.currentItem = 0
            mBinding.llInner?.bottomBar?.visibility = View.VISIBLE

        } else if (position == POS_INVITE) {
            //            fragment=new InvitedFragment();
            fragment = InviteContactFragment()
            CommonUtils.setFragment(fragment as InviteContactFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else if (position == POS_BACKUP_WALLET) {
            fragment = BackupFragment()
            CommonUtils.setFragment(fragment as BackupFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else if (position == POS_HELP_SECURITY) {
            fragment = HelpSupportFragment()
            CommonUtils.setFragment(fragment as HelpSupportFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else if (position == POS_ABOUT_US) {
            fragment = AboutUsFragment()
            CommonUtils.setFragment(fragment as AboutUsFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else if (position == POS_CONTACT) {
            fragment = AddAddressFragment()
            CommonUtils.setFragment(fragment as AddAddressFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else if (position == POS_SETTINGS) {
            MyViewHolder.hideEasyTouchView()
            fragment = SettingsFragment()
            CommonUtils.setFragment(fragment as SettingsFragment, true, this, R.id.container)
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        } else {
            mBinding.llInner?.bottomBar?.visibility = View.GONE
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<android.support.v4.app.Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): android.support.v4.app.Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: android.support.v4.app.Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onStart() {
        super.onStart()
//        val intent = Intent(/*BaseActivity.*/this, EnterPinActivity::class.java)
//        startActivityForResult(intent, AppConstants.REQUEST_CODE_PASS_CODE)
    }

    override fun onBackPressed() {

        try {
            if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                mBinding.drawerLayout.closeDrawer(GravityCompat.START)
            } else {

                supportFragmentManager.backStackEntryCount
                //Log.e("cnt---",""+cnt);

                if (supportFragmentManager.backStackEntryCount > 0) {

                    supportFragmentManager.popBackStack()
                    mBinding.llInner?.bottomBar?.visibility = View.VISIBLE


                } else if (fragment != null && fragment !is ScanFragment) {
                    MyViewHolder.showEasyTouchView()
                    mBinding.llInner?.container?.visibility = View.GONE
                    mBinding.llInner?.vpMain?.currentItem = 0
                    mBinding.llInner?.bottomBar?.visibility = View.VISIBLE
                    fragment = ScanFragment()
                } else {
                    if (mBinding.llInner?.bottomBar?.currentTabPosition == 0) {
                        //Log.e("exit--", "-");
                        if (doubleBackToExitPressedOnce) {
                            System.exit(0)
                        }

                        this.doubleBackToExitPressedOnce = true
                        Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show()

                        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

                    } else {
                        mBinding.llInner?.vpMain?.currentItem = 0
                    }
                }

                if (supportFragmentManager.backStackEntryCount == 1)
                    mBinding.llInner?.bottomBar?.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {
        lateinit var drawerLayout: DrawerLayout
        private val POS_Profile = 0
        private val POS_CONTACT = 1
        private val POS_BACKUP_WALLET = 2
        private val POS_HELP_SECURITY = 3
        private val POS_INVITE = 4
        private val POS_SETTINGS = 5
        private val POS_ABOUT_US = 6
        lateinit var mBinding: ActivityDashboardBinding

        var fragment: Fragment? = null
        var contactModels = ArrayList<ContactModel>()
        var addContactModels = ArrayList<AddContactModel>()
    }

   /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragment = supportFragmentManager.findFragmentById(SendFragment)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }*/

}
