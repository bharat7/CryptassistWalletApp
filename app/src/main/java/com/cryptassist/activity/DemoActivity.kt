package com.cryptassist.activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.Toast

import com.cryptassist.R
import com.cryptassist.assistivewidget.EasyTouchView
import com.cryptassist.assistivewidget.MyViewHolder

class DemoActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {
    private var mSwitch: Switch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
        mSwitch = findViewById(R.id.open_easytouch)
        mSwitch!!.setOnCheckedChangeListener(this)
        /* if (myViewHolder == null) {
            myViewHolder = new MyViewHolder(this);
        }*/
    }

    override fun onResume() {
        super.onResume()
        if (checkFloatWindowPermission()) {//已授权
            MyViewHolder.showEasyTouchView()
        }
        if (!EasyTouchView.isAlive) {
            Log.d(TAG, "setCheck(false) onResume")
            mSwitch!!.isChecked = false
        } else {
            Log.d(TAG, "setCheck(true) onResume")
            mSwitch!!.isChecked = true
        }
    }

    private fun checkFloatWindowPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(this)) {//已拥有权限
                return true
            }
        } else {//23以下版本只要安装就已被授权，并且无法撤销
            return true
        }
        return false
    }

    private fun showPromptingDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle(resources.getString(R.string.hint))
        alertDialog.setMessage(resources.getString(R.string.hint_content_window))
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(resources.getString(R.string.ok)) { _, _ ->
            val intent = Intent(android.provider.Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
            startActivityForResult(intent, REQUESTCODE)
            Toast.makeText(applicationContext, R.string.toast, Toast.LENGTH_LONG).show()
        }
        alertDialog.setNegativeButton(resources.getString(R.string.cancel)) { _, _ ->
            Log.d(TAG, "showPromptingDialog")
            mSwitch!!.isChecked = false
        }
        alertDialog.show()
    }

    override fun onCheckedChanged(compoundButton: CompoundButton, b: Boolean) {
        Log.d(TAG, "onCheckedChanged")
        if (b) {
            if (!checkFloatWindowPermission()) {//未授权
                showPromptingDialog()
            } else {
                MyViewHolder.showEasyTouchView()
            }
        } else {
            MyViewHolder.hideEasyTouchView()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUESTCODE) {
            if (!EasyTouchView.isAlive) {
                mSwitch!!.isChecked = false
            }
        }
    }

    companion object {
        private val TAG = "MainActivity"
        private val REQUESTCODE = 101
    }
}
