package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.cryptassist.R
import com.cryptassist.adapter.ValidatePassphraseAdapter
import com.cryptassist.databinding.ActivityValidatePassPhraseBinding
import com.cryptassist.utils.AppConstants
import com.cryptassist.viewModel.ValidatePassphraseViewModel
import java.util.*

class ValidatePassPhraseActivity : BaseActivity() {
    private var activityPassPhraseBinding: ActivityValidatePassPhraseBinding? = null
    private var validatePassphraseViewModel: ValidatePassphraseViewModel? = null
    private val validatePassphraseViewModels = ArrayList<ValidatePassphraseViewModel>()
    private val backupPhraseList = ArrayList<String>()
    private var preferredLanguageAdapter: ValidatePassphraseAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityPassPhraseBinding = DataBindingUtil.setContentView(this, R.layout.activity_validate_pass_phrase)
        try {
            if (intent != null && intent.getStringArrayListExtra(AppConstants.VALIDATE_PHRASE_LIST) != null) {
                backupPhraseList.clear()
                backupPhraseList.addAll(intent.getStringArrayListExtra(AppConstants.VALIDATE_PHRASE_LIST))
            }
            validatePassphraseViewModel = ValidatePassphraseViewModel(this@ValidatePassPhraseActivity)
            validatePassphraseViewModel?.viaLocales?.clear()
            validatePassphraseViewModel?.viaLocales?.addAll(backupPhraseList.shuffled())


            activityPassPhraseBinding?.rvList?.layoutManager = GridLayoutManager(this@ValidatePassPhraseActivity, 3)
            validatePassphraseViewModels.addAll(validatePassphraseViewModel!!.validatePassphraseViewModels)
            preferredLanguageAdapter = ValidatePassphraseAdapter(this@ValidatePassPhraseActivity, validatePassphraseViewModels, backupPhraseList)
            activityPassPhraseBinding?.rvList?.adapter = preferredLanguageAdapter


        } catch (e: Exception) {
        }
    }

}
