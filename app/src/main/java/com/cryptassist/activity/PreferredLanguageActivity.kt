package com.cryptassist.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils

import com.cryptassist.R
import com.cryptassist.adapter.PreferedLanguageAdapter
import com.cryptassist.commandintefaces.ChangeLocaleInterface
import com.cryptassist.databinding.ActivityPreferedLanguageBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.viewModel.LanguageViewModel
import com.cryptassist.viewModel.PreferredLanguageViewModel
import java.util.*


class PreferredLanguageActivity : BaseActivity(),ChangeLocaleInterface {


    private var preferredLanguageBinding: ActivityPreferedLanguageBinding? = null
    private var preferredLanguageViewModel: PreferredLanguageViewModel? = null
    private var preferredLanguageViewModels = ArrayList<PreferredLanguageViewModel>()
    private var preferredLanguageAdapter: PreferedLanguageAdapter? = null
    private lateinit var prefManager: PrefManager
    private var myLocale: Locale? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefManager = PrefManager.getInstance(this)

//        languageViewModel = ViewModelProviders.of(this).get(LanguageViewModel::class.java)

        preferredLanguageBinding = DataBindingUtil.setContentView(this@PreferredLanguageActivity, R.layout.activity_prefered_language)
        preferredLanguageViewModel = PreferredLanguageViewModel(this@PreferredLanguageActivity)
        preferredLanguageBinding!!.rvList.layoutManager = LinearLayoutManager(this@PreferredLanguageActivity)
        preferredLanguageViewModels = preferredLanguageViewModel!!.preferredLanguageModels
        preferredLanguageAdapter = PreferedLanguageAdapter(this@PreferredLanguageActivity, preferredLanguageViewModels,this, languageViewModel)
        preferredLanguageBinding!!.rvList.adapter = preferredLanguageAdapter
    }


    override fun onLanguageSelected(locale:String) {
        if (!TextUtils.isEmpty(locale)) {
            myLocale = Locale(locale)}
        else{
            myLocale = Locale("en")
        }

        fun onConfigurationChanged(newConfig: Configuration?) {
            super.onConfigurationChanged(newConfig)
            if (!TextUtils.isEmpty(prefManager.getPreference(AppConstants.SELECTED_PREFERRED_LANGUAGE_CODE,""))) {
                newConfig!!.locale = myLocale
                Locale.setDefault(myLocale)
                getResources().updateConfiguration(newConfig, getResources().getDisplayMetrics())
            }
        }
    }


}
