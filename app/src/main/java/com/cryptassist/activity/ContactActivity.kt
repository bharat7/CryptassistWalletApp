package com.cryptassist.activity

import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.TypedValue
import android.view.*
import android.widget.CheckBox
import android.widget.TextView
import com.cryptassist.R
import com.cryptassist.sectionheadercomponents.*
import com.cryptassist.utils.*


import java.util.*

class ContactActivity : BaseActivity() {
    private var mInflater: LayoutInflater? = null
    private var mListView: PinnedHeaderListView? = null
    private var mAdapter: ContactsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mInflater = LayoutInflater.from(this@ContactActivity)
        setContentView(R.layout.activity_contact)
        val contacts = getContacts()
        contacts!!.sortWith(Comparator { lhs, rhs ->
            val lhsFirstLetter = if (TextUtils.isEmpty(lhs.displayName)) ' ' else lhs.displayName!![0]
            val rhsFirstLetter = if (TextUtils.isEmpty(rhs.displayName)) ' ' else rhs.displayName!![0]
            val firstLetterComparison = Character.toUpperCase(lhsFirstLetter) - Character.toUpperCase(rhsFirstLetter)
            if (firstLetterComparison == 0) lhs.displayName!!.compareTo(rhs.displayName!!) else firstLetterComparison
        })
        mListView = findViewById<View>(android.R.id.list) as PinnedHeaderListView
        mAdapter = ContactsAdapter(contacts)

        val pinnedHeaderBackgroundColor = ContextCompat.getColor(this, getResIdFromAttribute(this, android.R.attr.colorBackground))
        mAdapter!!.setPinnedHeaderBackgroundColor(pinnedHeaderBackgroundColor)
        mAdapter!!.setPinnedHeaderTextColor(ContextCompat.getColor(this, R.color.pinned_header_text))
        mListView!!.setPinnedHeaderView(mInflater!!.inflate(R.layout.pinned_header_listview_side_header, mListView, false))
        mListView!!.adapter = mAdapter
        mListView!!.setOnScrollListener(mAdapter)
        mListView!!.setEnableHeaderTransparencyChanges(false)
        //    mAdapter.getFilter().filter(mQueryText,new FilterListener() ...
        //You can also perform operations on selected item by using :
        //    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() ...
    }


    fun getResIdFromAttribute(activity: Activity, attr: Int): Int {
        if (attr == 0)
            return 0
        val typedValue = TypedValue()
        activity.theme.resolveAttribute(attr, typedValue, true)
        return typedValue.resourceId
    }

    private fun getContacts(): ArrayList<Contact>? {
        if (checkContactsReadPermission()) {
            val uri = ContactsQuery.CONTENT_URI
            val cursor = managedQuery(uri, ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null, ContactsQuery.SORT_ORDER)
                    ?: return null
            val result = ArrayList<Contact>()
            while (cursor.moveToNext()) {
                val contact = Contact()
                contact.contactUri = ContactsContract.Contacts.getLookupUri(
                        cursor.getLong(ContactsQuery.ID),
                        cursor.getString(ContactsQuery.LOOKUP_KEY))
                contact.displayName = cursor.getString(ContactsQuery.DISPLAY_NAME)
                contact.photoId = cursor.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA)
                result.add(contact)
            }

            return result
        }
        val result = ArrayList<Contact>()
        val r = Random()
        val sb = StringBuilder()
        for (i in 0..999) {
            val contact = Contact()
            sb.delete(0, sb.length)
            val strLength = r.nextInt(10) + 1
            for (j in 0 until strLength)
                when (r.nextInt(3)) {
                    0 -> sb.append(('a'.toInt() + r.nextInt('z' - 'a')).toChar())
                    1 -> sb.append(('A'.toInt() + r.nextInt('Z' - 'A')).toChar())
                    2 -> sb.append(('0'.toInt() + r.nextInt('9' - '0')).toChar())
                }

            contact.displayName = sb.toString()
            result.add(contact)
        }
        return result
    }

    private fun checkContactsReadPermission(): Boolean {
        val permission = "android.permission.READ_CONTACTS"
        val res = checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    override fun onDestroy() {
        super.onDestroy()
        mAdapter!!.mAsyncTaskThreadPool.cancelAllTasks(true)
    }

    private class Contact {
        internal var contactId: Long = 0
        internal var contactUri: Uri? = null
        internal var displayName: String? = null
        internal var photoId: String? = null
    }

    /* override fun onCreateOptionsMenu(menu: Menu): Boolean {
         menuInflater.inflate(R.menu.activity_main, menu)
         val searchView = MenuItemCompat.getActionView(menu.findItem(R.id.menuItem_search)) as SearchView
         searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
             override fun onQueryTextSubmit(query: String): Boolean {
                 return false
             }

             override fun onQueryTextChange(newText: String): Boolean {
                 performSearch(newText)
                 return true
             }
         })

         return super.onCreateOptionsMenu(menu)
     }

     fun performSearch(queryText: String) {
         mAdapter!!.filter.filter(queryText)
         mAdapter!!.isHeaderViewVisible = TextUtils.isEmpty(queryText)
     }

     override fun onOptionsItemSelected(item: MenuItem): Boolean {
         var url: String? = null
         when (item.itemId) {
             R.id.menuItem_all_my_apps -> url = "https://play.google.com/store/apps/developer?id=AndroidDeveloperLB"
             R.id.menuItem_all_my_repositories -> url = "https://github.com/AndroidDeveloperLB"
             R.id.menuItem_current_repository_website -> url = "https://github.com/AndroidDeveloperLB/ListViewVariants"
         }
         if (url == null)
             return true
         val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
         intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
         startActivity(intent)
         return true
     }*/

    // ////////////////////////////////////////////////////////////
    // ContactsAdapter //
    // //////////////////
    private inner class ContactsAdapter(contacts: ArrayList<Contact>) : SearchablePinnedHeaderListViewAdapter<Contact>() {


        private var mContacts: ArrayList<Contact>? = null
        private val CONTACT_PHOTO_IMAGE_SIZE: Int
        private val PHOTO_TEXT_BACKGROUND_COLORS: IntArray
        val mAsyncTaskThreadPool = AsyncTaskThreadPool(1, 2, 10)

        override fun getSectionTitle(sectionIndex: Int): CharSequence {
            return (sections[sectionIndex] as StringArrayAlphabetIndexer.AlphaBetSection).name!!
        }

        init {
            setData(contacts)
            PHOTO_TEXT_BACKGROUND_COLORS = resources.getIntArray(R.array.contacts_text_background_colors)
            CONTACT_PHOTO_IMAGE_SIZE = resources.getDimensionPixelSize(
                    R.dimen.list_item__contact_imageview_size)
        }

        fun setData(contacts: ArrayList<Contact>) {
            this.mContacts = contacts
            val generatedContactNames = generateContactNames(contacts)
            setSectionIndexer(StringArrayAlphabetIndexer(generatedContactNames, true))
        }

        private fun generateContactNames(contacts: List<Contact>?): Array<String> {
            val contactNames = ArrayList<String>()
            if (contacts != null)
                for (contactEntity in contacts)
                    contactEntity.displayName?.let { contactNames.add(it) }
            return contactNames.toTypedArray()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val holder: ViewHolder
            val rootView: View
            if (convertView == null) {
                holder = ViewHolder()
                rootView = mInflater!!.inflate(R.layout.listview_item, parent, false)
                holder.friendProfileCircularContactView = rootView
                        .findViewById<View>(R.id.listview_item__friendPhotoImageView) as CircularContactView
                holder.friendProfileCircularContactView!!.textView?.setTextColor(-0x1)
                holder.friendName = rootView
                        .findViewById<View>(R.id.listview_item__friendNameTextView) as TextView
                holder.headerView = rootView.findViewById<View>(R.id.header_text) as TextView
                rootView.tag = holder
            } else {
                rootView = convertView
                holder = rootView.tag as ViewHolder
            }
            val contact = getItem(position)
            val displayName = contact!!.displayName
            holder.friendName!!.text = displayName
            val hasPhoto = !TextUtils.isEmpty(contact.photoId)
            if (holder.updateTask != null && !holder.updateTask!!.isCancelled())
                holder.updateTask!!.cancel(true)
            val cachedBitmap = if (hasPhoto) ImageCache.INSTANCE.getBitmapFromMemCache(contact.photoId!!) else null
            if (cachedBitmap != null)
                holder.friendProfileCircularContactView!!.setImageBitmap(cachedBitmap)
            else {
                val backgroundColorToUse = PHOTO_TEXT_BACKGROUND_COLORS[position % PHOTO_TEXT_BACKGROUND_COLORS.size]
                if (TextUtils.isEmpty(displayName))
                    holder.friendProfileCircularContactView!!.setImageResource(R.mipmap.contact_profile,
                            backgroundColorToUse)
                else {
                    val characterToShow = if (TextUtils.isEmpty(displayName)) "" else displayName!!.substring(0, 1).toUpperCase(Locale.getDefault())
                    holder.friendProfileCircularContactView!!.setTextAndBackgroundColor(characterToShow, backgroundColorToUse)
                }
                if (hasPhoto) {
                    holder.updateTask = object : AsyncTaskEx<Void, Void, Bitmap>() {

                        override fun doInBackground(vararg params: Void): Bitmap? {
                            if (isCancelled())
                                return null
                            val b = ContactImageUtil.loadContactPhotoThumbnail(this@ContactActivity, contact.photoId!!, CONTACT_PHOTO_IMAGE_SIZE)
                            return if (b != null) ThumbnailUtils.extractThumbnail(b, CONTACT_PHOTO_IMAGE_SIZE,
                                    CONTACT_PHOTO_IMAGE_SIZE) else null
                        }

                        override fun onPostExecute(result: Bitmap?) {
                            super.onPostExecute(result)
                            if (result == null)
                                return
                            ImageCache.INSTANCE.addBitmapToCache(contact.photoId, result)
                            holder.friendProfileCircularContactView!!.setImageBitmap(result)
                        }
                    }
                    mAsyncTaskThreadPool.executeAsyncTask(holder.updateTask)
                }
            }
            bindSectionHeader(holder.headerView, null, position)
            return rootView
        }

        /*override fun doFilter(item: Contact, constraint: CharSequence): Boolean {
            if (TextUtils.isEmpty(constraint))
                return true
            val displayName = item.displayName
            return !TextUtils.isEmpty(displayName) && displayName!!.toLowerCase(Locale.getDefault())
                    .contains(constraint.toString().toLowerCase(Locale.getDefault()))
        }

        override fun getOriginalList(): ArrayList<Contact>? {
            return mContacts
        }*/

        override val originalList = mContacts!!


        override fun doFilter(item: Contact, constraint: CharSequence?): Boolean {
            if (TextUtils.isEmpty(constraint))
                return true
            val displayName = item.displayName
            return !TextUtils.isEmpty(displayName) && displayName!!.toLowerCase(Locale.getDefault())
                    .contains(constraint.toString().toLowerCase(Locale.getDefault()))
        }


    }

    // /////////////////////////////////////////////////////////////////////////////////////
    // ViewHolder //
    // /////////////
    class ViewHolder {
        var friendProfileCircularContactView: CircularContactView? = null
        internal var friendName: TextView? = null
        internal var headerView: TextView? = null
        internal var tvEmail: TextView? = null
        internal var tvAddress: TextView? = null
        internal var cbSelect: CheckBox? = null
        var updateTask: AsyncTaskEx<Void, Void, Bitmap>? = null
    }
}
