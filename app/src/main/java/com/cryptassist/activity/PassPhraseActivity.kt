package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import com.cryptassist.R
import com.cryptassist.adapter.PassphraseAdapter
import com.cryptassist.bip39.MnemonicGenerator
import com.cryptassist.bip39.SeedCalculator
import com.cryptassist.bip39.Words
import com.cryptassist.bip39.wordlists.English
import com.cryptassist.databinding.ActivityPassPhraseBinding
import com.cryptassist.viewModel.PassphraseViewModel
import java.security.SecureRandom


class PassPhraseActivity : AppCompatActivity() {
    private var activityPassPhraseBinding: ActivityPassPhraseBinding? = null
    private var passphraseViewModel: PassphraseViewModel? = null
    private var preferredLanguageAdapter: PassphraseAdapter? = null
    private var passphraseViewModels = ArrayList<PassphraseViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityPassPhraseBinding = DataBindingUtil.setContentView(this, R.layout.activity_pass_phrase)
        passphraseViewModel = PassphraseViewModel(this@PassPhraseActivity)

        bip39()

        activityPassPhraseBinding?.onClick = passphraseViewModel
        activityPassPhraseBinding?.rvList?.layoutManager = GridLayoutManager(this@PassPhraseActivity, 3)
        passphraseViewModels.addAll(passphraseViewModel!!.preferredLanguageModels)
        preferredLanguageAdapter = PassphraseAdapter(this@PassPhraseActivity, passphraseViewModels)
        activityPassPhraseBinding?.rvList?.adapter = preferredLanguageAdapter

    }


    private fun bip39() {
        val sb = StringBuilder()
        val entropy = ByteArray(Words.TWELVE.byteLength())
        SecureRandom().nextBytes(entropy)

//        val entropyStr = Util.bytesToHexFun2(entropy)

        MnemonicGenerator(English.INSTANCE).createMnemonic(entropy, sb)

        val seed = SeedCalculator().calculateSeed(sb.toString(), "")

//        val seedStr = Util.bytesToHexFun2(seed)

        passphraseViewModel?.viaLocales?.clear()
        passphraseViewModel?.viaLocales?.addAll(sb.toString().split(" ".toRegex(), 0))

    }


}
