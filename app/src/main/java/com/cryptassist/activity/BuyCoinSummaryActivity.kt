package com.cryptassist.activity

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.cryptassist.R
import com.cryptassist.databinding.ActivityBuyCoinExchangeBinding
import com.cryptassist.databinding.ActivityBuyCoinSummaryBinding
import com.cryptassist.viewModel.BuyCoinExchangeViewModel
import com.cryptassist.viewModel.BuyCoinSummaryViewModel

class BuyCoinSummaryActivity : BaseActivity() {
    lateinit var mBinding: ActivityBuyCoinSummaryBinding
    lateinit var buyCoinSummaryViewModel: BuyCoinSummaryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(this,R.layout.activity_buy_coin_summary)
        buyCoinSummaryViewModel= BuyCoinSummaryViewModel(this);
        mBinding.viewModel=buyCoinSummaryViewModel
        buyCoinSummaryViewModel.ctaAddress.set(resources.getString(R.string.cta_address))
    }
}
