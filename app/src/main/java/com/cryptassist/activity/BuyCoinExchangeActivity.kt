package com.cryptassist.activity

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import com.cryptassist.R
import com.cryptassist.databinding.ActivityBuyCoinExchangeBinding
import com.cryptassist.databinding.DialogPopUpBinding
import com.cryptassist.preference.PrefManager
import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.CommonUtils
import com.cryptassist.utils.DialogUtils
import com.cryptassist.utils.ToastUtils
import com.cryptassist.viewModel.BuyCoinExchangeViewModel
import com.cryptassist.viewModel.PopUpDialogViewModel
import com.cryptassist.webservices.APIExecutor
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class BuyCoinExchangeActivity : BaseActivity() {
   lateinit var mBinding:ActivityBuyCoinExchangeBinding
    private var disposable: Disposable? = null
    lateinit var buyCoinExchangeViewModel:BuyCoinExchangeViewModel
    lateinit var prefManager:PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding=DataBindingUtil.setContentView(this,R.layout.activity_buy_coin_exchange)
        buyCoinExchangeViewModel=BuyCoinExchangeViewModel(this)
        mBinding.viewModel=buyCoinExchangeViewModel
        submitOrder()
    }


    private fun submitOrder(){
        CommonUtils.showProgressDialog(this)
        prefManager= PrefManager.getInstance(this)
        val jsonObject = JsonObject()
        jsonObject.addProperty("Coin", "")
        disposable = apiClient.coinDetailApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            CommonUtils.hideProgressDialog(this)
                            if (result.responseCode == 200) {

                            } else ToastUtils.showToastShort(this, CommonUtils.decrypt(result.responseMessage))
                        },
                        {

                            error -> ToastUtils.showToastShort(this, error.message!!)
                            CommonUtils.hideProgressDialog(this)},
                        { disposable?.dispose() }
                )

    }
    private val apiClient by lazy {
        APIExecutor.clientApi()
    }

}
