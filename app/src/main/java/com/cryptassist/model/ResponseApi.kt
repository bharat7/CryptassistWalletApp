package com.cryptassist.model

import java.io.Serializable

class ResponseApi : Serializable {
    var responseMessage: String? = null
    var content: String? = null
}