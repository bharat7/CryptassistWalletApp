package com.cryptassist.model


import com.google.gson.annotations.SerializedName

data class CurrencyModel(
        @SerializedName("id") val id: String,
        @SerializedName("mName") val name: String,
        @SerializedName("symbol") val symbol: String,
        @SerializedName("website_slug") val websiteSlug: String
)