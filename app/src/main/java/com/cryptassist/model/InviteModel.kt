package com.cryptassist.model

import android.databinding.ObservableField

class InviteModel {
    var emailId:ObservableField<String> = ObservableField()
    var phoneNo:ObservableField<String> = ObservableField()
}