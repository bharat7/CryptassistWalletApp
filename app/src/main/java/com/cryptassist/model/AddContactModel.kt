package com.cryptassist.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.graphics.Bitmap

@Entity(tableName = "contact_table")
class AddContactModel {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "contactId")
    var contactId: Int = 0

    @ColumnInfo(name = "mName")
    lateinit var name: String

    @ColumnInfo(name = "mAddress")
    lateinit var address: String

    @ColumnInfo(name = "mEmail")
    lateinit var email: String

    @ColumnInfo(name = "firstLetterName")
    lateinit var firstLetterName: String

    @ColumnInfo(name = "imagePath")
    lateinit var imagePath: String

}
