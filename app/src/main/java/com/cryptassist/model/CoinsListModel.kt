package com.cryptassist.model

import com.google.gson.annotations.SerializedName

data class CoinsListModel(
        val name: String? = null,
        val address: String? = null,
        val icon: String? = null,
        @SerializedName("current_bonus_rate") val currentBonusRate: String? = null,
        val currentRate: String? = null
)