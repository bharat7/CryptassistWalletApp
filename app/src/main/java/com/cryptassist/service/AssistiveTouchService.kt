package com.cryptassist.service

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Handler
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import android.widget.PopupWindow
import android.widget.Toast
import com.cryptassist.R
import com.cryptassist.sectionheadercomponents.SystemsUtils

import java.util.Timer

class AssistiveTouchService : Service() {

    private var isMoving: Boolean = false

    private var rawX: Float = 0.toFloat()
    private var rawY: Float = 0.toFloat()

    private var mScreenWidth: Int = 0
    private var mScreenHeight: Int = 0
    private var mStatusBarHeight: Int = 0

    private var lastAssistiveTouchViewX: Int = 0
    private var lastAssistiveTouchViewY: Int = 0

    private var mAssistiveTouchView: View? = null
    //    private View mInflateAssistiveTouchView;
    private var mWindowManager: WindowManager? = null
    private var mParams: WindowManager.LayoutParams? = null
    private val mPopupWindow: PopupWindow? = null
    private var mBulider: AlertDialog.Builder? = null
    private var mAlertDialog: AlertDialog? = null
    private val mScreenShotView: View? = null

    private var mTimer: Timer? = null
    private val mHandler: Handler? = null

    private var mInflater: LayoutInflater? = null

    override fun onCreate() {
        super.onCreate()
        init()
        calculateForMyPhone()
        createAssistiveTouchView()
        //        inflateViewListener();
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun init() {
        mTimer = Timer()
        //        mHandler =  new MyHandler();
        mBulider = AlertDialog.Builder(this@AssistiveTouchService)
        mAlertDialog = mBulider!!.create()
        mParams = WindowManager.LayoutParams()
        mWindowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mInflater = LayoutInflater.from(this)
        mAssistiveTouchView = mInflater!!.inflate(R.layout.assistive_touch_layout, null)
        //        mInflateAssistiveTouchView = mInflater.inflate(R.layout.assistive_touch_inflate_layout, null);
    }

    private fun calculateForMyPhone() {
        val displayMetrics = SystemsUtils.getScreenSize(this)
        mScreenWidth = displayMetrics.widthPixels
        mScreenHeight = displayMetrics.heightPixels
        mStatusBarHeight = SystemsUtils.getStatusBarHeight(this)

        //        mInflateAssistiveTouchView.setLayoutParams(new WindowManager.LayoutParams((int) (mScreenWidth * 0.75), (int) (mScreenWidth * 0.75)));
    }

    private fun createAssistiveTouchView() {
        mParams?.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY
        mParams?.width = WindowManager.LayoutParams.WRAP_CONTENT
        mParams?.height = WindowManager.LayoutParams.WRAP_CONTENT
        mParams?.x = mScreenWidth
        mParams?.y = 520
        mParams?.gravity = Gravity.TOP or Gravity.LEFT
        mParams?.format = PixelFormat.RGBA_8888
        mParams?.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        mWindowManager!!.addView(mAssistiveTouchView, mParams)
        mAssistiveTouchView!!.setOnTouchListener { _, event ->
            rawX = event.rawX
            rawY = event.rawY
            when (event.action) {
                MotionEvent.ACTION_DOWN -> isMoving = false
                MotionEvent.ACTION_UP -> setAssitiveTouchViewAlign()
                MotionEvent.ACTION_MOVE -> {
                    isMoving = true
                    mParams!!.x = (rawX - mAssistiveTouchView!!.measuredWidth / 2).toInt()
                    mParams!!.y = (rawY - (mAssistiveTouchView!!.measuredHeight / 2).toFloat() - mStatusBarHeight.toFloat()).toInt()
                    mWindowManager!!.updateViewLayout(mAssistiveTouchView, mParams)
                }
            }
            isMoving
        }
        mAssistiveTouchView!!.setOnClickListener { Toast.makeText(this@AssistiveTouchService, "jhk", Toast.LENGTH_LONG).show() }
    }

    /* private void inflateViewListener(){
        ImageView shutdown = (ImageView)mInflateAssistiveTouchView.findViewById(R.id.shutdown);
        ImageView star = (ImageView)mInflateAssistiveTouchView.findViewById(R.id.star);
        ImageView screenshot = (ImageView)mInflateAssistiveTouchView.findViewById(R.id.screenshot);
        ImageView home = (ImageView)mInflateAssistiveTouchView.findViewById(R.id.home);

        shutdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemsUtils.shutDown(AssistiveTouchService.this);
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mHandler.sendEmptyMessage(0);
                    }
                }, 626);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemsUtils.goHome(AssistiveTouchService.this);
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mHandler.sendEmptyMessage(0);
                    }
                }, 626);
            }
        });

        screenshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(0);
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        String filename = SystemsUtils.takeScreenShot(AssistiveTouchService.this);
                        Message msg = mHandler.obtainMessage();
                        msg.what = 1;
                        msg.obj = filename;
                        mHandler.sendMessage(msg);
                    }
                }, 626);
            }
        });
    }*/

    private fun myAssitiveTouchAnimator(fromx: Int, tox: Int, fromy: Int, toy: Int, flag: Boolean): ValueAnimator {
        val p1 = PropertyValuesHolder.ofInt("X", fromx, tox)
        val p2 = PropertyValuesHolder.ofInt("Y", fromy, toy)
        val v1 = ValueAnimator.ofPropertyValuesHolder(p1, p2)
        v1.duration = 100L
        v1.interpolator = DecelerateInterpolator()
        v1.addUpdateListener { animation ->
            val x = animation.getAnimatedValue("X") as Int
            val y = animation.getAnimatedValue("Y") as Int
            mParams?.x = x
            mParams?.y = y
            mWindowManager!!.updateViewLayout(mAssistiveTouchView, mParams)
        }
        v1.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                if (flag)
                    mAssistiveTouchView!!.alpha = 0.85f
            }
        })
        return v1
    }

    /*private ValueAnimator mSceenShotAnimator(){
        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat("scaleX", 1, 0);
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat("scaleY", 1, 0);
        ValueAnimator v1 = ValueAnimator.ofPropertyValuesHolder(p1, p2);
        v1.setDuration(5000L);
        v1.setInterpolator(new DecelerateInterpolator());
        v1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float scaleX = (Float) animation.getAnimatedValue("scaleX");
                Float scaleY = (Float) animation.getAnimatedValue("scaleY");
                *//*mScreenShotView.setScaleX(scaleX);
                mScreenShotView.setScaleY(scaleY);*//*
                *//*WindowManager.LayoutParams lp = mAlertDialog.getWindow().getAttributes();
                lp.width = (int)(scaleX * mScreenWidth);
                lp.height = (int)(scaleY * mScreenHeight);
                mAlertDialog.getWindow().setAttributes(lp);*//*
                *//*ViewGroup.LayoutParams lp = mScreenShotView.getLayoutParams();
                lp.width = (int)(scaleX * mScreenWidth);
                lp.height = (int)(scaleY * mScreenHeight);*//*
                //mScreenShotView
                mScreenShotView.setLayoutParams(new FrameLayout.LayoutParams((int)(scaleX * mScreenWidth), (int)(scaleY * mScreenHeight) ));
            }
        });
        return v1;
    }*/

    private fun setAssitiveTouchViewAlign() {
        val mAssistiveTouchViewWidth = mAssistiveTouchView!!.measuredWidth
        val mAssistiveTouchViewHeight = mAssistiveTouchView!!.measuredHeight
        val top = mParams!!.y + mAssistiveTouchViewWidth / 2
        val left = mParams!!.x + mAssistiveTouchViewHeight / 2
        val right = mScreenWidth - mParams!!.x - mAssistiveTouchViewWidth / 2
        val bottom = mScreenHeight - mParams!!.y - mAssistiveTouchViewHeight / 2
        val lor = Math.min(left, right)
        val tob = Math.min(top, bottom)
        val min = Math.min(lor, tob)
        lastAssistiveTouchViewX = mParams!!.x
        lastAssistiveTouchViewY = mParams!!.y
        if (min == top) mParams!!.y = 0
        if (min == left) mParams!!.x = 0
        if (min == right) mParams!!.x = mScreenWidth - mAssistiveTouchViewWidth
        if (min == bottom) mParams!!.y = mScreenHeight - mAssistiveTouchViewHeight
        myAssitiveTouchAnimator(lastAssistiveTouchViewX, mParams!!.x, lastAssistiveTouchViewY, mParams!!.y, false).start()
    }

    /*private void showScreenshot(String mName){
        String path = "/sdcard/Pictures/" + mName + ".png";
        Bitmap bitmap = BitmapFactory.decodeFile(path);

        mScreenShotView = mInflater.inflate(R.layout.screen_shot_show, null);
        ImageView imageView = (ImageView)mScreenShotView.findViewById(R.id.screenshot);
        imageView.setImageBitmap(bitmap);

        mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mAlertDialog.show();
        WindowManager.LayoutParams alertDialogParams = mAlertDialog.getWindow().getAttributes();
        alertDialogParams.width = mScreenWidth;
        alertDialogParams.height = mScreenHeight;
        mAlertDialog.getWindow().setAttributes(alertDialogParams);
        mAlertDialog.getWindow().setContentView(mScreenShotView);

        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Message msg = mHandler.obtainMessage();
                msg.what = 2;
                mHandler.sendMessage(msg);
            }
        }, 3000);

        *//*mSceenShotAnimator().start();*//*

        *//*ObjectAnimator.ofFloat(mScreenShotView, "translationX", 0, mScreenWidth-mScreenShotView.getX());
        ObjectAnimator.ofFloat(mScreenShotView, "translationY", 0, mScreenHeight-mScreenShotView.getY());
        ObjectAnimator.ofFloat(mScreenShotView, "scaleX", 1, 0);
        ObjectAnimator.ofFloat(mScreenShotView, "scaleY", 1, 0);*//*

        *//*mScreenShotView.setPivotX();
        mScreenShotView.setPivotY();*//*
        *//*PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat("X", 0, mScreenWidth);
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat("Y", 0, mScreenHeight/2);
        PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat("scaleX", 1, 0.5F);
        PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat("scaleY", 1, 0.5F);
        ObjectAnimator.ofPropertyValuesHolder(mScreenShotView,p1,p2,p3,p4).setDuration(2000).start();*//*
    }*/

    /*private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case 1:
                    showScreenshot((String)msg.obj);
                    break;
                case 2:
                    mAlertDialog.dismiss();
                default:
                    mPopupWindow.dismiss();
                    break;
            }
            super.handleMessage(msg);
        }
    }*/

    override fun onDestroy() {
        super.onDestroy()
        mWindowManager!!.removeView(mAssistiveTouchView)
    }
}
