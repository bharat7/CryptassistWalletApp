package com.cryptassist.repositories

import android.arch.lifecycle.LiveData
import android.content.Context
import android.os.AsyncTask
import com.cryptassist.dao.ContactDao
import com.cryptassist.db.ContactRoomDB
import com.cryptassist.model.AddContactModel

class ContactRepository constructor(context: Context) {

    private val contactDao: ContactDao
    private val alContacts: LiveData<List<AddContactModel>>


    init {

        val db = ContactRoomDB.getDataBase(context)
        contactDao = db?.contactDao()!!
        alContacts = contactDao.getContacts()
    }

    // You must call this on a non-UI thread or your app will crash.
    // Like this, Room ensures that you're not doing any long running operations on the main
    // thread, blocking the UI.
    fun insert(contactModel: AddContactModel) {
        insertAsyncTask(contactDao).execute(contactModel)
    }
    fun getAllContacts(): LiveData<List<AddContactModel>> {
        return alContacts
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: ContactDao): AsyncTask<AddContactModel, Void, Void>() {

        override fun doInBackground(vararg params: AddContactModel): Void? {

            mAsyncTaskDao.insert(params[0])
            return null
        }

    }
}