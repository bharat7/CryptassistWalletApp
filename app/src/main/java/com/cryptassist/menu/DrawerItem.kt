package com.cryptassist.menu

import android.view.ViewGroup

/**
 * Created by yarolegovich on 25.03.2017.
 */

abstract class DrawerItem<T : DrawerAdapter.ViewHolder> {

    var isChecked: Boolean = false

    abstract fun createViewHolder(parent: ViewGroup): T

    abstract fun bindViewHolder(holder: T)


    fun setChecked(isChecked: Boolean): DrawerItem<*> {
        this.isChecked = isChecked
        return this
    }

    val isSelectable: Boolean
        get() = true
    /*fun isChecked(): Boolean {
        return isChecked
    }*/

}
