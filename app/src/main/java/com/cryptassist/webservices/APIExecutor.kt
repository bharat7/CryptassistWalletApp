package com.cryptassist.webservices


import com.cryptassist.utils.AppConstants
import com.cryptassist.utils.LogUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIExecutor {
    private const val TIMEOUT: Long = 60
    private  val TAG = APIExecutor::class.simpleName
    fun clientMarketCap(): ApiInterface {

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppUrlConstant.MARKET_CAP_API)
                .build()

        return retrofit.create(ApiInterface::class.java)
    }

    fun clientApi(): ApiInterface {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                    .header("Accept", "application/json")
            if (AppConstants.AUTH_TOKEN != null)
                requestBuilder.header("Authorization", "Bearer " + AppConstants.AUTH_TOKEN!!)
            requestBuilder.method(original.method(), original.body())
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        httpClient.addInterceptor(interceptor)
        val client = httpClient.build()
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppUrlConstant.BASE_URL)
                .client(client)
                .build()

        return retrofit.create(ApiInterface::class.java)
    }


}
