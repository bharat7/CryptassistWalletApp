package com.cryptassist.webservices

import com.cryptassist.response.ApiResponse
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiInterface {

    //@GET("ticker/2491/")//V2 API
    @GET("ticker/travelflex/")
    fun fetchCoinDataService(@Query("convert") fiat: String): Observable<List<JsonObject>>


    @POST("create_token")
    fun createTokenService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("getContent")
    fun getContentService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("email_verification")
    fun emailVerificationService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("verifyOtp")
    fun verifyOtpService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("resendVerificationCode")
    fun resendOtpService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("sendFeedback")
    fun sendFeedbackService(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("coinListing")
    fun coinListing(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("suggestCoin")
    fun suggestCoinApi(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("myOrders")
    fun myOrdersListing(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("buyCoin")
    fun buyCoinApi(@Body jsonObject: JsonObject): Observable<ApiResponse>

    @POST("coinDetail")
    fun coinDetailApi(@Body jsonObject: JsonObject): Observable<ApiResponse>




}