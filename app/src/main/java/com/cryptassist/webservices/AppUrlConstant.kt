package com.cryptassist.webservices

import com.cryptassist.BuildConfig

/**
 * Created by Ashutosh Kumar on 10/1/2018.
 */

object AppUrlConstant {

    const val MARKET_CAP_API: String = "https://widgets.coinmarketcap.com/v1/"
//   const val MARKET_CAP_API: String = "https://api.coinmarketcap.com/v2/"  // V2 API

    val BASE_URL: String = if (BuildConfig.DEBUG) {
        /*Staging*/
//        "http://cta.local/api/"
        "http://192.168.1.16/api/"

    } else {
        /*Production*/
        ""
    }

}
