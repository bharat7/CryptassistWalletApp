package com.cryptassist.otputils;

public interface OtpListener {
  void onOtpEntered(String otp);
}
