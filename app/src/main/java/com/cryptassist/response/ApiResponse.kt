package com.cryptassist.response

import com.cryptassist.model.CoinsListModel
import com.cryptassist.viewModel.OrderViewModel
import com.google.gson.annotations.SerializedName

data class ApiResponse(
        val responseCode: Int,
        val responseMessage: String,
        val token: String,
        val title: String,
        @SerializedName("user_id") val userId: String,
        val content: String,
        val totalRecord: String,
        val otp: String,
        val currentPage: String,
        val totalPages: String,
        val coins:ArrayList<CoinsListModel>,
        val leads:ArrayList<OrderViewModel>

)