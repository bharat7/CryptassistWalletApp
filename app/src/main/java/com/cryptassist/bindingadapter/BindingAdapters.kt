package com.cryptassist.bindingadapter

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.cryptassist.R
import com.cryptassist.adapter.BuyCoinListAdapter
import com.cryptassist.adapter.ImportPassphraseAdapter
import com.cryptassist.adapter.OrderListingAdapter
import com.cryptassist.adapter.ProfileAddressAdapter
import com.cryptassist.model.BuyCoinListModel
import com.cryptassist.viewModel.ImportPhrasesViewModel
import com.cryptassist.viewModel.OrderViewModel
import com.cryptassist.viewModel.ProfileAddressListViewModel
import com.cryptassist.viewModel.ProfileAddressViewModel
import com.squareup.picasso.Picasso
import java.io.ByteArrayOutputStream

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("load_data")
    fun loadData(recyclerView: RecyclerView, data: ArrayList<ImportPhrasesViewModel>?) {
        recyclerView.layoutManager = GridLayoutManager(recyclerView.context,3)
        recyclerView.adapter = if (data != null) ImportPassphraseAdapter(recyclerView.context ,data) else ImportPassphraseAdapter(recyclerView.context, emptyList<ImportPhrasesViewModel>() as java.util.ArrayList<ImportPhrasesViewModel>)
    }


    @JvmStatic
    @BindingAdapter("profile_cta_address")
    fun profileCtaAddress(recyclerView: RecyclerView, data: ArrayList<ProfileAddressListViewModel>?/*, profileAddressViewModel: ProfileAddressViewModel*/) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = if (data != null) ProfileAddressAdapter(recyclerView.context ,data/*,profileAddressViewModel*/) else ProfileAddressAdapter(recyclerView.context, emptyList<ProfileAddressListViewModel>() as java.util.ArrayList<ProfileAddressListViewModel>/*,profileAddressViewModel*/)
    }

    @JvmStatic
    @BindingAdapter("order_listing")
    fun orderLising(recyclerView: RecyclerView, data: ArrayList<OrderViewModel>?) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = if (data != null) OrderListingAdapter(recyclerView.context ,data/*,profileAddressViewModel*/) else OrderListingAdapter(recyclerView.context, emptyList<OrderViewModel>() as java.util.ArrayList<OrderViewModel>/*,profileAddressViewModel*/)
    }

    @JvmStatic
    @BindingAdapter("buy_coin_list")
    fun buyCoinList(recyclerView: RecyclerView, data: ArrayList<BuyCoinListModel>?) {
        try {
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context,3)
            recyclerView.adapter = if (data != null) BuyCoinListAdapter(recyclerView.context ,data)
            else BuyCoinListAdapter(recyclerView.context, emptyList<BuyCoinListModel>() as java.util.ArrayList<BuyCoinListModel>)
        } catch (e: Exception) {
        }
    }

    @JvmStatic
    @BindingAdapter("error")
    fun showError(view: EditText, error: String?) {
        view.error = error
    }

    @JvmStatic
    @BindingAdapter("imageBitmap")
    fun loadImage(view: ImageView,path:String?=null) {
        try {
            if (!path.equals("null")&&!path.equals("")) {
                val bm = BitmapFactory.decodeFile(path)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                view.setImageBitmap(bm)
            }
            else {
                view.setImageResource(R.mipmap.contact_profile)
            }
        } catch (e: Exception) {
        }
    }

    @JvmStatic
    @BindingAdapter("coin_image")
    fun coinImage(view: ImageView,path:String?=null) {
        try {
            if (!path.equals("null")&&!path.equals("")) {
                Picasso.with(view.context)
                        .load(path)
                        .placeholder(R.mipmap.contact)
                        .error(R.mipmap.logos)
                        .into(view);
            }
            else {
                view.setImageResource(R.mipmap.logos)
            }
        } catch (e: Exception) {
        }
    }



    @JvmStatic
    @BindingAdapter("profile_image")
    fun ProfileImage(view: ImageView,path:String?=null) {
        try {
            if (!path.equals("null")&&!path.equals("")) {
                val bm = BitmapFactory.decodeFile(path)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                view.setImageBitmap(bm)
            }
            else {
                view.setImageResource(R.mipmap.contact_profile)
            }
        } catch (e: Exception) {
        }
    }

    @JvmStatic
    @BindingAdapter("visibility")
    fun showVisibility(view: ImageView, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
    @JvmStatic
    @BindingAdapter("edit_image")
    fun loadImage(imageView: ImageView,isEdit:Boolean) {
        try {
            if (isEdit) {
              imageView.setImageResource(R.mipmap.save)
            }
            else {
                imageView.setImageResource(R.mipmap.edit)
            }
        } catch (e: Exception) {
        }
    }

}